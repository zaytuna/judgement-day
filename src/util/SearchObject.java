package util;
public interface SearchObject
{
	public String getDisplay();
	public String getOutputString();
	public java.util.List<String> getSearchableFields();
}