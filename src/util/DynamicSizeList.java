package util;

import javax.swing.*;
import java.util.*;

public class DynamicSizeList<T extends SearchObject> extends SearchList<T> implements Runnable {
	private static final long serialVersionUID = -6414194140062039543L;
	int height = 20;
	int adjustTo = 200;
	boolean running = false;

	public DynamicSizeList(T[] data, JTextField input, JTextField output) {
		super(data, input, output);

		adjustTo = data.length;
		poss.clear();
		search("");
		adjust();
	}

	public DynamicSizeList(ArrayList<T> asdf, JTextField input, JTextField output) {
		super(asdf, input, output);

		adjustTo = asdf.size();
		poss.clear();
		search("");
		adjust();
	}

	public void adjust() {
		adjustTo = getModel().getSize() * height;
		if (!running) {
			new Thread(this).start();
			running = true;
		}
	}

	public void run() {
		while (adjustTo != getHeight()) {
			try {
				Thread.sleep(20);
			} catch (Exception e) {
				e.printStackTrace();
			}
			setSize(getWidth(), (int) (getHeight() + Math.signum(adjustTo - getHeight())
					* Math.max(Math.abs(adjustTo - getHeight()) / 7, 1)));
		}
		running = false;
	}

	public void search(String s) {

		if (s.equals("") || s == null)
			super.search("SFAR��?�?o?1�D8+=+");
		else
			super.search(s);

		adjust();
	}
}