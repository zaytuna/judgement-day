package util.paint;

import java.awt.Color;
import java.awt.Paint;
import java.awt.Point;
import java.io.Serializable;
import java.util.ArrayList;

public class MyShape implements Serializable {
	private static final long serialVersionUID = 5241287726449156112L;

	public int shapeType = 0;
	public boolean isFilled = false;
	public boolean isDashed = false;
	public Point startPoint = null;
	public Point endPoint = null;
	public Paint actual = Color.blue;
	public int pts = 5;

	public ArrayList<Point> points = new ArrayList<Point>();
	public int pointCount = 0;
	public boolean isBox = false;
	public int dashLength = 15;
	public String fileName = "";

	public MyShape(int type, boolean filled, boolean dashed, Point start,
			Point end, int width, Paint color, String fileN) {
		shapeType = type;
		isFilled = filled;
		isDashed = dashed;
		startPoint = start;
		endPoint = end;
		pts = width;
		actual = color;
		isBox = (type == 7);
		fileName = fileN;
	}

	public MyShape() {
	}

	public void setPoints(Point... blah) {
		points.clear();
		for (Point p : blah)
			points.add(p);
	}

	public void setPointCount(int blah) {
		pointCount = blah;
	}
}