package util.paint;

/*

 PART OF MASTER PROGRAM;
 in 'UltraPaint' Master Program (home)
 */

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Line2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

public class UltraPaintPanel extends JPanel implements java.io.Serializable {

	private static final long serialVersionUID = -2334167746682116713L;
	Controls controls;
	private static int gradientType = 0;
	public Paint colorOne = Color.black;
	public Paint colorTwo = Color.red;
	public int shapeCount = 0;
	Timer timer = new Timer(30, new MouseHandler());
	public ArrayList<MyShape> shapes = new ArrayList<MyShape>();

	Color background = Color.white;
	@SuppressWarnings("unused")
	private int xCoordinate = 0;
	public boolean isBox = false;
	@SuppressWarnings("unused")
	private int yCoordinate = 0;
	public String fileName = null;
	BufferedImage rep = null;
	int onPoint = 0;
	Graphics2D g2d;

	public int myHeight = 200;
	public int myWidth = 300;

	public UltraPaintPanel(Controls controls) {
		timer.start();
		setLayout(null);
		this.controls = controls;

		addMouseMotionListener(new MouseHandler());
		addMouseListener(new MouseHandler());
	}

	public void paintComponent(Graphics g) {
		try {
			g2d = (Graphics2D) g;
			g2d.setColor(background);
			g2d.fillRect(0, 0, getWidth(), getHeight());

			setBackground(background);
			if (rep != null) {
				g.drawImage(rep, 0, 0, this);
			}

			if (shapes.size() != 0) {
				for (int i = 0; i < shapeCount; i++) {
					if (shapes.get(i).shapeType != 3 && shapes.get(i).shapeType != 4 && shapes.get(i).shapeType != 6
							&& shapes.get(i).shapeType != 9 && shapes.get(i).shapeType != 7 && i != shapeCount) {
						g2d.setPaint(shapes.get(i).actual);

						if (shapes.get(i).shapeType == 0) {
							if (shapes.get(i).isDashed) {
								float[] dashesHi = { Float.parseFloat(shapes.get(i).dashLength + "") };
								g2d.setStroke(new BasicStroke(shapes.get(i).pts, BasicStroke.CAP_ROUND,
										BasicStroke.JOIN_MITER, 10, dashesHi, 0));
								g2d.draw(new Line2D.Double(shapes.get(i).startPoint.x, shapes.get(i).startPoint.y,
										shapes.get(i).endPoint.x, shapes.get(i).endPoint.y));
							} else {
								g2d.setStroke(new BasicStroke(shapes.get(i).pts, BasicStroke.CAP_ROUND,
										BasicStroke.JOIN_MITER));
								g2d.draw(new Line2D.Double(shapes.get(i).startPoint.x, shapes.get(i).startPoint.y,
										shapes.get(i).endPoint.x, shapes.get(i).endPoint.y));
							}
						} else if (shapes.get(i).shapeType == 1) {
							if (shapes.get(i).isFilled) {
								if ((shapes.get(i).endPoint.x - shapes.get(i).startPoint.x) < 0
										&& (shapes.get(i).endPoint.y - shapes.get(i).startPoint.y) < 0) {
									g2d.fillRect(shapes.get(i).endPoint.x, shapes.get(i).endPoint.y,
											(shapes.get(i).startPoint.x - shapes.get(i).endPoint.x),
											(shapes.get(i).startPoint.y - shapes.get(i).endPoint.y));
								} else if ((shapes.get(i).endPoint.y - shapes.get(i).startPoint.y) < 0) {
									g2d.fillRect(shapes.get(i).startPoint.x, shapes.get(i).endPoint.y,
											(shapes.get(i).endPoint.x - shapes.get(i).startPoint.x),
											(shapes.get(i).startPoint.y - shapes.get(i).endPoint.y));
								} else if ((shapes.get(i).endPoint.x - shapes.get(i).startPoint.x) < 0)
									g2d.fillRect(shapes.get(i).endPoint.x, shapes.get(i).startPoint.y,
											(shapes.get(i).startPoint.x - shapes.get(i).endPoint.x),
											(shapes.get(i).endPoint.y - shapes.get(i).startPoint.y));
								else
									g2d.fillRect(shapes.get(i).startPoint.x, shapes.get(i).startPoint.y,
											(shapes.get(i).endPoint.x - shapes.get(i).startPoint.x),
											(shapes.get(i).endPoint.y - shapes.get(i).startPoint.y));
							} else {
								if (shapes.get(i).isDashed) {
									float[] dashesHi = { Float.parseFloat(shapes.get(i).dashLength + "") };
									g2d.setStroke(new BasicStroke(shapes.get(i).pts, BasicStroke.CAP_ROUND,
											BasicStroke.JOIN_MITER, 10, dashesHi, 0));
								} else {
									g2d.setStroke(new BasicStroke(shapes.get(i).pts, BasicStroke.CAP_ROUND,
											BasicStroke.JOIN_MITER));
								}
								if ((shapes.get(i).endPoint.x - shapes.get(i).startPoint.x) < 0
										&& (shapes.get(i).endPoint.y - shapes.get(i).startPoint.y) < 0) {
									g2d.drawRect(shapes.get(i).endPoint.x, shapes.get(i).endPoint.y,
											(shapes.get(i).startPoint.x - shapes.get(i).endPoint.x),
											(shapes.get(i).startPoint.y - shapes.get(i).endPoint.y));
								} else if ((shapes.get(i).endPoint.y - shapes.get(i).startPoint.y) < 0) {
									g2d.drawRect(shapes.get(i).startPoint.x, shapes.get(i).endPoint.y,
											(shapes.get(i).endPoint.x - shapes.get(i).startPoint.x),
											(shapes.get(i).startPoint.y - shapes.get(i).endPoint.y));
								} else if ((shapes.get(i).endPoint.x - shapes.get(i).startPoint.x) < 0)
									g2d.drawRect(shapes.get(i).endPoint.x, shapes.get(i).startPoint.y,
											(shapes.get(i).startPoint.x - shapes.get(i).endPoint.x),
											(shapes.get(i).endPoint.y - shapes.get(i).startPoint.y));
								else
									g2d.drawRect(shapes.get(i).startPoint.x, shapes.get(i).startPoint.y,
											(shapes.get(i).endPoint.x - shapes.get(i).startPoint.x),
											(shapes.get(i).endPoint.y - shapes.get(i).startPoint.y));
							}
						} else if (shapes.get(i).shapeType == 2) {
							if (shapes.get(i).isFilled) {
								if ((shapes.get(i).endPoint.x - shapes.get(i).startPoint.x) < 0
										&& (shapes.get(i).endPoint.y - shapes.get(i).startPoint.y) < 0) {
									g2d.fillOval(shapes.get(i).endPoint.x, shapes.get(i).endPoint.y,
											(shapes.get(i).startPoint.x - shapes.get(i).endPoint.x),
											(shapes.get(i).startPoint.y - shapes.get(i).endPoint.y));
								} else if ((shapes.get(i).endPoint.y - shapes.get(i).startPoint.y) < 0) {
									g2d.fillOval(shapes.get(i).startPoint.x, shapes.get(i).endPoint.y,
											(shapes.get(i).endPoint.x - shapes.get(i).startPoint.x),
											(shapes.get(i).startPoint.y - shapes.get(i).endPoint.y));
								} else if ((shapes.get(i).endPoint.x - shapes.get(i).startPoint.x) < 0)
									g2d.fillOval(shapes.get(i).endPoint.x, shapes.get(i).startPoint.y,
											(shapes.get(i).startPoint.x - shapes.get(i).endPoint.x),
											(shapes.get(i).endPoint.y - shapes.get(i).startPoint.y));
								else
									g2d.fillOval(shapes.get(i).startPoint.x, shapes.get(i).startPoint.y,
											(shapes.get(i).endPoint.x - shapes.get(i).startPoint.x),
											(shapes.get(i).endPoint.y - shapes.get(i).startPoint.y));
							} else {
								if (shapes.get(i).isDashed) {
									float[] dashesHi = { Float.parseFloat(shapes.get(i).dashLength + "") };
									g2d.setStroke(new BasicStroke(shapes.get(i).pts, BasicStroke.CAP_ROUND,
											BasicStroke.JOIN_MITER, 10, dashesHi, 0));
								} else {
									g2d.setStroke(new BasicStroke(shapes.get(i).pts, BasicStroke.CAP_ROUND,
											BasicStroke.JOIN_MITER));
								}

								if ((shapes.get(i).endPoint.x - shapes.get(i).startPoint.x) < 0
										&& (shapes.get(i).endPoint.y - shapes.get(i).startPoint.y) < 0) {
									g2d.drawOval(shapes.get(i).endPoint.x, shapes.get(i).endPoint.y,
											(shapes.get(i).startPoint.x - shapes.get(i).endPoint.x),
											(shapes.get(i).startPoint.y - shapes.get(i).endPoint.y));
								} else if ((shapes.get(i).endPoint.y - shapes.get(i).startPoint.y) < 0) {
									g2d.drawOval(shapes.get(i).startPoint.x, shapes.get(i).endPoint.y,
											(shapes.get(i).endPoint.x - shapes.get(i).startPoint.x),
											(shapes.get(i).startPoint.y - shapes.get(i).endPoint.y));
								} else if ((shapes.get(i).endPoint.x - shapes.get(i).startPoint.x) < 0)
									g2d.drawOval(shapes.get(i).endPoint.x, shapes.get(i).startPoint.y,
											(shapes.get(i).startPoint.x - shapes.get(i).endPoint.x),
											(shapes.get(i).endPoint.y - shapes.get(i).startPoint.y));
								else
									g2d.drawOval(shapes.get(i).startPoint.x, shapes.get(i).startPoint.y,
											(shapes.get(i).endPoint.x - shapes.get(i).startPoint.x),
											(shapes.get(i).endPoint.y - shapes.get(i).startPoint.y));
							}
						} else if (shapes.get(i).shapeType == 5) {
							if (shapes.get(i).isFilled) {
								if ((shapes.get(i).endPoint.x - shapes.get(i).startPoint.x) < 0
										&& (shapes.get(i).endPoint.y - shapes.get(i).startPoint.y) < 0) {
									g2d.fill(new RoundRectangle2D.Double(shapes.get(i).endPoint.x,
											shapes.get(i).endPoint.y,
											(shapes.get(i).startPoint.x - shapes.get(i).endPoint.x),
											(shapes.get(i).startPoint.y - shapes.get(i).endPoint.y), 50, 50));
								} else if ((shapes.get(i).endPoint.y - shapes.get(i).startPoint.y) < 0) {
									g2d.fill(new RoundRectangle2D.Double(shapes.get(i).startPoint.x,
											shapes.get(i).endPoint.y,
											(shapes.get(i).endPoint.x - shapes.get(i).startPoint.x),
											(shapes.get(i).startPoint.y - shapes.get(i).endPoint.y), 50, 50));
								} else if ((shapes.get(i).endPoint.x - shapes.get(i).startPoint.x) < 0)
									g2d.fill(new RoundRectangle2D.Double(shapes.get(i).endPoint.x,
											shapes.get(i).startPoint.y,
											(shapes.get(i).startPoint.x - shapes.get(i).endPoint.x),
											(shapes.get(i).endPoint.y - shapes.get(i).startPoint.y), 50, 50));
								else
									g2d.fill(new RoundRectangle2D.Double(shapes.get(i).startPoint.x,
											shapes.get(i).startPoint.y,
											(shapes.get(i).endPoint.x - shapes.get(i).startPoint.x),
											(shapes.get(i).endPoint.y - shapes.get(i).startPoint.y), 50, 50));
							} else {
								if (shapes.get(i).isDashed) {
									float[] dashesHi = { Float.parseFloat(shapes.get(i).dashLength + "") };
									g2d.setStroke(new BasicStroke(shapes.get(i).pts, BasicStroke.CAP_ROUND,
											BasicStroke.JOIN_MITER, 10, dashesHi, 0));
								} else {
									g2d.setStroke(new BasicStroke(shapes.get(i).pts, BasicStroke.CAP_ROUND,
											BasicStroke.JOIN_MITER));
								}

								if ((shapes.get(i).endPoint.x - shapes.get(i).startPoint.x) < 0
										&& (shapes.get(i).endPoint.y - shapes.get(i).startPoint.y) < 0) {
									g2d.draw(new RoundRectangle2D.Double(shapes.get(i).endPoint.x,
											shapes.get(i).endPoint.y,
											(shapes.get(i).startPoint.x - shapes.get(i).endPoint.x),
											(shapes.get(i).startPoint.y - shapes.get(i).endPoint.y), 50, 50));
								} else if ((shapes.get(i).endPoint.y - shapes.get(i).startPoint.y) < 0) {
									g2d.draw(new RoundRectangle2D.Double(shapes.get(i).startPoint.x,
											shapes.get(i).endPoint.y,
											(shapes.get(i).endPoint.x - shapes.get(i).startPoint.x),
											(shapes.get(i).startPoint.y - shapes.get(i).endPoint.y), 50, 50));
								} else if ((shapes.get(i).endPoint.x - shapes.get(i).startPoint.x) < 0)
									g2d.draw(new RoundRectangle2D.Double(shapes.get(i).endPoint.x,
											shapes.get(i).startPoint.y,
											(shapes.get(i).startPoint.x - shapes.get(i).endPoint.x),
											(shapes.get(i).endPoint.y - shapes.get(i).startPoint.y), 50, 50));
								else
									g2d.draw(new RoundRectangle2D.Double(shapes.get(i).startPoint.x,
											shapes.get(i).startPoint.y,
											(shapes.get(i).endPoint.x - shapes.get(i).startPoint.x),
											(shapes.get(i).endPoint.y - shapes.get(i).startPoint.y), 50, 50));
							}

						} else if (shapes.get(i).shapeType == 8) {
							try {
								// new
								// ImageIcon(shapes.get(i).fileName).paintIcon(this,g,shapes.get(i).startPoint.x,shapes.get(i).startPoint.y);
								Image blah = new ImageIcon(shapes.get(i).fileName).getImage();
								g2d.drawImage(blah, shapes.get(i).startPoint.x, shapes.get(i).startPoint.y, (shapes
										.get(i).endPoint.x - shapes.get(i).startPoint.x),
										(shapes.get(i).endPoint.y - shapes.get(i).startPoint.y), this);
							} catch (Exception e) {
								e.printStackTrace();
								g.drawString("ERROR WITH PAINTING ICON: " + e, shapes.get(i).startPoint.x, shapes
										.get(i).startPoint.y);
							}
						}
					} else {
						g2d.setPaint(shapes.get(i).actual);

						if (shapes.get(i).shapeType == 3)// square cursor
						{
							for (int r = 0; r < shapes.get(i).pointCount; r++) {
								g2d.fillRect((shapes.get(i).points.get(r).x - (shapes.get(i).pts / 2)),
										(shapes.get(i).points.get(r).y - (shapes.get(i).pts / 2)), shapes.get(i).pts,
										shapes.get(i).pts);
							}
						} else if (shapes.get(i).shapeType == 4) {
							for (int r = 0; r < shapes.get(i).pointCount; r++) {
								g2d.fillOval((shapes.get(i).points.get(r).x - (shapes.get(i).pts / 2)),
										(shapes.get(i).points.get(r).y - (shapes.get(i).pts / 2)), shapes.get(i).pts,
										shapes.get(i).pts);
							}
						} else if (shapes.get(i).shapeType == 6)// Pencil
						{
							for (int r = 1; r < shapes.get(i).pointCount; r++) {
								if (shapes.get(i).isDashed) {
									float[] dashesHi = { Float.parseFloat(shapes.get(i).dashLength + "") };
									g2d.setStroke(new BasicStroke(shapes.get(i).pts, BasicStroke.CAP_ROUND,
											BasicStroke.JOIN_MITER, 10, dashesHi, 0));
								} else {
									g2d.setStroke(new BasicStroke(shapes.get(i).pts, BasicStroke.CAP_ROUND,
											BasicStroke.JOIN_MITER));
								}
								g2d.draw(new Line2D.Double(shapes.get(i).points.get(r).x,
										shapes.get(i).points.get(r).y, shapes.get(i).points.get(r - 1).x,
										shapes.get(i).points.get(r - 1).y));
							}
						} else if (shapes.get(i).shapeType == 9) {
							g2d.setStroke(new BasicStroke(shapes.get(i).pts, BasicStroke.CAP_ROUND,
									BasicStroke.JOIN_MITER));
							if (shapes.get(i).isFilled) {
								int[] xValues = new int[shapes.get(i).pointCount];
								int[] yValues = new int[shapes.get(i).pointCount];
								for (int r = 0; r < shapes.get(i).pointCount; r++) {
									xValues[r] = shapes.get(i).points.get(r).x;
									yValues[r] = shapes.get(i).points.get(r).y;
								}
								if (xValues.length == yValues.length && xValues.length == shapes.get(i).pointCount
										&& shapes.get(i).pointCount != 0) {
									g2d.fillPolygon(xValues, yValues, (shapes.get(i).pointCount));
								}
							} else {
								int[] xValues = new int[shapes.get(i).pointCount];
								int[] yValues = new int[shapes.get(i).pointCount];
								for (int r = 0; r < shapes.get(i).pointCount; r++) {
									xValues[r] = shapes.get(i).points.get(r).x;
									yValues[r] = shapes.get(i).points.get(r).y;
								}
								if (xValues.length == yValues.length && xValues.length == shapes.get(i).pointCount
										&& shapes.get(i).pointCount != 0) {
									g2d.drawPolygon(xValues, yValues, (shapes.get(i).pointCount));
								}
							}
						} else if (shapes.get(i).shapeType == 7) {
							if (onPoint % 3 == 0 || i != shapeCount - 1) {
								g2d.setStroke(new BasicStroke(shapes.get(i).pts, BasicStroke.CAP_ROUND,
										BasicStroke.JOIN_MITER));
								if (shapes.get(i).isFilled) {
									int[] xValues = new int[3];
									int[] yValues = new int[3];
									for (int r = 0; r < 3; r++) {
										xValues[r] = shapes.get(i).points.get(r).x;
										yValues[r] = shapes.get(i).points.get(r).y;
									}
									if (xValues.length == yValues.length && xValues.length == shapes.get(i).pointCount
											&& shapes.get(i).pointCount != 0) {
										g2d.fillPolygon(xValues, yValues, 3);
									}
								} else {
									int[] xValues = new int[3];
									int[] yValues = new int[3];
									for (int r = 0; r < 3; r++) {
										xValues[r] = shapes.get(i).points.get(r).x;
										yValues[r] = shapes.get(i).points.get(r).y;
									}
									if (xValues.length == yValues.length && xValues.length == shapes.get(i).pointCount
											&& shapes.get(i).pointCount != 0) {
										g2d.drawPolygon(xValues, yValues, 3);
									}
								}
							} else {
								try {
									for (int r = 0; r < 3; r++)
										g.drawString((r + 1) + "", shapes.get(i).points.get(r).x, shapes.get(i).points
												.get(r).y);
								} catch (IndexOutOfBoundsException e) {
								}
							}
						}
					}
				}
			}
			g2d.setStroke(new BasicStroke(1, 1, 1));
			if (controls.gradient.isSelected())
				if (gradientType == 3)
					g2d.setPaint(new GradientPaint(0, 0, (Color) colorOne, 100, 100, (Color) colorTwo, true));
				else if (gradientType == 2)
					g2d.setPaint(new GradientPaint(0, 0, (Color) colorOne, 0, 100, (Color) colorTwo, true));
				else if (gradientType == 1)
					g2d.setPaint(new GradientPaint(0, 0, (Color) colorOne, 100, 0, (Color) colorTwo, true));
				else if (gradientType == 0)
					g2d.setPaint(new GradientPaint(0, 0, (Color) colorOne, 100, 100, (Color) colorTwo, true));
				else
					g2d.setPaint((Color) colorOne);

			// int blah = 0;
			// try {
			// blah = Integer.parseInt(controls.lineWidth.getText());
			// } catch (NumberFormatException e) {
			// blah = 5;
			// }

			// g.drawOval(xCoordinate-blah,yCoordinate-blah,blah*2,blah*2);
			// g.drawLine(xCoordinate-blah,yCoordinate,xCoordinate+blah,yCoordinate);
			// g.drawLine(xCoordinate,yCoordinate-blah,xCoordinate,yCoordinate+blah);
			g2d.setStroke(new BasicStroke(1, 1, 1));
		} catch (Exception e) {
			e.printStackTrace();
			/*
			 * String s = ""; for(StackTraceElement element: e.getStackTrace())
			 * s+="\n\t@ "+element.toString(); JTextArea display = new
			 * JTextArea(25,70); display.setText("The Error was "+e+"\n"+s);
			 * display.setFont(new Font("SERIF",Font.PLAIN,14));
			 * display.setBackground(null);
			 * JOptionPane.showMessageDialog(UltraPaintPanel.this,new
			 * JScrollPane(display), "(UPP)Error: "+e,0); try {
			 * Thread.sleep(5000); }catch(Exception er){}
			 */
		}
	}

	public Dimension getPreferredSize() {
		return new Dimension(myWidth, myHeight);
	}

	private class MouseHandler implements MouseListener, MouseMotionListener, ActionListener {
		public void actionPerformed(ActionEvent evt) {
			repaint();
		}

		public void mouseExited(MouseEvent evt) {
			controls.mouseCoordinates.setText("Mouse outside of panel");
			controls.mouseCoordinates.setForeground(Color.LIGHT_GRAY);
		}

		public void mouseEntered(MouseEvent evt) {
			controls.mouseCoordinates.setForeground(Color.BLACK);
			controls.mouseCoordinates.setText("Mouse at: (" + evt.getX() + "," + evt.getY() + ")");
		}

		public void mouseClicked(MouseEvent evt) {

		}

		public void mouseMoved(MouseEvent evt) {
			controls.mouseCoordinates.setText("Mouse at: (" + evt.getX() + "," + evt.getY() + ")");
			repaint();
			xCoordinate = evt.getX();
			yCoordinate = evt.getY();
		}

		public void mousePressed(MouseEvent evt) {
			try {
				controls.owner.panel = UltraPaintPanel.this;
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Error in pressing in a document: \n" + e);
			}
			if (controls.shapeTypeCombo.getSelectedIndex() != 7) {
				shapes.add(new MyShape());
				try {
					if (fileName != null)
						shapes.get(shapeCount).fileName = fileName;
				} catch (Exception e) {
				}
				isBox = false;
				if (controls.gradient.isSelected()) {
					if (gradientType == 3)
						shapes.get(shapeCount).actual = (new GradientPaint(0, 0, (Color) colorOne, 100, 100,
								(Color) colorTwo, true));
					else if (gradientType == 2)
						shapes.get(shapeCount).actual = (new GradientPaint(0, 0, (Color) colorOne, 0, 100,
								(Color) colorTwo, true));
					else if (gradientType == 1)
						shapes.get(shapeCount).actual = (new GradientPaint(0, 0, (Color) colorOne, 100, 0,
								(Color) colorTwo, true));
					else if (gradientType == 0)
						shapes.get(shapeCount).actual = (new GradientPaint(0, 0, (Color) colorOne, 100, 100,
								(Color) colorTwo, true));
				} else
					shapes.get(shapeCount).actual = colorOne;

				shapes.get(shapeCount).shapeType = controls.shapeTypeCombo.getSelectedIndex();
				shapes.get(shapeCount).pts = Integer.parseInt(controls.lineWidth.getText());
				shapes.get(shapeCount).dashLength = Integer.parseInt(controls.dashLength.getText());

				shapes.get(shapeCount).isFilled = controls.filled.isSelected();
				shapes.get(shapeCount).isDashed = controls.dashed.isSelected();

				shapes.get(shapeCount).startPoint = evt.getPoint();
				shapes.get(shapeCount).endPoint = evt.getPoint();

				if (shapes.get(shapeCount).shapeType == 3 || shapes.get(shapeCount).shapeType == 4
						|| shapes.get(shapeCount).shapeType == 6) {
					shapes.get(shapeCount).points.add(evt.getPoint());
					shapes.get(shapeCount).pointCount++;
				}
				shapeCount++;
				repaint();
			}
		}

		public void mouseReleased(MouseEvent evt) {
			if (controls.shapeTypeCombo.getSelectedIndex() == 7) {
				onPoint++;
				if (onPoint % 3 == 1) {
					shapes.add(new MyShape());
					shapes.get(shapeCount).isFilled = controls.filled.isSelected();
					shapes.get(shapeCount).shapeType = controls.shapeTypeCombo.getSelectedIndex();
					shapes.get(shapeCount).pts = Integer.parseInt(controls.lineWidth.getText());
					if (controls.gradient.isSelected()) {
						if (gradientType == 3)
							shapes.get(shapeCount).actual = (new GradientPaint(0, 0, (Color) colorOne, 100, 100,
									(Color) colorTwo, true));
						else if (gradientType == 2)
							shapes.get(shapeCount).actual = (new GradientPaint(0, 0, (Color) colorOne, 0, 100,
									(Color) colorTwo, true));
						else if (gradientType == 1)
							shapes.get(shapeCount).actual = (new GradientPaint(0, 0, (Color) colorOne, 100, 0,
									(Color) colorTwo, true));
						else if (gradientType == 0)
							shapes.get(shapeCount).actual = (new GradientPaint(0, 0, (Color) colorOne, 100, 100,
									(Color) colorTwo, true));
					} else
						shapes.get(shapeCount).actual = (Color) colorOne;

					shapeCount++;
				}
				shapes.get(shapeCount - 1).points.add(evt.getPoint());
				shapes.get(shapeCount - 1).pointCount++;
			} else {
				shapes.get(shapeCount - 1).endPoint = evt.getPoint();
				repaint();
			}
			controls.undo.setEnabled(true);
		}

		public void mouseDragged(MouseEvent evt) {
			if (controls.shapeTypeCombo.getSelectedIndex() != 7) {
				if (shapes.get(shapeCount - 1).shapeType == 3 || shapes.get(shapeCount - 1).shapeType == 4
						|| shapes.get(shapeCount - 1).shapeType == 6 || shapes.get(shapeCount - 1).shapeType == 9) {
					shapes.get(shapeCount - 1).points.add(evt.getPoint());
					shapes.get(shapeCount - 1).pointCount++;
				}
				shapes.get(shapeCount - 1).endPoint = evt.getPoint();
				repaint();
				controls.mouseCoordinates.setText("Mouse at: (" + evt.getX() + "," + evt.getY() + ")");
				xCoordinate = evt.getX();
				yCoordinate = evt.getY();
			}// end of not a textBox
		}// end of mouseDragged
	}// end of MOuseHandler
}// end of class UltraPaintPanel