package util.paint;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import util.Methods;

public class UltraPaint extends JPanel {// implements ActionListener,
	// ChangeListener {

	private static final long serialVersionUID = 4993696054500930564L;
	// public JDesktopPane whole;
	// JButton newDoc = new JButton("New Document");
	String[] names = { "JPG", "BMP", "GIF" };
	Controls controls = new Controls(this);
	// public int count = 0;
	// public ArrayList<UltraPaintPanel> paint = new
	// ArrayList<UltraPaintPanel>();
	// public ArrayList<JInternalFrame> docs = new ArrayList<JInternalFrame>();
	// public UltraPaintPanel currentDoc = null;
	// private JPanel cChooser = new JPanel();
	// JSlider red = new JSlider(SwingConstants.VERTICAL, 0, 255, 240);
	// JSlider blue = new JSlider(SwingConstants.VERTICAL, 0, 255, 240);
	// JSlider green = new JSlider(SwingConstants.VERTICAL, 0, 255, 240);
	// JSlider alpha = new JSlider(SwingConstants.VERTICAL,0,255,240);
	JToolBar tools = new JToolBar();

	public UltraPaintPanel panel = new UltraPaintPanel(controls);

	public UltraPaint() {
		tools.setLayout(new FlowLayout(FlowLayout.CENTER));
		// red.addChangeListener(this);
		// green.addChangeListener(this);
		// blue.addChangeListener(this);
		// alpha.addChangeListener(this);
		// cChooser.setLayout(new GridLayout(1, 3));
		// cChooser.add(red);
		// cChooser.add(green);
		// cChooser.add(blue);
		// cChooser.add(alpha);
		// blue.setBackground(Color.blue);
		// green.setBackground(Color.green);
		// red.setBackground(Color.red);
		// alpha.setBackground(Color.PINK);
		// newDoc.setBounds(770, 540, 120, 35);

		setLayout(new BorderLayout());

		// whole = new JDesktopPane();
		// whole.setBounds(5,5,820,495);
		// cChooser.setBounds(826,0,70,500);
		// newDoc.addActionListener(this);
		// whole.setBorder(new CompoundBorder(new SoftBevelBorder(
		// BevelBorder.LOWERED), new LineBorder(Color.black)));
		//
		// add(whole, BorderLayout.CENTER);
		add(panel, BorderLayout.CENTER);
		add(tools, BorderLayout.NORTH);
		// add(cChooser, BorderLayout.EAST);
		controls.setOpaque(false);
		controls.setPreferredSize(new Dimension(765, 70));
		tools.add(controls);
		// tools.add(newDoc);

		// tools.setBounds(0,800,900,100);
		// add(tools);

		// controls.setBounds(0,500,760,100);
		// setPreferredSize(new Dimension(900, 800));

		addComponentListener(new Feet());
		// add(cChooser);
	}

	// public void stateChanged(ChangeEvent evt) {
	// UltraPaintPanel.background = new Color(red.getValue(),
	// green.getValue(), blue.getValue()/* ,alpha.getValue() */);
	// }

	public BufferedImage getImage() {
		BufferedImage img = new BufferedImage(panel.myWidth, panel.myHeight, BufferedImage.TYPE_INT_BGR);

		panel.paintComponent(img.createGraphics());

		return img;
	}

	// public void actionPerformed(ActionEvent evt) {
	// paint.add(new UltraPaintPanel(controls));
	// JInternalFrame document = new JInternalFrame("Document " + (count + 1),
	// true, true, true, true);
	// document.setBorder(new EtchedBorder(Color.GREEN, Color.BLACK));
	// docs.add(document);
	// document.add(new JScrollPane(paint.get(count)));
	// document.pack();
	// document.setLocation(count * 15, count * 10);
	// // document.setBorder(new CompoundBorder(new
	// // SoftBevelBorder((int)(Math.random()*2)),new
	// // LineBorder(Color.magenta)));
	// whole.add(document);
	// currentDoc = paint.get(count);
	// document.setVisible(true);
	// stateChanged(null);
	// count++;
	// }

	// public

	public void centerFrame(int frameWidth, int frameHeight) {
		Toolkit tools = Toolkit.getDefaultToolkit();
		Dimension screen = tools.getScreenSize();
		int xUpperLeftCorner = (screen.width - frameWidth) / 2;
		int yUpperLeftCorner = (screen.height - frameHeight) / 2;

		setBounds(xUpperLeftCorner, yUpperLeftCorner, frameWidth, frameHeight);
	}

	public void kill() {
		setVisible(false);
		tools.setVisible(false);
	}

	public void save(File file) {
		if (!file.exists()) {
			try {
				BufferedImage img = new BufferedImage(panel.getWidth(), panel.getHeight(), BufferedImage.TYPE_INT_BGR);

				panel.paintComponent(img.createGraphics());

				ImageIO.write(img, "JPG", file);
			} catch (Exception e) {
			}
		} else {
			int ans = JOptionPane.showConfirmDialog(null, "Do you want to replace the last file?");

			switch (ans) {
			case 1:
				try {
					BufferedImage img = new BufferedImage(panel.getWidth(), panel.getHeight(),
							BufferedImage.TYPE_INT_BGR);

					panel.paintComponent(img.createGraphics());

					ImageIO.write(img, "JPG", file);
				} catch (Exception e) {
				}
				break;
			case 2:
				int ans2 = JOptionPane.showConfirmDialog(null, "Do you want to add to the last file?");

				if (ans2 == 0) {
					try {
						BufferedImage img = new BufferedImage(panel.getWidth(), panel.getHeight(),
								BufferedImage.TYPE_INT_BGR);

						panel.paintComponent(img.createGraphics());

						ImageIO.write(img, "JPG", file);
					} catch (Exception e) {
					}
				}
				break;
			}
		}
	}

	public void load(File file) throws Exception {
		System.out.println(file.getAbsolutePath());

		if (file.exists()) {
			BufferedImage sg;
			sg = ImageIO.read(file);

			// JInternalFrame document = new JInternalFrame(file
			// .getName(), true, true, true, true);
			UltraPaintPanel savedPaintPanel = new UltraPaintPanel(controls);
			savedPaintPanel.rep = sg;
			// document.add(savedPaintPanel);
			// document.setSize(700, 400);
			// document.setLocation(45, 30);
			// owner.docs.add(document);
			// owner.paint.add(savedPaintPanel);
			// owner.whole.add(document);
			remove(panel);
			add(savedPaintPanel, BorderLayout.CENTER);
			panel = savedPaintPanel;
		}
	}

	public static void main(String args[]) {
		JFrame frame = new JFrame("Ultra Paint Program");
		JPanel panel = new UltraPaint();
		frame.add(panel);
		Methods.centerFrame(900, 600, frame);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	private class Feet extends ComponentAdapter implements ComponentListener {
		public void componentResized(ComponentEvent evt) {
			// whole.setBounds(5,5,getWidth()-80,getHeight()-105);
			// cChooser.setBounds(getWidth()-74,0,70,getHeight()-100);
			// controls.setBounds(0,getHeight()-100,getWidth(),100);
			// newDoc.setBounds(getWidth()-130,getHeight()-70,120,35);
		}
	}

}