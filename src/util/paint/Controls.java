package util.paint;

import java.awt.Color;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Controls extends JPanel implements ActionListener, ItemListener {
	private static final long serialVersionUID = 2072173920667385152L;

	UltraPaint owner;

	JButton undo = new JButton("Undo");
	JButton clear = new JButton("Clear");
	String names[] = { "Line", "Rectangle", "Oval", "Square Cursor",
			"Circular Cursor", "Rounded Rectangle", "Pencil", "Triangle",
			"Image", "Polygon" };
	JComboBox<String> shapeTypeCombo = new JComboBox<String>(names);
	JCheckBox filled = new JCheckBox("Filled");
	JCheckBox gradient = new JCheckBox("Use Gradient");
	JCheckBox dashed = new JCheckBox("Dashed");
	JButton colorChoose1 = new JButton("1st Color...");
	JButton colorChoose2 = new JButton("2nd Color...");

	JButton load = new JButton("Load");
	JButton save = new JButton("Save");
	JButton ins = new JButton("Choose Picture");
	JTextField dashLength = new JTextField();
	JTextField lineWidth = new JTextField();
	JLabel widthLabel = new JLabel("Width:"),
			mouseCoordinates = new JLabel(""), dashLabel = new JLabel(
					"Dash Length:");
	private UltraPaintPanel flashPanel = null;
	private javax.swing.Timer timer;
	private String lastDIR = "";

	public Controls(UltraPaint a) {
		this.owner = a;
		shapeTypeCombo.setMaximumRowCount(20);

		setLayout(null);
		// setLayout(new FlowLayout(FlowLayout.CENTER));

		undo.addActionListener(this);
		undo.setEnabled(false);
		clear.addActionListener(this);
		colorChoose1.addActionListener(this);
		save.addActionListener(this);
		load.addActionListener(this);
		colorChoose2.addActionListener(this);
		ins.addActionListener(this);
		shapeTypeCombo.addItemListener(this);
		// more.addActionListener(this);
		// less.addActionListener(this);
		// more2.addActionListener(this);
		// less2.addActionListener(this);
		gradient.addActionListener(this);

		gradient.setOpaque(false);
		filled.setOpaque(false);
		dashed.setOpaque(false);

		undo.setBounds(10, 10, 70, 25);
		mouseCoordinates.setBounds(630, 10, 135, 20);
		clear.setBounds(80, 10, 70, 25);
		shapeTypeCombo.setBounds(130, 40, 130, 25);
		filled.setBounds(270, 40, 70, 25);
		gradient.setBounds(190, 10, 100, 25);
		colorChoose1.setBounds(10, 40, 110, 25);
		colorChoose2.setBounds(290, 10, 110, 25);
		dashLength.setBounds(590, 40, 30, 25);
		lineWidth.setBounds(390, 40, 30, 25);
		widthLabel.setBounds(340, 40, 80, 25);
		// more.setBounds(10,70,200,20);
		dashLabel.setBounds(510, 40, 80, 25);
		dashed.setBounds(430, 40, 80, 25);
		load.setBounds(460, 10, 70, 25);
		ins.setBounds(630, 30, 135, 25);
		save.setBounds(535, 10, 70, 25);
		// more2.setBounds(210,70,200,20);
		// less.setBounds(410,70,200,20);
		// less2.setBounds(610,70,200,20);

		lineWidth.setText("5");
		dashLength.setText("15");
		// more.setBackground(new Color(224,204,152));
		// more2.setBackground(new Color(112,102,76));
		// less.setBackground(new Color(224/3,204/3,152/3));
		// less2.setBackground(new Color(224/8,204/8,152/8));

		add(mouseCoordinates);
		add(undo);
		add(clear);
		add(shapeTypeCombo);
		add(filled);
		add(gradient);
		// add(more);
		// add(more2);
		// add(less);
		// add(less2);
		add(colorChoose1);
		add(colorChoose2);
		add(dashLength);
		add(lineWidth);
		add(dashLabel);
		add(widthLabel);
		add(dashed);
		add(load);
		add(save);
		add(ins);
	}

	public void itemStateChanged(ItemEvent evt) {
		if (shapeTypeCombo.getSelectedIndex() == 8) {
			filled.setEnabled(false);
			gradient.setEnabled(false);
			dashed.setEnabled(false);
			colorChoose1.setEnabled(false);
			colorChoose2.setEnabled(false);
			load.setEnabled(false);
			save.setEnabled(false);
			dashLength.setEnabled(false);
			lineWidth.setEnabled(false);
			dashLabel.setEnabled(false);
			widthLabel.setEnabled(false);
		} else {
			filled.setEnabled(true);
			gradient.setEnabled(true);
			dashed.setEnabled(true);
			colorChoose1.setEnabled(true);
			colorChoose2.setEnabled(true);
			load.setEnabled(true);
			save.setEnabled(true);
			dashLength.setEnabled(true);
			lineWidth.setEnabled(true);
			dashLabel.setEnabled(true);
			widthLabel.setEnabled(true);
		}
	}

	public void actionPerformed(ActionEvent evt) {
		try {
			if (evt.getSource() == undo) {
				try {

					owner.panel.shapeCount--;
					owner.panel.shapes.get(owner.panel.shapeCount).pointCount = 0;
					owner.panel.onPoint = 0;
					
					owner.panel.shapes.get(owner.panel.shapeCount).points.clear();

					if (owner.panel.shapeCount <= 0)
						undo.setEnabled(false);

					repaint();

				} catch (Exception e) {
					undo.setEnabled(false);
				}
			}
			/*
			 * else if(evt.getSource() == more) { owner.panel.myHeight+=30;
			 * owner.panel.updateUI(); owner.panel.repaint(); if((owner
			 * .currentDoc.myHeight+30<owner.whole.getHeight())&&(owner.
			 * currentDoc.myHeight-30>200))
			 * owner.docs.get(owner.paint.indexOf(owner.panel)).pack(); } else
			 * if(evt.getSource() == more2) { owner.panel.myWidth+=30;
			 * owner.panel.updateUI(); owner.panel.repaint(); if((owner
			 * .currentDoc.myWidth+30<owner.whole.getWidth())&&(owner.panel
			 * .myWidth-30>300))
			 * owner.docs.get(owner.paint.indexOf(owner.panel)).pack(); } else
			 * if(evt.getSource() == less) { owner.panel.myHeight-=30;
			 * owner.panel.updateUI(); owner.panel.repaint(); if((owner
			 * .currentDoc.myHeight-30>200)&&(owner.panel.myHeight+
			 * 30<owner.whole.getHeight()))
			 * owner.docs.get(owner.paint.indexOf(owner.panel)).pack(); } else
			 * if(evt.getSource() == less2) { owner.panel.myWidth-=30;
			 * owner.panel.updateUI(); owner.panel.repaint(); if((owner
			 * .currentDoc.myWidth-30>300)&&(owner.panel.myWidth+30
			 * <owner.whole.getWidth()))
			 * owner.docs.get(owner.paint.indexOf(owner.panel)).pack(); }
			 */
			else if (evt.getSource() == clear) {
				owner.panel.shapeCount = 0;
				owner.panel.onPoint = 0;
				owner.panel.shapes.clear();
				owner.panel.rep = null;
				undo.setEnabled(false);
				owner.panel.repaint();
			} else if (evt.getSource() == colorChoose1) {
				owner.panel.colorOne = (Paint) (JColorChooser.showDialog(null,
						"Choose a foreground color",
						(Color) owner.panel.colorOne));
				colorChoose1.setBackground((Color) owner.panel.colorOne);
				System.out.println((Color) owner.panel.colorOne);
			} else if (evt.getSource() == colorChoose2) {
				owner.panel.colorTwo = (Paint) (JColorChooser.showDialog(null,
						"Choose a second gradient color",
						(Color) owner.panel.colorTwo));
				colorChoose2.setBackground((Color) owner.panel.colorTwo);
			}
			/*
			 * else if(evt.getSource() == gradient) { if(gradient.isSelected())
			 * { owner.panel.shapes.get(owner.panel.shapeCount).actual = new
			 * GradientPaint(0,0,(Color)owner.panel.colorOne, 50,50,
			 * (Color)owner.panel.colorTwo, true); } else {
			 * owner.panel.shapes.get(owner.panel.shapeCount).actual =
			 * (Color)owner.panel.colorOne; } }
			 */
			else if (evt.getSource() == load) {
				JFileChooser f = new JFileChooser();
				f.setCurrentDirectory(new File(lastDIR));
				f.setFileFilter(new javax.swing.filechooser.FileFilter() {
					public boolean accept(File f) {
						return (f.getName().endsWith(".jpg") || f.isDirectory());
					}

					public String getDescription() {
						return "Paint (.jpg)";
					}
				});
				int r = f.showOpenDialog(null);
				if (r == JFileChooser.APPROVE_OPTION) {
					lastDIR = f.getSelectedFile().getParent();
					try// ***************************************************
					{
						File file = new File(f.getSelectedFile()
								.getAbsolutePath());

						owner.load(file);
						// document.setVisible(true);
					} catch (Exception e) {
						String exception = e + "";
						e.printStackTrace();

						if (exception
								.equals("java.lang.ArrayIndexOutOfBoundsException: -1")) {
							int ans2 = JOptionPane
									.showConfirmDialog(
											this,
											"Blank file: "
													+ f.getSelectedFile()
															.getName()
													+ "\n\nDo you wish to delete the file?");
							if (ans2 == 0)
								new File(f.getSelectedFile().getAbsolutePath())
										.delete();
						} else
							JOptionPane.showMessageDialog(null, "Exception: "
									+ e);
					}// ****************************************************
				}
			} else if (evt.getSource() == save) {
				flash(owner.panel);
				int yes = JOptionPane.showConfirmDialog(null,
						"Is the flashing document the one you want to save?\n\n(if not"
								+ ", click on the correct document)");
				timer.stop();
				flashPanel = null;
				if (yes == 0) {
					JFileChooser f = new JFileChooser();
					f.setCurrentDirectory(new File(lastDIR));
					f.setFileFilter(new javax.swing.filechooser.FileFilter() {
						public boolean accept(File f) {
							return (f.getName().endsWith(".jpg") || f
									.isDirectory());
						}

						public String getDescription() {
							return "Picture (.jpg)";
						}
					});
					int r = f.showSaveDialog(null);
					if (r == JFileChooser.APPROVE_OPTION) {
						// **************************************************************
						lastDIR = f.getSelectedFile().getParent();
						String dir = f.getSelectedFile().getAbsolutePath();
						if (!(dir.endsWith(".jpg") || dir.endsWith(".JPG")))
							dir += ".jpg";
						System.out.println("The file name is: " + dir);
						owner.save(new File(dir));
						// ********************************************************
					}
				}
			} else if (evt.getSource() == ins) {
				JFileChooser f = new JFileChooser();
				f.setCurrentDirectory(new File(lastDIR));
				f.setFileFilter(new javax.swing.filechooser.FileFilter() {
					public boolean accept(File f) {
						return (f.getName().endsWith(".gif")
								|| (f.getName().endsWith(".jpg"))
								|| f.getName().endsWith(".png") || f
								.isDirectory());
					}

					public String getDescription() {
						return "Image (.gif, .jpg, .png)";
					}
				});
				int r = f.showOpenDialog(null);
				if (r == JFileChooser.APPROVE_OPTION) {
					lastDIR = f.getSelectedFile().getParent();
					if (owner.panel != null) {
						try {
							owner.panel.fileName = f.getSelectedFile()
									.getAbsolutePath();
						} catch (Exception e) {
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void flash(UltraPaintPanel p) {
		timer = new javax.swing.Timer(40, new Flasher());
		flashPanel = p;
		timer.start();
	}

	private class Flasher implements ActionListener {
		public void actionPerformed(ActionEvent evt) {
			flashPanel.background = (new Color((int) (Math.random() * 255),
					(int) (Math.random() * 255), (int) (Math.random() * 255))
					.brighter());
		}
	}
}// end of class Controls