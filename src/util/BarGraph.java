package util;

import java.awt.*;
import java.util.*;

import javax.swing.JFrame;

public class BarGraph extends javax.swing.JLabel {
	private static final long serialVersionUID = 1687234004174410982L;
	double[][] data;// [DATA][NUMBER]
	String[] labels;
	String[] keys;
	String xAxis, yAxis;

	int mode = 0;// 0 == bar, 1 == line

	public BarGraph(String xAxis, String yAxis, double[]... data) {
		this.data = data;
		this.xAxis = xAxis;
		this.yAxis = yAxis;
		// setOpaque(true);
	}

	public void setLabels(String... labels) {
		this.labels = labels;
	}

	public void setKey(String... keys) {
		this.keys = keys;
	}

	public void setData(double[]... data) {
		this.data = data;
		repaint();
	}

	public void setData(Map<Double, Double>[] data) {
		// System.out.println("Maximum length in data: " +
		// Methods.getMaxLength(data));
		this.data = new double[data.length][Methods.getMaxLength(data)];
		int index1 = 0;
		for (Map<Double, Double> dataSet : data) {
			if (dataSet.keySet().isEmpty())
				continue;
			// double maxValue = Methods.getMax(dataSet.keySet());
			// double[] array = new double[dataSet.keySet().size()];

			int index2 = 0;

			for (Double d : dataSet.keySet()) {
				dataSet.get(d);
				this.data[index1][index2] = dataSet.get(d);
				index2++;
			}

			index1++;
		}
		repaint();
	}

	public void paintComponent(Graphics gr) {
		Graphics2D g = (Graphics2D) gr;
		super.paintComponent(g);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		if (data.length > 0 && data[0].length > 0) {
			int xstep = (getWidth() - 30) / data[0].length;
			int ystep = (int) ((getHeight() - 90) / Methods.max(data));

			for (int i = 0; i < data.length; i++) {
				g.setColor(Methods.randomColor(i * 321 + 53 + i * i, 255));

				if (mode == 1)
					g.setStroke(new BasicStroke(5));

				for (int j = 0; j < data[i].length; j++) {
					if (mode == 1) {
						if (j == 0)
							continue;
						g.drawLine(20 + (j - 1) * xstep, getHeight() - (int) (data[i][j - 1] * ystep) - 30, 20 + j
								* xstep, getHeight() - (int) (data[i][j] * ystep) - 30);
					} else
						g.fillRect(20 + j * xstep + i * xstep / (data.length + 1), getHeight()
								- (int) (data[i][j] * ystep) - 30, xstep / (data.length + 1) + 1,
								(int) (data[i][j] * ystep) + 2);
				}
			}

			g.setStroke(new BasicStroke(1));

			for (int i = 0; i <= Methods.max(data); i += Math.max(Methods.max(data) / 10, 1)) {
				g.setColor(Color.BLACK);
				g.drawString("" + i, getWidth() - 15, getHeight() - ystep * i - 30);

				g.setColor(new Color(0, 0, 0, 100));
				g.drawLine(getWidth() - 15, getHeight() - ystep * i - 30, 20, getHeight() - ystep * i - 30);
			}
			for (int i = 0; i <= data[0].length; i++) {
				g.setColor(new Color(0, 0, 0, 100));
				g.drawLine(20 + xstep * i, 60, 20 + xstep * i, getHeight() - 15);

				g.setColor(Color.BLACK);
				g.drawString(labels == null || i >= labels.length ? "" + i : labels[i], 20 + xstep * i, getHeight() - 5);
			}

			if (keys != null)
				for (int i = 0; i < keys.length; i++) {
					g.setColor(Methods.randomColor(i * 321 + 53 + i * i, 255));
					g.fillRect(20 + i * (getWidth() - 40) / keys.length, 13, 10, 10);

					g.setColor(Color.WHITE/*
										 * Methods.randomColor(i * 321 + 53 + i
										 * * i, 255)
										 */);
					g.drawString(keys[i].substring(0, Math.min(keys[i].length(), 8)), 20 + i * (getWidth() - 40)
							/ keys.length + 15, 23);
				}
			g.setColor(Color.BLACK);
			g.drawString(xAxis, getWidth() / 2 - 20, 45);
			g.rotate(Math.PI / 2);
			g.drawString(yAxis, getHeight() / 2 - 20, -5);
			g.rotate(-Math.PI / 2);
		}
	}

	public static void main(String args[]) throws Exception {
		javax.swing.JFrame frame = new javax.swing.JFrame("TEST BAR GRAPH");
		Methods.centerFrame(440, 460, frame);
		BarGraph b = new BarGraph("Time", "Data", new double[] { 12, 5, 3, 8, 5, 0, 7, 5 }, new double[] { 2, 5, 7, 3,
				2, 9, 8, 7 }, new double[] { 10, 12, 8, 11, 0, 3, 3, 11 });
		b.setBounds(10, 10, 400, 400);
		frame.setLayout(null);
		frame.add(b);
		b.setBorder(new javax.swing.border.BevelBorder(0));
		b.setLabels("Hello", "n1", "3", "Avg", "TOP");
		b.setKey("A", "B", "C", "D");
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		while (true) {
			Thread.sleep(5000);
			b.mode = (b.mode + 1) % 2;
			b.repaint();
		}
	}
}