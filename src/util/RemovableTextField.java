package util;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;

public class RemovableTextField extends JPanel {
	private static final long serialVersionUID = 5432796441388912078L;
	protected JTextField field = new JTextField(4);
	protected JButton remove = new JButton("X");
	protected JToggleButton active = new JCheckBox();

	public RemovableTextField() {
		setLayout(new BorderLayout());
		remove.setBackground(Color.PINK);

		add(remove, BorderLayout.EAST);
		add(field, BorderLayout.WEST);
		add(active, BorderLayout.SOUTH);
	}

	public JTextField getField() {
		return field;
	}

	public JButton getButton() {
		return remove;
	}

	public JToggleButton getCheckBox() {
		return active;
	}

	public void setCheckBox(JToggleButton button) {
		remove(active);
		this.active = button;
		add(button, BorderLayout.SOUTH);
	}
}