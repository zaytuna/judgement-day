package util;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;

public class ColorfulExpand extends ExpandPanel {
	private static final long serialVersionUID = 4160150896851867197L;
	public Color c1 = new Color(100, 250, 200);
	public Color c2 = Color.black;
	public Point p = null;
	public Point p2 = null;
	Paint paint = null;

	public ColorfulExpand() {
		c1 = null;
		c2 = null;
	}

	public ColorfulExpand(Color c12, Color c21) {
		c1 = c12;
		c2 = c21;
	}

	public ColorfulExpand(Color c1, Color c2, Point p) {
		this.c2 = c2;
		this.c1 = c1;
		this.p = p;
	}

	public ColorfulExpand(Color c1, Color c2, Point p, Point p2) {
		this.c2 = c2;
		this.c1 = c1;
		this.p = p;
		this.p2 = p2;
	}

	public void paint() {
		repaint();
	}

	public void setPaint(Paint p) {
		this.paint = p;
	}

	public void paintComponent(Graphics g22) {
		super.paintComponent(g22);
		Graphics2D g = (Graphics2D) g22;

		if (!(c1 == null || c2 == null)) {
			boolean point = p == null;
			boolean point2 = p2 == null;
			g.setPaint(paint == null ? new GradientPaint(point2 ? 0 : p2.x,
					point2 ? 0 : p2.y, c1, point ? getWidth() : p.x,
					point ? getHeight() : p.y, c2) : paint);
			g.fillRect(0, 0, getWidth(), getHeight());
		}
	}
}