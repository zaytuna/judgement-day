package util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class ExpandPanel extends JPanel implements ComponentListener {
	private static final long serialVersionUID = 3800891809885646639L;
	HashMap<Component, FractionBounds> comp = new HashMap<Component, FractionBounds>();

	public Component add(Component c) {
		super.add(c);

		if (getLayout() == null)
			comp.put(c, new FractionBounds(c.getX() / (double) getX(), c.getY()
					/ (double) getY(), c.getWidth() / (double) getWidth(), c
					.getHeight()
					/ (double) getHeight()));
		return c;
	}

	public void remove(Component c) {
		if (comp.keySet().contains(c))
			comp.remove(c);
	}

	public void componentHidden(ComponentEvent evt) {
	}

	public void componentShown(ComponentEvent evt) {
	}

	public void componentMoved(ComponentEvent evt) {
	}

	public void componentResized(ComponentEvent evt) {
		if (getLayout() == null)
			for (Component c : comp.keySet()) {
				FractionBounds b = comp.get(c);
				c.setBounds((int) (b.x * getX()), (int) (b.y * getY()),
						(int) (b.w * getWidth()), (int) (b.h * getHeight()));
			}
		else
			comp.clear();
	}

	public class FractionBounds {
		double x, y, w, h;

		FractionBounds(double a, double b, double c, double d) {
			x = a;
			y = b;
			w = c;
			h = d;
		}
	}
}