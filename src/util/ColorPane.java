package util;

import java.awt.Color;

import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

public class ColorPane extends JTextPane {
	private static final long serialVersionUID = 8059518103333744072L;

	public void append(Color c, String s) {
		try {
			StyleContext sc = StyleContext.getDefaultStyleContext();
			AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY,
					StyleConstants.Foreground, c);
			Document doc = getDocument();

			int len = getDocument().getLength();
			// setCaretPosition(len);
			// setCharacterAttributes(aset, false);
			// replaceSelection(s);
			doc.insertString(len, s, aset);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}