package util;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

public class TypeThisField extends JTextField implements FocusListener,
		ActionListener {
	private static final long serialVersionUID = 4138390405650346036L;

	String display;
	String text = "";

	TypeThisField(String to) {
		display = to;
		addFocusListener(this);
		addActionListener(this);
		setForeground(Color.LIGHT_GRAY);
		super.setText(display + ": " + text);
	}

	public void focusGained(FocusEvent evt) {
		setForeground(Color.BLACK);
		super.setText(text);
	}

	public void setText(String s) {
		text = s;
		if (hasFocus())
			super.setText(s);
		else
			super.setText(display + ": " + text);
	}

	public String getText() {
		return (hasFocus() ? super.getText() : text);
	}

	public void clear() {
		super.setText(display + ": ");
		text = "";
	}

	public void focusLost(FocusEvent evt) {
		text = super.getText();
		setForeground(Color.GRAY);
		super.setText(display + ": " + text);
	}

	public void actionPerformed(ActionEvent evt) {
		transferFocus();
	}
}