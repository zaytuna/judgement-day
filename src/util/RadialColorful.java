package util;

import java.awt.*;
import javax.swing.*;
import java.awt.geom.*;

public class RadialColorful extends JPanel {
	private static final long serialVersionUID = 7852602149356855414L;
	Color[] colors;
	float[] dist;
	Point center, focus;
	int radius;
	public boolean gradients = true;
	MultipleGradientPaint.CycleMethod type = MultipleGradientPaint.CycleMethod.NO_CYCLE;

	public RadialColorful() {
		colors = null;
	}

	public RadialColorful(Color... colors) {
		this(null, colors);
	}

	public RadialColorful(Point a, Color... colors) {
		this(200, null, a, colors);
	}

	public RadialColorful(int rad, Point a, Point b, Color... colors) {
		super(true);
		this.colors = colors;
		this.radius = rad;
		this.focus = b;
		this.center = a;
	}

	public void paint() {
		repaint();
	}

	public void setAll(int rad, Point a, Point b, Color... colors) {
		this.colors = colors;
		this.radius = rad;
		this.focus = b;
		this.center = a;
	}

	public void paintComponent(Graphics g22) {
		super.paintComponent(g22);
		Graphics2D g = (Graphics2D) g22;

		if (gradients) {

			if (colors != null) {
				if (dist == null) {
					dist = new float[colors.length];
					for (int i = 0; i < colors.length; i++) {
						dist[i] = ((float) i) / colors.length;
					}
				}

				boolean c = center == null;
				boolean f = focus == null;

				g.setPaint(new RadialGradientPaint(c ? new Point2D.Float(getWidth() / 2, getHeight() / 2) : center,
						radius, f ? (c ? new Point2D.Float(getWidth() / 2, getHeight() / 2) : center) : focus, dist,
						colors, type));

				g.fillRect(0, 0, getWidth(), getHeight());
			}
		} else if (colors != null && colors.length > 0) {
			g.setColor(colors[colors.length - 1]);
			g.fill(g.getClip());
		}
	}
}