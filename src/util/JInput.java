package util;

import java.awt.Color;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

public class JInput extends JComponent {
	private static final long serialVersionUID = -5194977523503097286L;

	public JTextField f;
	public JLabel l;
	boolean stars = false;

	public JInput(String s, boolean b) {
		l = new JLabel(s);
		stars = b;
		f = stars ? new JPasswordField() : new JTextField();
		l.setBounds(0, 0, 0, 0);
		f.setBounds(100, 0, 0, 0);
		f.setOpaque(false);
		setOpaque(false);
		add(f);
		add(l);
		f.setBorder(new LineBorder(Color.BLACK));
		
		setPreferredSize(f.getPreferredSize());
	}

	public String getText() {
		return stars ? new String(((JPasswordField) f).getPassword()) : f
				.getText();
	}

	public void setBounds(int a, int b, int c, int d) {
		super.setBounds(a, b, c, d);
		l.setBounds(0, 0, c / 2, d);
		f.setBounds(c / 2, 0, c / 2, d);
	}
}