package util;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class SearchList<T extends SearchObject> extends javax.swing.JList<String> implements ListSelectionListener,
		KeyListener {

	private static final long serialVersionUID = -6649094846363830019L;

	public List<T> poss;
	public List<T> total;
	javax.swing.JTextField adjuster;
	javax.swing.JTextField output;
	java.awt.Color color;

	public SearchList(T[] possibilities, javax.swing.JTextField t, javax.swing.JTextField j) {
		this(Methods.getList(possibilities), t, j);
	}

	public SearchList(ArrayList<T> possibilities, javax.swing.JTextField t, javax.swing.JTextField j) {
		total = possibilities;
		poss = new ArrayList<T>(possibilities);
		adjuster = t;
		output = j;
		addListSelectionListener(this);
		if (adjuster != null)
			adjuster.addKeyListener(this);
		updateData();
	}

	public void updateData() {
		String[] data = new String[poss.size()];

		for (int i = 0; i < data.length; i++) {
			data[i] = poss.get(i).getDisplay();
		}
		setListData(data);
		if (poss.size() == 1)
			setSelectedIndex(0);
	}

	public void setListData(List<T> stuff) {
		total = stuff;
		poss = new ArrayList<T>(total);
		updateData();
	}

	public void keyReleased(KeyEvent evt) {
		search(adjuster.getText());
	}

	public void keyPressed(KeyEvent evt) {
	}

	public void keyTyped(KeyEvent evt) {
	}

	public void valueChanged(ListSelectionEvent evt) {
		if (output != null)
			output.setText(getSelectedIndex() >= 0 ? poss.get(getSelectedIndex()).getOutputString() : "");
	}

	public void search(String search) {
		poss.clear();
		search = search.toLowerCase();
		for (T asdf : total) {
			for (String s : asdf.getSearchableFields()) {
				if (s.trim().toLowerCase().contains(search)) {
					if (!s.equals("")) {
						poss.add(asdf);
						break;
					}
				}
			}
		}
		updateData();
	}
}