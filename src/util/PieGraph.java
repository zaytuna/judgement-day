package util;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Arc2D;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class PieGraph extends JLabel implements Runnable {
	private static final long serialVersionUID = 3038029536831495576L;
	double[] data, dataTo;
	String[] labels;

	public PieGraph(double data[]) {
		this.data = data;
	}

	public PieGraph() {
	}

	public void setLabels(String... labels) {
		this.labels = labels;
	}

	public void setData(double... data) {
		if (this.data == null) {
			this.data = data;
			this.dataTo = data;
			labels = new String[data.length];
			for (int i = 0; i < data.length; i++)
				labels[i] = "" + data[i];
			return;
		} else {
			this.dataTo = data;
			String[] l = new String[data.length];
			for (int i = 0; i < data.length; i++)
				l[i] = "" + (i >= labels.length ? data[i] : labels[i]);

			this.labels = l;
			new Thread(this).start();
			repaint();
		}
	}

	public void run() {
		int want = dataTo.length;
		while (!contentsAreClose(data, dataTo, .01)) {
			try {
				Thread.sleep(10);
			} catch (Exception e) {
			}
			if (data.length < dataTo.length) {
				double[] doubles = new double[dataTo.length];
				for (int i = 0; i < dataTo.length; i++)
					doubles[i] = i < data.length ? data[i] : 0;
				data = doubles;
			} else if (data.length > dataTo.length) {
				double[] doubles = new double[data.length];
				for (int i = 0; i < data.length; i++)
					doubles[i] = i < dataTo.length ? dataTo[i] : 0;
				dataTo = doubles;
			}

			for (int i = 0; i < data.length; i++) {
				data[i] += (dataTo[i] - data[i]) / 10;
			}
			repaint();
		}

		try {
			if (dataTo.length != want) {
				double[] doubles = new double[want];
				for (int i = 0; i < want; i++)
					doubles[i] = dataTo[i];
				dataTo = doubles;
			}
		} catch (Exception e) {
		}
		data = dataTo;
	}

	public void paintComponent(Graphics gr) {
		Graphics2D g = (Graphics2D) gr;
		super.paintComponent(g);

		if (!(data == null || data.length == 0)) {
			final double sum = Methods.sum(data);

			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

			double startAngle = 0;
			/*
			 * g.setPaint(new GradientPaint(80, 50 + (int) (getHeight() / 5),
			 * Color.BLACK, getWidth() - 20,
			 * 50 + (int) (getHeight() / 2.5), Color.BLUE));
			 * g.fillOval(80, 50 + getHeight() / 5, getWidth() - 100,
			 * (int) (getHeight() / 2.5));
			 * g.fillRect(80, 50 + (int) (getHeight() / 5), getWidth() - 100,
			 * (int) (getHeight() / 5));
			 */
			g.setStroke(new BasicStroke(2));
			g.drawOval(80, 50 + getHeight() / 5, getWidth() - 100, (int) (getHeight() / 2.5));
			g.setStroke(new BasicStroke(1));

			for (int i = 0; i < data.length; i++) {
				g.setColor(Methods.randomColor(i * 2521 + 2338, 255).darker());

				double angle = ((360 * data[i]) / sum);

				double sa = Math.max(180, startAngle), saa = Math.max(180, startAngle + angle);

				Arc2D.Double arc = new Arc2D.Double(80, 50 + getHeight() / 5, getWidth() - 100,
						(int) (getHeight() / 2.5), sa, saa - sa, Arc2D.CHORD);
				g.fill(arc);

				g
						.fillPolygon(new int[] {
								(int) (getWidth() / 2 + 30 + Math.cos(Math.PI * sa / 180) * (getWidth() / 2 - 50)),
								(int) (getWidth() / 2 + 30 + Math.cos(Math.PI * sa / 180) * (getWidth() / 2 - 50)),
								(int) (getWidth() / 2 + 30 + Math.cos(Math.PI * saa / 180) * (getWidth() / 2 - 50)),
								(int) (getWidth() / 2 + 30 + Math.cos(Math.PI * saa / 180) * (getWidth() / 2 - 50)) },
								new int[] {
										(int) (getHeight() / 5 + 50),
										(int) (2 * getHeight() / 5 + 50 - Math.sin(Math.PI * sa / 180)
												* (getHeight() / 5)) + 2,
										(int) (2 * getHeight() / 5 + 50 - Math.sin(Math.PI * saa / 180)
												* (getHeight() / 5)) + 2, (int) (getHeight() / 5 + 50) }, 4);

				g.fillRect(1, i * 30 + 8, 15, 15);
				g.setColor(Color.BLACK);

				if (i < labels.length)
					g.drawString(labels[i] + " (" + (int) data[i] + "/" + (int) sum + ")", 22, i * 30 + 20);

				startAngle += angle;
			}
			startAngle = 0;
			for (int i = 0; i < data.length; i++) {
				g.setColor(Color.BLACK);
				double angle = ((360 * data[i]) / sum);
				g.setColor(Methods.randomColor(i * 2521 + 2338, 255));
				g.fillArc(80, 50, getWidth() - 100, (int) (getHeight() / 2.5), (int) startAngle, (int) angle + 1);
				startAngle += angle;
			}

		}

	}

	public static boolean contentsAreClose(double[] a, double[] b, double precision) {
		if (a == null || b == null)
			return false;
		if (a.length != b.length)
			return false;
		for (int i = 0; i < a.length; i++)
			if (Math.abs(a[i] - b[i]) > precision)
				return false;
		return true;
	}

	public static void main(String args[]) throws Exception {
		javax.swing.JFrame frame = new javax.swing.JFrame("TEST PIE GRAPH");
		Methods.centerFrame(420, 420, frame);
		PieGraph b = new PieGraph();
		b.setData(new double[] { 12, 5, 3, 8, 5, 0, 7, 5 });
		b.setLabels("A", "B", "C", "D", "E", "F", "G", "H");
		b.setBounds(0, 0, 400, 400);
		frame.setLayout(null);
		frame.add(b);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		while (true) {
			Thread.sleep(2000);
			double[] array = new double[(int) (Math.random() * 6) + 1];
			for (int i = 0; i < array.length; i++)
				array[i] = (int) (4 * Math.random() + 1);
			b.setData(array);
		}
	}
}