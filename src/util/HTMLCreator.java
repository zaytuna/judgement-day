package util;

import java.awt.Color;
import java.awt.Font;

public class HTMLCreator {

	private StringBuilder text = new StringBuilder();
	boolean underline = false;

	public void addCenteredLine(String txt) {
		text.append("<BR><center>" + txt + "</center>");
	}

	public void addLine(String txt) {
		text.append("<BR>" + txt);
	}

	public void add(String txt) {
		text.append(text);
	}

	public void setFont(Font f) {

	}

	public void toggleUnderline() {
		if (underline)
			text.append("</u>");
		else
			text.append("<u>");

		underline = !underline;
	}

	public void toggleBold() {
		if (underline)
			text.append("</b>");
		else
			text.append("<b>");

		underline = !underline;
	}

	public void toggleItalic() {
		if (underline)
			text.append("</i>");
		else
			text.append("<i>");

		underline = !underline;
	}

	public void setForeground(Color c) {
		text.append("<font color=\"#" + Integer.toHexString(c.getRGB()) + "\">");
	}

	public String getHTML() {
		return text.toString() + "</HTML>";
	}

	public String getText() {
		return removeHTML(getHTML());
	}

	public static String getHTML(String s) {
		return "<HTML>" + s.replaceAll("\n", "<BR>") + "</HTML>";
	}

	public static String removeHTML(String s) {
		StringBuffer buffer = new StringBuffer(s);
		int indexLeft = 0;
		int indexRight = 0;
		boolean done = false;
		// remove < >
		do {
			indexLeft = buffer.indexOf("<");
			indexRight = buffer.indexOf(">");
			if (indexLeft < 0 && indexRight < 0) {
				done = true;
				break;
			}
			buffer.delete(indexLeft, indexRight + 1);
			s = buffer.toString();
			buffer = new StringBuffer(s);
		} while (!done);

		// remove &nbsp;
		done = false;
		do {
			indexLeft = buffer.indexOf("&nbsp;");
			indexRight = indexLeft + 6;
			if (indexLeft < 0) {
				done = true;
				break;
			}

			buffer.delete(indexLeft, indexRight);
			s = buffer.toString();
			buffer = new StringBuffer(s);
		} while (!done);
		return buffer.toString();
	}// end of removeHTML()
}
