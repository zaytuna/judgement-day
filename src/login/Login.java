package login;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;

import util.ColorfulExpand;
import util.SearchList;
import admin.CreateUserPanel;
import framework.JudgementDriver;
import framework.User;

public class Login extends ColorfulExpand implements ActionListener, Runnable, javax.swing.event.ChangeListener {
	private static final long serialVersionUID = 9003417655512171220L;
	double MOVING = .125;

	public static BufferedImage img;

	double sliding = 0; // 0 = in=invisible, 1 = out=available
	int placement = 0;// 0 = in, 1 == sliding out, 2 == out, 3 == sliding in.
	double velocity = 0;

	JTextField username = new JTextField();
	JPasswordField password = new JPasswordField();
	JButton go = new JButton("Log in");
	JButton help = new JButton("HELP");
	JButton documentation = new JButton("Documentation");
	JToggleButton newUser = new JToggleButton("New User");
	JButton exit = new JButton("Exit");
	JTextField search = new JTextField();
	JLabel searchLabel = new JLabel("Search:");
	JLabel usernameLabel = new JLabel("Useraname:");
	JLabel passLabel = new JLabel("Password:");
	JLabel credits = new JLabel("Program created by Oliver Richardson, 10th grade");

	HelpDialog helpPanel;

	Point available, not_seen;

	CreateUserPanel create = new CreateUserPanel(this);
	SearchList<User> users = new SearchList<User>(User.allUsers, search, username);

	JudgementDriver owner;
	JScrollPane pig;

	static {
		try {
			img = javax.imageio.ImageIO.read(new File(JudgementDriver.uioptions.logo));
		} catch (Exception e) {
		}
	}

	public Login(JudgementDriver owner) {
		super(JudgementDriver.uioptions.loginColor, JudgementDriver.uioptions.loginColor2, new Point(900, 880),
				new Point(600, 600));
		setLayout(null);

		this.owner = owner;
		helpPanel = new HelpDialog(owner);

		setPreferredSize(new Dimension(250, 150));
		// setBorder(new
		// javax.swing.border.BevelBorder(javax.swing.border.BevelBorder.RAISED));

		setBackground(new Color(200, 220, 255));
		
		pig = new JScrollPane(users);

		exit.setBounds(460, 720, 130, 50);
		documentation.setBounds(590, 720, 130, 50);
		help.setBounds(720, 720, 130, 50);
		newUser.setBounds(850, 720, 130, 50);

		search.setBounds(660, 200, 220, 25);
		searchLabel.setBounds(590, 200, 70, 25);
		username.setBounds(665, 390, 115, 25);
		password.setBounds(665, 416, 115, 25);
		passLabel.setBounds(590, 416, 70, 25);
		usernameLabel.setBounds(590, 390, 70, 25);
		pig.setBounds(590, 230, 290, 150);
		username.setEditable(false);
		username.setBorder(new javax.swing.border.LineBorder(Color.BLACK));
		username.setOpaque(false);
		// password.setBackground(new Color(225, 255, 225));
		go.setBounds(785, 390, 100, 51);
		create.setSize(create.getPreferredSize());
		password.addActionListener(this);
		go.addActionListener(this);
		help.addActionListener(this);
		exit.addActionListener(this);
		documentation.addActionListener(this);
		newUser.addActionListener(this);
		search.addActionListener(this);

		exit.setBackground(JudgementDriver.uioptions.loginButtonColors[0]);
		documentation.setBackground(JudgementDriver.uioptions.loginButtonColors[1]);
		help.setBackground(JudgementDriver.uioptions.loginButtonColors[2]);
		newUser.setBackground(JudgementDriver.uioptions.loginButtonColors[3]);

		exit.setForeground(JudgementDriver.uioptions.loginButtonColors[0].getRed()
				+ JudgementDriver.uioptions.loginButtonColors[0].getGreen()
				+ JudgementDriver.uioptions.loginButtonColors[0].getBlue() > 382 ? Color.BLACK : Color.WHITE);
		documentation.setForeground(JudgementDriver.uioptions.loginButtonColors[1].getRed()
				+ JudgementDriver.uioptions.loginButtonColors[1].getGreen()
				+ JudgementDriver.uioptions.loginButtonColors[1].getBlue() > 382 ? Color.BLACK : Color.WHITE);
		help.setForeground(JudgementDriver.uioptions.loginButtonColors[2].getRed()
				+ JudgementDriver.uioptions.loginButtonColors[2].getGreen()
				+ JudgementDriver.uioptions.loginButtonColors[2].getBlue() > 382 ? Color.BLACK : Color.WHITE);
		newUser.setForeground(JudgementDriver.uioptions.loginButtonColors[3].getRed()
				+ JudgementDriver.uioptions.loginButtonColors[3].getGreen()
				+ JudgementDriver.uioptions.loginButtonColors[3].getBlue() > 382 ? Color.BLACK : Color.WHITE);

		/*
		 * exit.setContentAreaFilled(false); study.setContentAreaFilled(false);
		 * documentation.setContentAreaFilled(false);
		 * help.setContentAreaFilled(false);
		 * newUser.setContentAreaFilled(false);
		 */

		add(username);
		add(searchLabel);
		add(search);
		add(usernameLabel);
		add(passLabel);
		add(pig);
		add(exit);
		add(password);
		add(documentation);
		add(go);
		add(newUser);
		add(help);
		add(create);
		add(credits);
		username.requestFocus();
		new Thread(this).start();
	}

	public void setVisible(boolean b) {
		super.setVisible(b);

		users.setListData(User.allUsers);
	}

	public void setBounds(int a, int b, int c, int d) {
		super.setBounds(a, b, c, d);

		not_seen = new Point(c - 270, d);
		available = new Point(c - create.getPreferredSize().width - 20, d - create.getPreferredSize().height - 70);
		// available = new Point(50,d - create.getPreferredSize().height-70);

		create.setLocation(not_seen);

		exit.setBounds(c - 540, d - 60, 130, 50);
		documentation.setBounds(c - 410, d - 60, 130, 50);
		help.setBounds(c - 280, d - 60, 130, 50);
		newUser.setBounds(c - 150, d - 60, 130, 50);
		credits.setBounds(30, d - 30, 300, 30);

		int SHIFT = 20 * d / 77 - 200, LIST_GROW = d / 3 - 150;

		search.setBounds(59 * c / 100 + 70, 200 + SHIFT, 29 * c / 100 - 70, 25);
		searchLabel.setBounds(59 * c / 100, 200 + SHIFT, 70, 25);
		username.setBounds(59 * c / 100 + 75, 390 + SHIFT + LIST_GROW, 185 * c / 1000 - 70 + 95 * c / 1000 - 90, 25);
		password.setBounds(59 * c / 100 + 75, 416 + SHIFT + LIST_GROW, 185 * c / 1000 - 70 + 95 * c / 1000 - 90, 25);
		passLabel.setBounds(59 * c / 100, 416 + SHIFT + LIST_GROW, 70, 25);
		usernameLabel.setBounds(59 * c / 100, 390 + SHIFT + LIST_GROW, 70, 25);
		pig.setBounds(59 * c / 100, 230 + SHIFT, 29 * c / 100, 150 + LIST_GROW);
		go.setBounds(88 * c / 100 - 90, 390 + SHIFT + LIST_GROW, 90, 51);

		super.p = new Point(9 * c / 10, 88 * d / 77);
		super.p2 = new Point(6 * c / 10, 60 * d / 77);
		paint();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(img, 8 * getWidth() / 100, 15 * getHeight() / 77, null);
	}

	public void stateChanged(javax.swing.event.ChangeEvent evt) {
		placement = 3;
		velocity = MOVING;
		User.addUser(create.getUser());
		users.setListData(User.allUsers);
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == help) {
			User u = User.getUser(username.getText());
			if (u != null) {
				helpPanel.setUser(u);
				helpPanel.setVisible(true);
			} else
				JOptionPane.showMessageDialog(null, "You must first select your username.");
		} else if (evt.getSource() == exit)
			System.exit(0);
		else if (evt.getSource() == documentation) {
		} else if (evt.getSource() == newUser) {
			placement = !newUser.isSelected() ? 3 : 1;
			velocity = MOVING;
			create.clear();
		} else if (evt.getSource() == search) {
			password.requestFocus();
		} else {
			User u = User.getUser(username.getText(), new String(password.getPassword()));
			if (u == null) {
				JOptionPane.showMessageDialog(null, "Wrong password.\n If you're having trouble,"
						+ " select your name from the list,and press HELP.");
				password.requestFocus();
				password.setText(null);

				return;

			} else
				owner.login(u);

			username.setText("");
			password.setText("");
			search.setText("");
			users.search("");
		}
		repaint();
	}

	public void run() {
		// double counter = 0;

		while (true) {
			try {
				Thread.sleep(10);

				/*
				 * counter += .01; int dy = (int)(20*Math.sin(counter));
				 * 
				 * exit.setLocation(exit.getX(),getHeight()-60+dy);
				 * study.setLocation(study.getX(),getHeight()-60+dy);
				 * documentation
				 * .setLocation(documentation.getX(),getHeight()-60+dy);
				 * help.setLocation(help.getX(),getHeight()-60+dy);
				 * newUser.setLocation(newUser.getX(),getHeight()-60+dy);
				 */

				if (placement == 0 || placement == 2)
					;
				if (placement == 1) {
					if (sliding < 1) {
						velocity *= .9;
						sliding += velocity;
						create.setLocation((int) (not_seen.x + (available.x - not_seen.x) * sliding),
								(int) (not_seen.y + (available.y - not_seen.y) * sliding));
					} else {
						sliding = 1;
						velocity = 0;
						placement = 2;
						create.setLocation(available);
					}
				} else if (placement == 3) {
					if (sliding > 0) {
						velocity *= .9;
						sliding -= velocity;
						create.setLocation((int) (not_seen.x + (available.x - not_seen.x) * sliding),
								(int) (not_seen.y + (available.y - not_seen.y) * sliding));
					} else {
						sliding = 0;
						velocity = 0;
						placement = 0;
						create.setLocation(not_seen);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}