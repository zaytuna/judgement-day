package login;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import util.ColorfulExpand;
import util.Methods;
import admin.ChangeDataPanel;
import admin.GradingPanel;
import framework.Administrator;
import framework.Course;
import framework.JudgementDriver;
import framework.MenuListener;
import framework.Score;
import framework.Test;
import framework.User;

public class Menu extends JPanel implements ActionListener, Runnable, MouseListener {
	private static final long serialVersionUID = 8784990793980856594L;

	String[] tests = new File("Tests/").list();

	ArrayList<MenuListener> listeners = new ArrayList<MenuListener>();

	JButton logOff = new JButton("Log off", new ImageIcon("Resources/key.png"));
	JButton exit = new JButton("Exit", new ImageIcon("Resources/exit.png"));

	User owner;

	ChangeDataPanel cdp = new ChangeDataPanel();

	// triangle vars
	String[] names = { "Take Test", "Change Classes", "View Scores", "Activity", "Chat", "Grading" };
	double[] sliding = { 0, 0, 0, 0, 0, 0 };// 0 to 1;
	Color[] colors = { new Color(43, 59, 0), new Color(99, 110, 0), new Color(247, 177, 0), new Color(235, 66, 0),
			new Color(107, 0, 25), Color.BLACK };
	// int mouseOver = -1;
	int selected = -1;

	OptionsPane[] panels = { new TakeTestPanel(), new ChangeClassPanel(), new ViewScoresPanel(), new ActivityPanel(),
			new ComplainPanel(), new GradingWrapper() };

	public Menu(MenuListener driver) {
		super(null);

		listeners.add(driver);
		setBorder(new BevelBorder(BevelBorder.RAISED));

		setBackground(JudgementDriver.uioptions.menuBack);
		setTheme(logOff, JudgementDriver.uioptions.menuBack);
		setTheme(exit, JudgementDriver.uioptions.menuBack);

		cdp.setBounds(1000 / 20, 2 * 770 / 11, 1000 * 340 / 1000, 230);

		logOff.addActionListener(this);
		exit.addActionListener(this);
		cdp.addActionListener(this);

		addMouseListener(this);

		add(cdp);
		if (driver != null)
			add(logOff);
		add(exit);

		for (int i = 0; i < names.length; i++) {
			panels[i].setBounds(650, 140, 300, 500);
			panels[i].setDoubleBuffered(true);
			add(panels[i]);
			panels[i].setOpaque(false);
		}

		new Thread(this).start();
	}

	public void setBounds(int a, int b, int c, int d) {
		super.setBounds(a, b, c, d);

		logOff.setBounds(c / 2 - 233 / 2, d - 60, 113, 35);
		exit.setBounds(c / 2 - 233 / 2 + 113, d - 60, 120, 35);
	}

	public void mousePressed(MouseEvent evt) {
	}

	public void setTheme(Color c) {
		setBackground(c);
		setTheme(logOff, c);
		setTheme(exit, c);
	}

	public void mouseReleased(MouseEvent evt) {
		// if (mouseOver != -1) {
		// // listSelected[selected] = mouseOver;
		// return;
		// }

		for (int i = 0; i < names.length; i++) {
			int x = (int) (800 - 340 * Methods.sum(sliding));
			if (new Polygon(new int[] { x, x, (int) (110 + x + 60 * sliding[i]), (int) (130 + x + 40 * sliding[i]),
					(int) (110 + x + 60 * sliding[i]) }, new int[] { (int) (200 + 70 * i + 20 * (1 - sliding[i])),
					(int) (200 + 70 * i - 20 * (1 - sliding[i])), (int) (180 + 70 * i - 30 * sliding[i]), 200 + 70 * i,
					(int) (220 + 70 * i + 30 * sliding[i]) }, 5).contains(evt.getPoint())) {
				if (i == selected)
					selected = -1;
				else
					selected = i;
			}
		}
	}

	public void mouseClicked(MouseEvent evt) {
	}

	public void mouseEntered(MouseEvent evt) {
	}

	public void mouseExited(MouseEvent evt) {
	}

	public void run() {
		while (true) {
			// try {
			// Point mouse = new Point(Methods.mouse().x
			// - getLocationOnScreen().x, Methods.mouse().y
			// - getLocationOnScreen().y);
			//
			// mouseOver = -1;
			//
			// if (selected >= 0)
			// for (int j = 0; j < listItems[selected].size(); j++)
			// if (mouse.x > 660 && mouse.y < 175 + j * 25
			// && mouse.y > 150 + j * 25 && mouse.x < 950) {
			// mouseOver = j;
			// break;
			// }
			// } catch (Exception e) {
			// }

			cdp.setLocation((int) (200 - 150 * Methods.sum(sliding)), (int) (190 - 50 * Methods.sum(sliding)));

			try {
				Thread.sleep(30);
			} catch (Exception e) {
			}

			for (int i = 0; i < sliding.length; i++) {
				double ds = ((selected == i ? 1 : 0) - sliding[i]) / 5;
				sliding[i] += ds;
				panels[i].setLocation(650, (int) (640 * Math.pow(sliding[i], 2) - 505));
				panels[i].repaint();
			}

			repaint();
		}
	}

	public void reset() {
		sliding = new double[] { 0, 0, 0, 0, 0, 0 };
		selected = -1;
	}

	private void setTheme(JComponent c, Color a) {
		c.setBackground(a);
		c.setBorder(new BevelBorder(BevelBorder.RAISED));
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == logOff)
			for (MenuListener ml : listeners)
				ml.logout();

		else if (evt.getSource() == exit)
			System.exit(0);

		else if (evt.getSource() == cdp) {
			if (owner.isAdmin()) {
				int x = Administrator.allAdmins.indexOf(User.getUser(owner.id));
				Administrator.allAdmins.set(x, (Administrator) cdp.createUser(owner));
				Administrator.saveAll();

				owner = cdp.createUser(owner);
				setUser(owner);
			} else {
				int x = User.allUsers.indexOf(User.getUser(owner.id));
				User.allUsers.set(x, cdp.createUser(owner));
				User.saveAll();

				owner = cdp.createUser(owner);
				setUser(owner);
			}
		}
	}

	public void setUser(User u) {
		owner = u;
		repaint();
		new Thread(new Loader()).start();

		exit.setVisible(owner == null || !owner.isAdmin());
		logOff.setVisible(owner == null || !owner.isAdmin());
	}

	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		g2d.setColor(new Color(150, 0, 0));

		g2d.setFont(new Font("SERIF", Font.BOLD, 30));
		g2d.drawString("Welcome " + owner.fName + "!", cdp.getX() + 50, cdp.getY() - 10);

		for (int i = 0; i < names.length; i++) {
			// draw a tab
			g2d.setColor(colors[i]);

			int x = (int) (800 - 340 * Methods.sum(sliding));

			Polygon p = new Polygon(new int[] { x, x, (int) (110 + x + 60 * sliding[i]),
					(int) (130 + x + 40 * sliding[i]), (int) (110 + x + 60 * sliding[i]) }, new int[] {
					(int) (200 + 70 * i + 20 * (1 - sliding[i])), (int) (200 + 70 * i - 20 * (1 - sliding[i])),
					(int) (180 + 70 * i - 30 * sliding[i]), 200 + 70 * i, (int) (220 + 70 * i + 30 * sliding[i]) }, 5);

			g2d.fillPolygon(p);
			g2d.setColor(Color.BLACK);
			g2d.drawPolygon(p);

			g2d.setColor(Color.WHITE);
			g2d.setFont(new Font("SERIF", Font.BOLD, 17));
			g2d.drawString((selected == i ? "- " : "") + names[i], (int) (x + 4 + sliding[i] * 40), (205 + 70 * i));

			// start drawing individual components.
			/*
			 * g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER
			 * ,(float)sliding[i])); g2d.setComposite(AlphaComposite.Src);
			 */
		}

	}

	public static void drawCenteredText(Graphics g, Font f, String s, int x, int y, int w, int h) {
		// Find the size of string s in font f in the current Graphics context
		// g.
		FontMetrics fm = g.getFontMetrics(f);
		java.awt.geom.Rectangle2D rect = fm.getStringBounds(s, g);

		int textHeight = (int) (rect.getHeight());
		int textWidth = (int) (rect.getWidth());

		// Center text horizontally and vertically
		int p = (w - textWidth) / 2 + x;
		int q = (h - textHeight) / 2 + fm.getAscent() + y;

		g.drawString(s, p, q); // Draw the string.
	}

	// following are internal panel classes

	abstract class OptionsPane extends ColorfulExpand {
		private static final long serialVersionUID = 4764745003403233381L;

		public abstract void update();
	}

	class TakeTestPanel extends OptionsPane implements ActionListener {
		private static final long serialVersionUID = 5841047029718577394L;
		JList<String> list = new JList<String>();
		JButton take = new JButton("Go");

		ArrayList<Integer> taken;
		ArrayList<Test> availableTests;
		ArrayList<Score> score;

		TakeTestPanel() {
			setLayout(new BorderLayout());
			list.setCellRenderer(new TestListRenderer());

			take.setPreferredSize(new Dimension(120, 34));
			JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
			panel.setOpaque(false);
			take.addActionListener(this);

			list.setBorder(new LineBorder(Color.BLUE));
			list.setBackground(new Color(200, 220, 255));
			list.setForeground(Color.BLACK);

			panel.add(take);

			add(list, BorderLayout.CENTER);

			add(panel, BorderLayout.SOUTH);
		}

		public void update() {
			take.setVisible(owner == null || !owner.isAdmin());
		}

		public void actionPerformed(ActionEvent evt) {
			for (MenuListener l : listeners) {
				if (l != null) {
					String itemName = (String) list.getSelectedValue();
					Test test = Test.fromName(itemName);

					if (owner.getTimesTaken(test) < test.retakes || test.retakes == -1)
						l.startTest(test);
				}
			}
		}

		public class TestListRenderer extends DefaultListCellRenderer {
			private static final long serialVersionUID = 7730000416455783413L;

			public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
					boolean hasFocus) {

				JPanel panel = new JPanel(new BorderLayout());
				JLabel times = new JLabel((taken.get(index) == 0 ? "" : taken.get(index))
						+ (availableTests.get(index).retakes == -1 ? " " : " / " + availableTests.get(index).retakes
								+ " "));

				JLabel j = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, hasFocus);

				if (taken.get(index) > 0) {
					j.setHorizontalAlignment(JLabel.CENTER);
					double result = score.get(index).grade() / score.get(index).possible();

					j.setForeground(Methods.colorMeld(Color.RED, Color.GREEN, result).darker());
					j.setFont(new Font("SANS_SERIF", Font.PLAIN, 15));

					JLabel lbl2 = new JLabel((int) (100 * result) + "%");
					lbl2.setFont(new Font("SERIF", Font.PLAIN, 20));
					panel.add(lbl2, BorderLayout.WEST);
				} else {
					j.setIcon(new ImageIcon("Resources/arrow.png"));
					j.setHorizontalAlignment(JLabel.CENTER);
					j.setFont(new Font("SANS_SERIF", Font.BOLD, 24));
				}

				if (taken.get(index) >= availableTests.get(index).retakes && availableTests.get(index).retakes >= 0) {
					times.setForeground(Color.RED);
					j.setForeground(Color.RED);
					j.setIcon(null);
				}

				panel.add(j, BorderLayout.CENTER);
				panel.add(times, BorderLayout.EAST);

				panel.setOpaque(false);
				return panel;
			}
		}
	}

	class ChangeClassPanel extends OptionsPane implements ActionListener {

		private static final long serialVersionUID = -4260465687470422268L;
		JList<Course> listTaken = new JList<Course>();
		JList<Course> listNotTaken = new JList<Course>();
		JButton remove = new JButton(new ImageIcon("Resources/Other/left.png"));
		JButton add = new JButton(new ImageIcon("Resources/Other/right.png"));

		ChangeClassPanel() {
			setLayout(null);

			JPanel container = new JPanel();
			container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
			container.setOpaque(false);

			container.setBounds(126, 100, 50, 300);
			listTaken.setBounds(180, 0, 120, 500);
			listNotTaken.setBounds(0, 0, 120, 500);

			listTaken.setBorder(new javax.swing.border.LineBorder(Color.BLACK, 2));
			listTaken.setBackground(new Color(220, 255, 220));
			listNotTaken.setBackground(new Color(255, 220, 220));
			listNotTaken.setBorder(new javax.swing.border.LineBorder(Color.BLACK, 2));

			add(container);
			add(listTaken);
			add(listNotTaken);

			add.addActionListener(this);
			remove.addActionListener(this);

			container.add(remove);
			container.add(add);
		}

		public void actionPerformed(ActionEvent evt) {
			if (evt.getSource() == add && listNotTaken.getSelectedIndex() >= 0) {
				owner.classes = Methods.ensureContains(owner.classes, ((Course) (listNotTaken.getSelectedValue()))
						.getNumber());
				update();

				if (owner.isAdmin())
					Administrator.saveAll();
				else
					User.saveAll();

				new Thread(new Loader()).start();

			} else if (evt.getSource() == remove && listTaken.getSelectedIndex() >= 0) {
				owner.classes = Methods.removeAll(owner.classes, ((Course) (listTaken.getSelectedValue())).getNumber());
				update();
				if (owner.isAdmin())
					Administrator.saveAll();
				else
					User.saveAll();

				new Thread(new Loader()).start();
			}
		}

		public void update() {
			listTaken.setListData(new Vector<Course>(Course.getCourses(owner)));

			ArrayList<Course> courses = new ArrayList<Course>();
			for (Course c : Course.allCourses)
				courses.add(c);
			courses.removeAll(Course.getCourses(owner));
			listNotTaken.setListData(new Vector<Course>(courses));
		}
	}

	class ViewScoresPanel extends OptionsPane implements ListSelectionListener {
		private static final long serialVersionUID = -714418154144263681L;

		JLabel display = new JLabel();
		JList<String> tests = new JList<String>();

		ViewScoresPanel() {
			setLayout(new BorderLayout());
			add(tests, BorderLayout.NORTH);
			add(display, BorderLayout.CENTER);
			display.setPreferredSize(new Dimension(600, 400));
			display.setHorizontalAlignment(JLabel.CENTER);
			display.setVerticalAlignment(JLabel.CENTER);

			tests.setBorder(new javax.swing.border.LineBorder(Color.BLACK, 2));
			tests.setBackground(new Color(250, 230, 200));

			tests.addListSelectionListener(this);
		}

		public void update() {
			tests.setListData(new Vector<String>(Test.getAvailableTestNames(owner)));
			if (tests.getModel().getSize() > 0)
				tests.setSelectedIndex(0);
		}

		@Override
		public void valueChanged(ListSelectionEvent evt) {
			if (tests.getSelectedIndex() >= 0) {
				String itemName = (String) tests.getSelectedValue();

				Test t = Test.fromName(itemName);
				if (owner != null && t != null)
					if (owner.hasTaken(t)) {

						Score score = Score.fetch(owner, t);

						String str = "<html><center><font size = \"10\"><p>" + t.getName() + ":  "
								+ new DecimalFormat().format(100 * score.grade() / score.possible()) + "%</p></font>";

						str += " Ring: " + score.ring + " (of " + t.bands + ") <br> Correct: " + score.correct
								+ "<br>Inorrect: " + score.incorrect + " <br>Blank: " + score.blank
								+ " <br><br>Final score: " + score.grade() + " of " + score.possible()
								+ " <br>Ungraded Points: " + score.getUngradedPoints() + " <br>Minutes taken: "
								+ score.mstime / 60000 + " <br>Times taken: " + owner.getTimesTaken(score.test)
								+ " <br></center></html>";

						display.setText(str);
					} else
						display.setText("<html><center><font size = \"10\"><p>" + t.getName() + "</p></font>"
								+ "<p>Test not yet taken</p></center></html>");
			}
		}
	}

	class ActivityPanel extends OptionsPane {
		private static final long serialVersionUID = 4479798989150095709L;

		ActivityPanel() {
		}

		public void update() {
		}
	}

	class ComplainPanel extends OptionsPane {
		private static final long serialVersionUID = -4387233098565339804L;

		ComplainPanel() {
		}

		public void update() {
		}
	}

	class GradingWrapper extends OptionsPane {
		private static final long serialVersionUID = 3068365677133100570L;

		GradingPanel gp;

		GradingWrapper() {
			setLayout(new BorderLayout());
			gp = new GradingPanel();
			add(gp, BorderLayout.CENTER);
		}

		public void update() {
		}
	}

	// Usefull classes follow.

	private class Loader implements Runnable {
		public void run() {
			cdp.setUser(owner);

			((TakeTestPanel) panels[0]).availableTests = Test.getAvailableTests(owner);
			((TakeTestPanel) panels[0]).taken = new ArrayList<Integer>();
			((TakeTestPanel) panels[0]).score = new ArrayList<Score>();

			for (Test t : ((TakeTestPanel) panels[0]).availableTests) {
				int a = owner.getTimesTaken(t);
				((TakeTestPanel) panels[0]).taken.add(a);
				((TakeTestPanel) panels[0]).score.add(a > 0 ? Score.fetch(owner, t) : null);
			}

			((TakeTestPanel) panels[0]).list.setListData(new Vector<String>(Test.getAvailableTestNames(owner)));

			((ViewScoresPanel) panels[2]).display.setText("<html><center><font size = \"7\">Select a Test<br>"
					+ "</font><br> If you cannot see any tests, the classes "
					+ "you are enrolled in have none.</center></html>");
			((GradingWrapper) panels[5]).gp.setOwner(owner);

			cdp.change.setVisible(owner != Administrator.SUPER_ADMINISTRATOR);

			for (OptionsPane p : panels)
				p.update();

		}
	}

	public static void main(String[] args) throws Exception {
		JudgementDriver.main(args);
	}
}
