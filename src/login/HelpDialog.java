package login;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import util.Methods;
import framework.User;

public class HelpDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1797724433146897379L;

	User user;
	JTextField answer = new JTextField();
	JLabel prompt = new JLabel("Answer: ");
	JLabel question = new JLabel("", JLabel.CENTER);
	JLabel clarify = new JLabel("Here is the question you set for yourself: ");
	JButton done = new JButton("Submit");
	JButton cancel = new JButton("Cancel");

	public HelpDialog(JFrame f) {
		super(f, "Help");

		getContentPane().setBackground(new Color(255, 255, 200));

		Methods.centerFrame(400, 150, this);
		setLayout(null);
		clarify.setBounds(10, 2, 360, 20);
		question.setBounds(10, 25, 360, 25);
		prompt.setBounds(10, 50, 60, 25);
		answer.setBounds(70, 50, 250, 25);
		done.setBounds(30, 80, 100, 25);
		cancel.setBounds(130, 80, 100, 25);

		done.addActionListener(this);
		cancel.addActionListener(this);
		answer.addActionListener(this);

		question.setFont(new Font("SERIF", Font.ITALIC, 15));
		question.setForeground(Color.RED);

		add(clarify);
		add(question);
		add(prompt);
		add(answer);
		add(done);
		add(cancel);
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == done || evt.getSource() == answer) {
			if (answer.getText().equals(user.answer))
				JOptionPane.showMessageDialog(null, "Your password is \""
						+ user.password + "\".");
			else {
				JOptionPane.showMessageDialog(null, "Incorrect answer.");
				return;
			}
		}
		setVisible(false);
	}

	public void setUser(User u) {
		question.setText(u.question);
		answer.setText("");
		user = u;
	}
}