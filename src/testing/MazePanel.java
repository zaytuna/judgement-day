package testing;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JPanel;

import framework.Question;

public class MazePanel extends JPanel implements MouseListener {
	private static final long serialVersionUID = -3565407921014431632L;
	public static Font FONT = new Font("SERIF", Font.BOLD, 20);

	public ArrayList<Option> options = new ArrayList<Option>();
	FontMetrics fm;
	public Font font = FONT;
	String text;
	int width = 0;
	public int phase = 0;// 0 = normal, 1 = stop further input, 2 = show
	// answers;
	boolean ready = false;

	public Question question;

	public MazePanel(FontMetrics fm, int width) {
		setBackground(Color.BLACK);
		addMouseListener(this);
		this.fm = fm;
		this.width = width;
	}

	public MazePanel(FontMetrics fm, int width, Question q) {
		this(fm, width);
		setQuestion(q);
	}

	public void setQuestion(Question q) {
		this.question = q;
		setText(q.text);
	}

	public void setText(String txt) {
		ready = false;
		text = txt;
		setup();
	}

	public void setup() {
		options.clear();
		String[] lines = text.split("\n");

		int placeholder = 0;

		for (int i = 0; i < lines.length; i++) {
			int x = width / 2 - fm.stringWidth(lines[i]) / 2;

			while (x < 10) {
				// insert newline
				text = text.substring(0, placeholder + lines[i].length() / 2) + "-\n-"
						+ text.substring(placeholder + lines[i].length() / 2);

				lines = text.split("\n");
				x = width / 2 - fm.stringWidth(lines[i]) / 2;
			}

			int index = 0, end = 0;
			while (lines[i].indexOf("[", end) >= 0) {
				index = lines[i].indexOf("[", end);

				x += fm.stringWidth(lines[i].substring(end, index + 1));

				end = lines[i].indexOf("]", index);

				String[] opt = lines[i].substring(index + 1, end).split(",");
				Option o = new Option();

				o.x = x;
				o.y = i * 20 + 15;
				o.h = 20;
				o.w = fm.stringWidth(lines[i].substring(index + 1, end));

				for (String s : opt)
					o.words.add(s);

				x += fm.stringWidth(lines[i].substring(index + 1, end));

				options.add(o);
			}
			placeholder += lines[i].length() + 1;
		}

		ready = true;

		setPreferredSize(new Dimension(600, lines.length * 20 + 30));
	}

	public void paintComponent(Graphics draw) {
		super.paintComponent(draw);

		if (text != null) {
			fm = draw.getFontMetrics(font);

			if (!ready)
				setup();

			Graphics2D g = (Graphics2D) draw;
			g.setFont(font);

			/*
			 * g.setStroke(new BasicStroke(3));
			 * for (Option o : options)
			 * for (PressWord word : o.words) {
			 * if (word.selected) {
			 * g.setColor(Color.RED);
			 * if (word.selected)
			 * g.drawOval(word.x, word.y, word.w, word.h);
			 * }
			 * }
			 * 
			 * g.setStroke(new BasicStroke(1));
			 */

			String[] lines = text.split("\n");
			int counter = 0;
			for (int i = 0; i < lines.length; i++) {
				int x = getWidth() / 2 - fm.stringWidth(lines[i]) / 2;

				int index = 0, end = 0;
				while (lines[i].indexOf("[", end) >= 0) {
					index = lines[i].indexOf("[", end);

					g.setColor(phase == 0 ? Color.WHITE : Color.gray);
					g.drawString(lines[i].substring(end == 0 ? 0 : end + 1, index), x, i * 20 + 30);
					x += fm.stringWidth(lines[i].substring(end, index + 1));

					end = lines[i].indexOf("]", index);
					drawOption(g, options.get(counter++));
					x += fm.stringWidth(lines[i].substring(index + 1, end + 1));
				}

				g.setColor(phase == 0 ? Color.WHITE : Color.GRAY);
				g.drawString(lines[i].substring(end == 0 ? 0 : end + 1), x, i * 20 + 30);
			}
		}
	}

	private void drawOption(Graphics2D g, Option o) {
		g.setColor(phase == 0 ? Color.GREEN : Color.GRAY);
		g.drawRect(o.x - 4, o.y, o.w + 8, o.h);

		int curX = o.x;

		for (int i = 0; i < o.words.size(); i++) {
			g.setColor(o.selected == i ? new Color(100, 100, 160) : Color.BLACK);
			g.fillRoundRect(curX, o.y + 3, fm.stringWidth(o.words.get(i)), o.h - 6, 5, 15);

			g.setColor(o.selected == i ? Color.WHITE : Color.GRAY);
			g.drawString(o.words.get(i), curX, o.y + 15);

			if (phase == 2 && o.correct == i) {
				g.setStroke(new BasicStroke(3));
				g.setColor(Color.GREEN);
				g.drawOval(curX, o.y, fm.stringWidth(o.words.get(i)), o.h);
				g.setStroke(new BasicStroke(1));
			} else if (phase == 2 && i == o.selected) {
				g.setStroke(new BasicStroke(3));
				g.setColor(Color.RED);
				g.drawLine(curX, o.y, curX + fm.stringWidth(o.words.get(i)), o.y + o.h);
				g.drawLine(curX, o.y + o.h, curX + fm.stringWidth(o.words.get(i)), o.y);

			}

			curX += fm.stringWidth(o.words.get(i) + ",");
		}

	}

	public class Option {
		public ArrayList<String> words = new ArrayList<String>();
		public int selected = -1, correct = -1;
		int x, y, w, h;

		public void setSelected(int i) {
			selected = i;
		}
	}

	public String getAnswer() {
		StringBuilder build = new StringBuilder();

		for (MazePanel.Option o : options)
			build.append("," + o.selected);
		if (build.length() > 0)
			build.deleteCharAt(0);

		return build.toString();
	}

	public void mouseReleased(MouseEvent evt) {
		if (phase == 0) {
			loop: for (Option o : options) {
				int curX = o.x;
				for (int i = 0; i < o.words.size(); i++) {

					if (evt.getX() >= curX && evt.getY() >= o.y + 3
							&& evt.getX() <= curX + fm.stringWidth(o.words.get(i)) && evt.getY() <= o.y + o.h - 3) {
						o.setSelected(i);
						break loop;
					}

					curX += fm.stringWidth(o.words.get(i) + ",");
				}
			}
			repaint();
		}
	}

	public void mousePressed(MouseEvent evt) {
	}

	public void mouseClicked(MouseEvent evt) {
	}

	public void mouseEntered(MouseEvent evt) {
	}

	public void mouseExited(MouseEvent evt) {
	}

	public void finish() {
		setBackground(new Color(0, 0, 60));
		phase = 1;
	}

	public void showAnswers() {
		System.out.println("SHOWING!!!");
		phase = 2;

		String[] asdf = question.correct.split(",");
		for (int i = 0; i < options.size(); i++)
			options.get(i).correct = Integer.parseInt(asdf[i]);
	}

	public int grade() {
		boolean blank = true;
		for (Option o : options)
			if (o.selected >= 0)
				blank = false;

		if (blank)
			return question.blankPolicy;

		double score = 0;
		String[] asdf = question.correct.split(",");

		if (asdf.length != options.size())
			throw new IllegalArgumentException();
		for (int i = 0; i < options.size(); i++) {
			if (Integer.parseInt(asdf[i]) == options.get(i).selected)
				score++;
		}

		return (int) (question.worth * score / options.size());
	}

	public void loadAnswers(String ans) {
		System.out.println("Loading: " + ans);
		String[] asdf = ans.split(",");

		setup();

		for (int i = 0; i < options.size(); i++) {
			System.out.println(i + ": " + asdf[i]);
			options.get(i).selected = Integer.parseInt(asdf[i]);
		}
	}
}
