package testing;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import util.HTMLCreator;
import util.Methods;
import framework.JudgementDriver;
import framework.Question;

public class MatcherPanel extends JPanel implements MouseListener, MouseMotionListener {
	public static final Paint BLANK_COLOR = new GradientPaint(new Point(0, 0), new Color(255, 255, 255, 150),
			new Point(50, 0), new Color(0, 0, 0, 0), true);
	private static final long serialVersionUID = -4297595237027222646L;
	public int SIZE = 30;

	private Selection selected, mouseOver;
	private Point to;
	boolean finished = false;

	public JLabel text = new JLabel();
	public int fifty = 50;

	public ArrayList<String> left = new ArrayList<String>(), right = new ArrayList<String>();

	Question question;

	public ArrayList<Connection> connections = new ArrayList<Connection>();

	public MatcherPanel() {
		super(true);
		addMouseMotionListener(this);
		addMouseListener(this);
		setPreferredSize(new Dimension(600, 300));
		setLayout(new BorderLayout());
		setOpaque(false);
		text.setHorizontalAlignment(JLabel.CENTER);
		add(text, BorderLayout.NORTH);
		text.setFont(JudgementDriver.uioptions.questionFont);
	}

	public void setMixAndMatchTest(Question q, boolean scramble) {
		this.question = q;
		this.left = Methods.getList(q.getLeftMatchingSide());
		this.right = Methods.getList(q.getRightMatchingSide());

		if (scramble)
			scramble();

		text.setText(HTMLCreator.getHTML(q.text));
		if (!q.image.equals("NONE"))
			text.setIcon(new ImageIcon("Links/" + q.image));

		setPreferredSize(new Dimension(600, SIZE * Math.max(left.size(), right.size()) + 90));
	}

	public void show() {
		finished = true;

		for (Connection c : connections)
			c.color = new Color(250, 0, 0, 150);

		String[] correct = question.correct.split(":"), leftSide = question.getLeftMatchingSide(), rightSide = question
				.getRightMatchingSide();
		for (int i = 0; i < correct.length; i += 2) {
			for (Connection c : connections) {
				if (left.get(c.a).equals(leftSide[Integer.parseInt(correct[i])])
						&& right.get(c.b).equals(rightSide[Integer.parseInt(correct[i + 1])]))
					c.color = new Color(0, 250, 0, 150);

			}
		}

		for (int i = 0; i < correct.length; i += 2) {
			boolean used = false;

			for (Connection c : connections)
				if (left.get(c.a).equalsIgnoreCase(leftSide[Integer.parseInt(correct[i])]))
					if (right.get(c.b).equalsIgnoreCase(rightSide[Integer.parseInt(correct[i + 1])]))
						used = true;

			if (!used) {
				Connection c = new Connection(left.indexOf(leftSide[Integer.parseInt(correct[i])]), right
						.indexOf(rightSide[Integer.parseInt(correct[i + 1])]));
				c.color = BLANK_COLOR;
				connections.add(c);
			}

		}

		repaint();
	}

	public void scramble() {// VERY DANGEROUS
		for (int i = 0; i < left.size(); i++) {
			int newIndex = (int) (Math.random() * left.size());
			for (Connection c : connections) {
				if (c.a == i)
					c.a = newIndex;
				else if (c.a == newIndex)
					c.a = i;
			}

			String temp = left.get(i);
			left.set(i, left.get(newIndex));
			left.set(newIndex, temp);
		}

		for (int i = 0; i < right.size(); i++) {
			int newIndex = (int) (Math.random() * right.size());
			for (Connection c : connections) {
				if (c.b == i)
					c.b = newIndex;
				else if (c.b == newIndex)
					c.b = i;
			}
			String temp = right.get(i);
			right.set(i, right.get(newIndex));
			right.set(newIndex, temp);
		}
	}

	public void mousePressed(MouseEvent evt) {
		if (!finished)
			selected = getNumberForPos(evt.getPoint());
	}

	public int isCorrect() {
		if (question.spec == 2)
			return -1;
		if (connections.size() == 0)
			return 2;

		int numCorrect = 0;

		String[] correct = question.correct.split(":"), leftSide = question.getLeftMatchingSide(), rightSide = question
				.getRightMatchingSide();
		for (int i = 0; i < correct.length; i += 2) {
			for (Connection c : connections) {
				if (c.color == BLANK_COLOR)
					continue;
				if (question.spec == 0) {
					if (left.get(c.a).equalsIgnoreCase(leftSide[Integer.parseInt(correct[i])]))
						if (right.get(c.b).equalsIgnoreCase(rightSide[Integer.parseInt(correct[i + 1])]))
							numCorrect++;
				} else if (question.spec == 1) {
					if (left.get(c.a).equals(leftSide[Integer.parseInt(correct[i])]))
						if (right.get(c.b).equals(rightSide[Integer.parseInt(correct[i + 1])]))
							numCorrect++;
				}

			}
		}

		int numExtra = connections.size() - numCorrect;

		if (correct.length / 2 == numCorrect && numExtra == 0)
			return 0;
		return 1;
	}

	public int grade() {
		if (question.spec == 2)
			return 0;
		if (connections.size() == 0)
			return question.blankPolicy;

		int numCorrect = 0;

		String[] correct = question.correct.split(":"), leftSide = question.getLeftMatchingSide(), rightSide = question
				.getRightMatchingSide();
		for (int i = 0; i < correct.length; i += 2) {
			for (Connection c : connections) {
				if (c.color == BLANK_COLOR)
					continue;
				if (question.spec == 0) {
					if (left.get(c.a).equalsIgnoreCase(leftSide[Integer.parseInt(correct[i])]))
						if (right.get(c.b).equalsIgnoreCase(rightSide[Integer.parseInt(correct[i + 1])]))
							numCorrect++;
				} else if (question.spec == 1) {
					if (left.get(c.a).equals(leftSide[Integer.parseInt(correct[i])]))
						if (right.get(c.b).equals(rightSide[Integer.parseInt(correct[i + 1])]))
							numCorrect++;
				}

			}
		}

		int numExtra = connections.size() - numCorrect;

		return (Math.max(0, numCorrect - numExtra) * question.worth) / (correct.length / 2);
	}

	public void mouseReleased(MouseEvent evt) {
		if (!finished) {
			to = evt.getPoint();
			if (selected != null) {
				boolean good = true;

				if (getNumberForPos(to) == null)
					good = false;
				else
					for (Connection c : connections)
						if (c.is(selected, getNumberForPos(to)))
							good = false;

				if (good && selected.side ^ getNumberForPos(to).side) {
					Connection c = new Connection(selected, getNumberForPos(to));
					c.color = Methods.randomColorRange(connections.size(), 150, 150, 255);
					connections.add(c);
				}

				to = null;
				selected = null;
				mouseOver = null;
				repaint();

			}
		}
	}

	private Selection getNumberForPos(Point p) {
		if (p.x < 120)
			for (int i = 0; i < left.size(); i++)
				if (p.y < fifty + i * SIZE + SIZE + 5 && p.y > fifty + i * SIZE + 5)
					return new Selection(i, true);
		if (p.x > 200)
			for (int i = 0; i < right.size(); i++)
				if (p.y < fifty + i * SIZE + SIZE + 5 && p.y > fifty + i * SIZE + 5)
					return new Selection(i, false);

		return null;
	}

	public void mouseEntered(MouseEvent evt) {
	}

	public void mouseMoved(MouseEvent evt) {
	}

	public void mouseDragged(MouseEvent evt) {
		if (!finished) {
			to = evt.getPoint();
			mouseOver = getNumberForPos(evt.getPoint());
			repaint();
		}
	}

	public void mouseExited(MouseEvent evt) {
	}

	public void mouseClicked(MouseEvent evt) {
		if (!finished)
			if (evt.getClickCount() == 2) {
				Selection s = getNumberForPos(evt.getPoint());
				for (int i = 0; i < connections.size(); i++) {
					if (connections.get(i).has(s)) {
						connections.remove(i);
						i--;
					}
				}

				repaint();
			}
	}

	public void paintComponent(Graphics asdf) {
		super.paintComponent(asdf);

		Graphics2D g = (Graphics2D) asdf;
		g.setColor(Color.BLACK);

		g.setFont(JudgementDriver.uioptions.questionFont);

		for (int i = 0; i < connections.size(); i++) {
			g.setPaint(connections.get(i).color);

			g.fillRect(200, fifty + connections.get(i).b * SIZE + SIZE / 2, 800, SIZE);
			g.fillRect(20, fifty + connections.get(i).a * SIZE + SIZE / 2, 100, SIZE);

			if (connections.get(i).color == BLANK_COLOR) {
				g
						.setStroke(new BasicStroke(3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER, 10,
								new float[] { 5 }, 0));
				g.setColor(Color.BLACK);
			} else
				g.setStroke(new BasicStroke(5));

			g.drawLine(120, fifty + connections.get(i).a * SIZE + SIZE, 200, fifty + connections.get(i).b * SIZE + SIZE);
			g.setStroke(new BasicStroke(1));

			// g.setColor(Color.black);
			// g.fillRect(200, fifty + connections.get(i).b * size + 10, 800,
			// size);
			// g.fillRect(20, fifty + connections.get(i).a * size + 10, 100, size);
		}

		for (int i = 0; i < left.size(); i++) {
			g.setColor(Methods.randomColorRange(connections.size(), 150, 150, 255));
			if (mouseOver != null && mouseOver.val == i && mouseOver.side && selected != null)
				g.fillRect(20, fifty + i * SIZE + SIZE / 2, 100, SIZE);

			if (selected != null && selected.val == i && selected.side)
				g.fillRect(20, fifty + i * SIZE + SIZE / 2, 100, SIZE);

			g.setColor(Color.BLACK);
			g.drawString(left.get(i), 20, i * SIZE + fifty + SIZE + 5);

			// g.setColor(new Color(35, 166, 214, 56));

		}

		for (int i = 0; i < right.size(); i++) {

			g.setColor(Methods.randomColorRange(connections.size(), 150, 150, 255));
			if (mouseOver != null && mouseOver.val == i && !mouseOver.side && selected != null)
				g.fillRect(200, fifty + i * SIZE + SIZE / 2, 800, SIZE);

			if (selected != null && selected.val == i && !selected.side)
				g.fillRect(200, fifty + i * SIZE + SIZE / 2, 800, SIZE);

			g.setColor(Color.BLACK);
			g.drawString(right.get(i), 200, fifty + i * SIZE + SIZE + 5);

			// g.setColor(new Color(35, 166, 214, 56));

		}

		g.setStroke(new BasicStroke(5));

		// g.setColor(Color.BLUE);
		g.setColor(Methods.randomColorRange(connections.size(), 150, 150, 255));
		if (selected != null && to != null)
			g.drawLine(selected.side ? 120 : 200, fifty + selected.val * SIZE + SIZE, to.x, to.y);

		g.setStroke(new BasicStroke(1));
	}

	public class Connection {
		public int a, b;
		public Paint color;

		public Connection(int a, int b) {
			this.a = a;
			this.b = b;
		}

		Connection(Selection a, Selection b) {
			if (a.side) {
				this.a = a.val;
				this.b = b.val;
			} else {
				this.a = b.val;
				this.b = a.val;
			}
		}

		public boolean is(int a, int b) {
			return a == this.a && b == this.b;
		}

		public boolean has(Selection a) {
			if (a == null)
				return false;
			return (a.side && (a.val == this.a)) || (!a.side && (a.val == this.b));
		}

		public boolean is(Selection a, Selection b) {
			return (a.side ? a.val == this.a : a.val == this.b) && (b.side ? b.val == this.a : b.val == this.b);
		}
	}

	class Selection {
		boolean side;
		int val;

		Selection(int a, boolean b) {
			this.val = a;
			this.side = b;
		}

		public boolean is(int a, boolean b) {
			return a == this.val && b == this.side;
		}
	}
}
