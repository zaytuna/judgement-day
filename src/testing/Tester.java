package testing;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import util.Encoder;
import util.Methods;
import util.RadialColorful;
import util.Timer;
import framework.GradeManager;
import framework.JudgementDriver;
import framework.MenuListener;
import framework.Question;
import framework.Score;
import framework.Test;
import framework.User;
import framework.Question.QuestionComponent;

public class Tester extends RadialColorful implements ActionListener, Runnable {
	private static final long serialVersionUID = 1166382349570506630L;
	Test test;
	ArrayList<DisplayQuestionsPanel> pages = new ArrayList<DisplayQuestionsPanel>();
	Handler handler = new Handler();

	int currentPage = 0, questionTo = -1;
	int pos = 0;
	int task = 0;// -1 = back, 1 = foreward, 2 = goTo

	User current = null;

	JPanel main = new JPanel();
	JScrollPane scroll = new JScrollPane(main);
	JProgressBar progress = new JProgressBar(JProgressBar.HORIZONTAL, 0, 100);
	JButton back = new JButton("Back", new ImageIcon("Resources/Other/SMALL_LEFT.png"));
	JButton foreward = new JButton("Next", new ImageIcon("Resources/Other/SMALL_RIGHT.png"));
	JButton finish = new JButton("Done", new ImageIcon("Resources/exit.png"));
	JButton quit = new JButton("Back to Menu");
	JPanel south = new JPanel();

	TabComponent tabPanel = new TabComponent();
	// ArrayList<JButton> tabs = new ArrayList<JButton>();

	JLabel scoreFinal = new JLabel();
	JLabel pageNumLabel = new JLabel();
	Timer timer = new Timer();

	MenuListener owner;
	int phase = 0;
	boolean running = false;

	public Tester(MenuListener m) {
		super(300, new Point(0, 0), new Point(140, 140), new Color(0, 150, 0), new Color(0, 0, 150),
				new Color(0, 0, 0), JudgementDriver.uioptions.testColor);

		if (!JudgementDriver.uioptions.marks) {
			gradients = false;
			paint();
		}

		owner = m;
		setLayout(null);
		setBackground(Color.BLACK);

		main.setLayout(null);

		main.setOpaque(false);
		scroll.setOpaque(false);
		scroll.getViewport().setOpaque(false);

		progress.setStringPainted(true);
		progress.setString("Progress");

		south.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
		
		back.setPreferredSize(new Dimension(120, 40));
		progress.setPreferredSize(new Dimension(500, 40));
		foreward.setPreferredSize(new Dimension(120, 40));
		quit.setPreferredSize(new Dimension(140, 40));
		finish.setPreferredSize(new Dimension(120, 40));

		back.addActionListener(this);
		foreward.addActionListener(this);
		finish.addActionListener(this);
		timer.addActionListener(this);
		quit.addActionListener(this);

		back.setFocusPainted(false);
		quit.setFocusPainted(false);
		foreward.setFocusPainted(false);
		finish.setFocusPainted(false);

		south.add(quit);
		south.add(back);
		south.add(progress);
		south.add(foreward);
		south.add(finish);
		south.add(timer);

		scroll.setBorder(null);
		scroll.getVerticalScrollBar().setUnitIncrement(16);

		south.setBorder(new BevelBorder(BevelBorder.RAISED));

		tabPanel.setOpaque(false);

		add(scroll);
		add(south);
		add(tabPanel);
		add(scoreFinal);
		add(pageNumLabel);

		setComponentZOrder(scoreFinal, 0);
		setComponentZOrder(pageNumLabel, 1);
		scoreFinal.setFont(new Font("SANS_SERIF", Font.BOLD, 50));
		pageNumLabel.setFont(new Font("SANS_SERIF", Font.BOLD, 20));
		scoreFinal.setForeground(JudgementDriver.uioptions.reveiewForeground);

		reset();
	}

	public void setBounds(int a, int b, int c, int d) {
		super.setBounds(a, b, c, d);
		scroll.setBounds(0, 0, c, d - 135);
		south.setBounds(10, d - 65, c - 20, 60);
		tabPanel.setBounds(10, d - 135, c - 20, 70);
		pageNumLabel.setBounds(c * 15 / 16, 0, c / 16, 50);
		scoreFinal.setBounds(c - 200, d - 200, 200, 130);
	}

	public void run() {
		if (!running) {
			running = true;

			switch (task) {
			case -1:// back
				// move scroll to right (pos ->getWidth())
				// switch pages
				// teleport to far left (pos = -getWidth())
				// move scroll to midle (pos-> 0)
				while (pos < getWidth()) {
					pos += (pos + 5) / 5;
					scroll.setLocation(pos, 0);
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				if (currentPage > 0) {
					setPage(--currentPage);
					foreward.setEnabled(true);
				}

				if (currentPage <= 0)
					back.setEnabled(false);

				pos = -getWidth();

				while (pos < 0) {
					pos += (5 - pos) / 5;
					scroll.setLocation(pos, 0);
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				pos = 0;
				scroll.setLocation(0, 0);

				break;
			case 1:// foreward
				while (pos > -getWidth()) {
					pos += (-5 + pos) / 5;
					scroll.setLocation(pos, 0);
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

				if (currentPage < pages.size() - 1) {
					setPage(++currentPage);
					if (test.canGoBack)
						back.setEnabled(true);
				}

				if (currentPage >= pages.size() - 1)
					foreward.setEnabled(false);
				pos = getWidth();

				while (pos > 0) {
					pos += (-5 - pos) / 5;
					scroll.setLocation(pos, 0);
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				pos = 0;
				scroll.setLocation(0, 0);
				break;
			case 2:
				if (getPageForQuestion(questionTo) == currentPage || questionTo == -1)
					break;
				while (pos > -getHeight()) {
					pos += (-5 + pos) / 5;
					scroll.setLocation(0, pos);
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

				int hi = getPageForQuestion(questionTo);
				setPage(hi);

				back.setEnabled(hi > 0);
				foreward.setEnabled(hi < pages.size() - 1);

				while (pos < 0) {
					pos += (5 - pos) / 5;
					scroll.setLocation(0, pos);
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				pos = 0;
				scroll.setLocation(0, 0);
				break;
			}
			running = false;
		}
	}

	public void colorQuestions() {
		for (DisplayQuestionsPanel d : pages) {
			for (int i = 0; i < d.questionsPerPage; i++)
				d.slots[i].setBorder(new LineBorder((d.startQ + i == questionTo) ? Color.ORANGE : Color.BLACK,
						(d.startQ + i == questionTo) ? 5 : 2));
		}

	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == back) {
			task = -1;
			new Thread(this).start();
		} else if (evt.getSource() == foreward) {
			task = 1;
			new Thread(this).start();
		} else if (evt.getSource() == finish || evt.getSource() == timer) {
			
			quit.setEnabled(true);

			if (evt.getSource() == timer
					|| JOptionPane.showConfirmDialog(this, "Submit final answers?") == JOptionPane.YES_OPTION) {
				timer.pause();
				phase = 1;
				if (test.showAnswerMode != 3) {
					setAll(500, new Point(0, 0), new Point(140, 140), Color.WHITE, new Color(100, 0, 0), new Color(250,
							200, 150), JudgementDriver.uioptions.reviewColor);
					south.setBackground(JudgementDriver.uioptions.reviewColor);

					paint();
				}

				String[] answers = new String[test.questions.size()];
				int[] sc = new int[test.questions.size()];
				int c = 0, i = 0, b = 0, tot = 0;

				for (DisplayQuestionsPanel d : pages)
					for (int v = 0; v < d.slots.length; v++) {
						QuestionComponent q = d.slots[v];
						sc[tot] = q.grade();

						if (q.question.teacherMustGrade()) {
							GradeManager.offer(current.id, test.getName(), tot);
							q.saveToFile(new File("Links/Pending/"
									+ Encoder.encode(current.id + "-" + test.getName() + "-" + tot) + ".jpg"));
						} else {
							switch (q.isCorrect()) {
							case 0:
								c++;
								break;
							case 1:
								i++;
								break;
							case 2:
								b++;
								break;
							default:
								break;
							}
						}

						answers[tot++] = q.getAnswer();
						q.show(test.showAnswerMode);

						if (test.showAnswerMode == 1)
							// tabs.get(v + d.startQ).setBackground(
							tabPanel.qstate.get(d.pageIndex).set(v,
									getQuestionColor(q.isCorrect(), q.grade(), d.questions[v].worth));

						if (test.showAnswerMode == 1)
							d.feedback[v].setText(q.grade() + " of " + d.questions[v].worth);
					}

				Score score = new Score(test, c, i, b, tot, answers, sc);
				score.setTime(timer.getTime());
				Score.recordScore(score, current, test);
				System.out.println(score + "\t" + score.mstime);
				System.out.println(timer.getForeground());
				timer.setForeground(JudgementDriver.uioptions.reveiewForeground);
				pageNumLabel.setForeground(JudgementDriver.uioptions.reveiewForeground);

				if (test.showAnswerMode != 3)
					scoreFinal.setText((int)(100 * score.grade() / score.possible()) + "%");

				if (test.showAnswerMode == 3)
					owner.toMenu();

				finish.setEnabled(false);
			}
		} else if (evt.getSource() == quit) {
			owner.toMenu();
		}
	}

	private Color getQuestionColor(int correct, int grade, int worth) {
		switch (correct) {
		default:
		case 0:
		case 1:
			return (Methods.colorMeld(Color.RED, Color.GREEN, grade / worth));
		case 2:
			return Color.black;
		case -1:
			return Color.white;
		}
	}

	public int getPageForQuestion(int i) {
		for (DisplayQuestionsPanel d : pages)
			if (d.startQ + d.questionsPerPage > i)
				return d.pageIndex;
		throw new IllegalArgumentException("WTF? NOT ON PAGE!");
	}

	public void setUser(User u) {
		this.current = u;
	}

	public void updateProgress() {
		if (phase == 0) {
			int total = 0, checked = 0;
			for (DisplayQuestionsPanel d : pages)
				for (int i = 0; i < d.slots.length; i++) {
					checked++;
					if (d.slots[i].hasStarted()) {
						// tabs.get(d.startQ + i).setBackground(
						tabPanel.qstate.get(d.pageIndex).set(
								i,
								new Color((int) (JudgementDriver.uioptions.testColor.getRed() * .265),
										(int) (JudgementDriver.uioptions.testColor.getGreen() * .355),
										(int) (JudgementDriver.uioptions.testColor.getRed() * .56)));
						total++;
					} else
						tabPanel.qstate.get(d.pageIndex).set(i,
								Methods.colorMeld(JudgementDriver.uioptions.testColor, Color.BLACK, .1));
					// tabs.get(d.startQ +
					// i).setBackground(JudgementDriver.uioptions.testColor);
				}

			if (progress.getValue() != (total * 100) / checked)
				questionTo = -1;

			colorQuestions();

			progress.setValue((total * 100) / checked);
		}
	}

	public class Handler extends MouseAdapter implements ActionListener, MouseListener, KeyListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			updateProgress();
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			updateProgress();
		}

		@Override
		public void keyPressed(KeyEvent arg0) {
		}

		@Override
		public void keyReleased(KeyEvent arg0) {
			updateProgress();
		}

		@Override
		public void keyTyped(KeyEvent arg0) {
			updateProgress();
		}

	}

	public void setPage(int index) {
		currentPage = index;
		pageNumLabel.setText("Page " + (index + 1));
		for (int i = 0; i < pages.size(); i++) {
			pages.get(i).setVisible(i == index);
		}
		tabPanel.setSelected(index);

		// for (int i = 0; i < tabs.size(); i++)
		// tabs.get(i).setEnabled(getPageForQuestion(i) != index);
	}

	public void startTest(Test t) {
		main.removeAll();
		this.test = t;

		pages.clear();

		int yAt = 40, page = 0, countComponents = 0, lastIndex = -1;
		boolean nextPage = false;

		tabPanel.removeAll();

		// JPanel mini = new JPanel(new FlowLayout(FlowLayout.CENTER));

		for (int i = 0; i < t.questions.size(); i++) {
			JButton numB = new JButton("<html>" + i + "</html>");
			numB.setFocusPainted(false);
			numB.setPreferredSize(new Dimension(30, 23));
			numB.setBorder(null);
			// numB.setBackground(JudgementDriver.uioptions.testColor);
			// tabs.add(numB);
			// mini.add(numB);
			numB.addActionListener(this);

			int temp = t.questions.get(i).getPreferredHeight();
			int temp2 = ((i + 1 < t.questions.size()) ? t.questions.get(i + 1).getPreferredHeight() : 0);

			switch (t.displayMode) {

			case -1:
				nextPage = yAt + temp + temp2 > scroll.getHeight();
				break;
			case -2:
				nextPage = false;
				break;
			default:
				nextPage = countComponents > t.displayMode;
				break;
			}

			if (nextPage || i == t.questions.size() - 1) {
				// mini.setBorder(new TitledBorder(new LineBorder(Color.BLACK,
				// 2), "Page " + (page + 1)));
				// mini.setOpaque(false);
				// tabPanel.add(mini);

				// mini = new JPanel(new FlowLayout(FlowLayout.CENTER));
				tabPanel.addPage(lastIndex + 1, i + 1);

				DisplayQuestionsPanel dqp = new DisplayQuestionsPanel(page++, i - lastIndex);
				dqp.setBounds(0, 0, getWidth() - 30, Math.max(yAt + temp2 + 220, main.getHeight()));
				dqp.setQuestions(lastIndex + 1, Methods.createSubArray(t.questions, lastIndex + 1, i + 1,
						new Question[i - lastIndex]));
				main.setPreferredSize(new Dimension(getWidth() - 30, Math.max(yAt, main.getPreferredSize().height)));

				pages.add(dqp);

				main.add(dqp);
				yAt = 40;
				countComponents = 0;
				lastIndex = i;
			}

			if (!nextPage || i == t.questions.size() - 1) {
				yAt += temp + 20;
				countComponents++;
			}
		}
		setPage(0);

		back.setEnabled(false);
		foreward.setEnabled(pages.size() > 1);
		updateProgress();

		timer.clear();
		if (test.time > 0)
			timer.setMax(test.time);
		timer.start();
	}

	private class TabComponent extends JPanel implements Runnable, MouseListener {
		private static final long serialVersionUID = -1950971989177502099L;
		int pos, posTo, selectPos;// middle in pixels

		public ArrayList<ArrayList<Color>> qstate = new ArrayList<ArrayList<Color>>();
		int selected, pressed = -1;

		public TabComponent() {
			new Thread(this).start();
			addMouseListener(this);
		}

		public void setSelected(int page) {
			selected = page;
		}

		public void addPage(int startQ, int endQ) {
			ArrayList<Color> list = new ArrayList<Color>();
			for (int i = startQ; i < endQ; i++)
				list.add(JudgementDriver.uioptions.testColor.darker());
			qstate.add(list);

			posTo = (getWidth() - (endQ * 20 + 30 * pages.size())) / 2;
			repaint();
		}

		private int index(Point p) {
			int curX = 0, counter = 0;

			for (int index = 0; index < qstate.size(); index++) {
				for (int i = 0; i < qstate.get(index).size(); i++) {
					if ((p.x >= pos + curX + i * 20 + 11) && (p.y >= 24) && (p.x <= 18 + pos + curX + i * 20 + 11)
							&& (p.y <= getHeight() - 24))
						return counter;
					counter++;
				}
				curX += qstate.get(index).size() * 20 + 30;
			}

			return -1;
		}

		public void mouseReleased(MouseEvent evt) {
			int num = index(evt.getPoint());

			if (num >= 0) {
				questionTo = num;
				task = 2;
				new Thread(Tester.this).start();
				pressed = -1;
				colorQuestions();
			}
		}

		public void paintComponent(Graphics g3) {
			Graphics2D g = (Graphics2D) g3;

			super.paintComponent(g);

			int curX = 0;
			for (int index = 0; index < qstate.size(); index++) {
				int page = qstate.get(index).size();

				g.setFont(new Font("SANS_SERIF", Font.PLAIN, 15));
				g.setColor(index == selected ? Color.ORANGE.darker()
						: (phase == 0 ? JudgementDriver.uioptions.testForeground
								: JudgementDriver.uioptions.reveiewForeground));
				g.setStroke(new BasicStroke(index == selected ? 3 : 1));

				g.drawRect(pos + curX, 20, page * 20 + 20, getHeight() - 40);

				if (index == selected) {
					g.fillPolygon(new int[] { pos + curX + page * 10, pos + curX + page * 10 + 10,
							pos + curX + page * 10 + 20 }, new int[] { getHeight() - 5, getHeight() - 15,
							getHeight() - 5 }, 3);
					selectPos = curX + page * 10 + 10;
				}
				// if (pressed >= 0 && index == getPageForQuestion(pressed)) {
				// g.setColor(Color.BLACK);
				// g.fillRect(pos + curX, 20, page * 20 + 20, getHeight() - 40);
				// }

				g.drawString("Page " + (index + 1), pos + curX, 18);

				for (int i = 0; i < page; i++) {
					g.setColor(pages.get(index).startQ + i == questionTo ? Methods.colorMeld(Color.ORANGE, qstate.get(
							index).get(i), .2) : (pages.get(index).startQ + i == pressed ? Color.BLACK : qstate.get(
							index).get(i)));
					g.fillRect(pos + curX + i * 20 + 11, 24, 18, getHeight() - 48);
				}

				curX += page * 20 + 30;
			}
		}

		public void run() {
			while (true) {
				try {
					Thread.sleep(40);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (Methods.mouse().y > getY() && Methods.mouse().y < getY() + getHeight()) {
					if (Methods.mouse().x > getWidth() * 99 / 100)
						posTo -= 15;
					else if (Methods.mouse().x < getWidth() / 100)
						posTo += 15;
				} else
					posTo = getWidth() / 2 - selectPos;

				pos += (posTo - pos) / 5;
				repaint();
			}
		}

		public void mouseClicked(MouseEvent arg0) {
		}

		public void mouseEntered(MouseEvent arg0) {
		}

		public void mouseExited(MouseEvent arg0) {
		}

		public void mousePressed(MouseEvent evt) {
			pressed = index(evt.getPoint());
		}
	}

	private class DisplayQuestionsPanel extends JPanel {
		private static final long serialVersionUID = 5155888322001936665L;

		int pageIndex, questionsPerPage, startQ;

		Question.QuestionComponent[] slots;
		JLabel[] feedback;

		Question[] questions;

		DisplayQuestionsPanel(int pi, int qpp) {
			pageIndex = pi;
			setOpaque(false);
			questionsPerPage = qpp;
			feedback = new JLabel[qpp];
			slots = new Question.QuestionComponent[qpp];
			setLayout(null);

			// setBackground(Methods.randomColor(pi, 100));
		}

		public void setQuestions(int start, Question... q) {
			startQ = start;
			questions = q;
			feedback = new JLabel[q.length];

			for (Component c : slots)
				if (c != null)
					remove(c);

			int yAt = 0;

			slots = new Question.QuestionComponent[q.length];

			for (int i = 0; i < q.length; i++) {
				slots[i] = q[i].createDisplay(getWidth() * 7 / 8, q[i].getPreferredHeight());
				slots[i].setBorder(new LineBorder(Color.BLACK, 2));
				slots[i].setBounds(getWidth() / 16, 20 + yAt, getWidth() * 7 / 8, q[i].getPreferredHeight());
				feedback[i] = new JLabel();
				feedback[i].setHorizontalAlignment(JLabel.CENTER);
				feedback[i].setFont(new Font("SANS_SERIF", Font.BOLD + Font.ITALIC, 16));
				feedback[i].setForeground(JudgementDriver.uioptions.reveiewForeground);
				feedback[i].setBounds(15 * getWidth() / 16, 20 + yAt, getWidth() / 16, q[i].getPreferredHeight());
				// slots[i].setBackground(Methods.randomColor(i * 42 + 3, 255));
				yAt += slots[i].getHeight() + 20;
				add(slots[i]);
				add(feedback[i]);
				slots[i].addListeners(handler);
			}
		}
	}

	public void reset() {
		currentPage = 0;
		progress.setValue(0);
		finish.setEnabled(true);
		pages.clear();
		scoreFinal.setText("");
		quit.setEnabled(false);
		pageNumLabel.setForeground(JudgementDriver.uioptions.testForeground);
		setAll(300, new Point(0, 0), new Point(140, 140), new Color(0, 150, 0), new Color(0, 0, 150),
				new Color(0, 0, 0), JudgementDriver.uioptions.testColor);
		south.setBackground(JudgementDriver.uioptions.testColor);
		phase = 0;
		current = null;
		timer.clear();
		main.setPreferredSize(new Dimension(getWidth() - 10, getHeight() - 145));
		timer.setForeground(JudgementDriver.uioptions.testForeground);
		tabPanel.qstate.clear();
		test = null;
	}
}