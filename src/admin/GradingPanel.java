package admin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import testing.MatcherPanel;
import testing.MazePanel;
import util.Encoder;
import util.Methods;
import util.TransperencyFilter;
import framework.GradeManager;
import framework.Question;
import framework.Test;
import framework.User;
import framework.GradeManager.GradingTask;

public class GradingPanel extends Box implements ActionListener {
	private static final long serialVersionUID = 1871540111808736385L;

	JPanel buttons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
	JTextField score = new JTextField();
	JButton skip = new JButton("Skip");
	JButton submit = new JButton("Submit");
	JButton start = new JButton("Refresh Queue");
	JButton end = new JButton("End");
	Question question;

	User owner = null;

	GradingTask task;

	public GradingPanel() {
		super(BoxLayout.Y_AXIS);
		setBackground(new Color(220, 220, 240));
		setBorder(new LineBorder(Color.black));
		setOpaque(true);

		start.setAlignmentX(0.5f);
		end.setAlignmentX(0.5f);
		skip.setAlignmentX(1f);
		score.setAlignmentX(0.5f);

		start.setPreferredSize(new Dimension(500, 35));

		add(Box.createGlue());
		add(start);
		add(Box.createGlue());

		buttons.add(submit);
		buttons.setOpaque(false);
		buttons.add(skip);
		buttons.add(end);

		score.setMaximumSize(new Dimension(150, 25));
		score.setPreferredSize(new Dimension(150, 25));

		submit.addActionListener(this);
		score.addActionListener(this);
		skip.addActionListener(this);
		start.addActionListener(this);
		end.addActionListener(this);
	}

	public void setOwner(User u) {
		owner = u;

		removeAll();
		add(Box.createGlue());
		add(start);
		add(Box.createGlue());
	}

	public void newTask() {
		removeAll();
		task = GradeManager.requestTask(owner);

		if (task == null) {

			add(createGlue());
			add(start);
			JLabel label = new JLabel("No more grading!");
			label.setAlignmentX(0.5f);
			label.setFont(new Font("SANS_SERIF", Font.BOLD, 30));
			add(label);
			add(createGlue());
		} else {
			File fileRef;

			question = Test.fromName(task.testName).questions.get(task.questionNumber);
			labelize("Question: ");
			JTextArea lbl = new JTextArea(question.text);
			lbl.setEditable(false);
			lbl.setLineWrap(true);
			lbl.setWrapStyleWord(true);
			lbl.setOpaque(false);
			lbl.setBorder(null);
			lbl.setAlignmentX(0.5f);
			add(lbl);
			labelize("His/her answer:");
			add(createGlue());

			fileRef = new File("Links/Pending/"
					+ Encoder.encode(task.userID + "-" + task.testName + "-" + task.questionNumber) + ".jpg");

			try {
				add(new AnswerDisplay(question, Methods.getFileContents(fileRef).trim(), fileRef));
			} catch (Exception e) {
			}

			labelize("Rubric/Guidelines:");
			add(createGlue());

			fileRef = new File("Links/Keys/" + question.correct);
			if (fileRef.exists())
				add(new AnswerDisplay(question, Methods.getFileContents(fileRef).trim(), fileRef));
			else
				add(new AnswerDisplay(question, question.correct, fileRef));

			add(createGlue());

			labelize("Points for correct: " + question.worth + "    for blank: " + question.blankPolicy);
			add(score);
			add(buttons);
		}
		revalidate();
		repaint();
	}

	public void labelize(String str) {
		JLabel label = new JLabel(str.contains("<html>") ? str : "<html><center><u>" + str + "</u></center></html>",
				JLabel.CENTER);
		label.setAlignmentX(0.5f);
		label.setForeground(new Color(0, 0, 50));
		label.setFont(new Font("SERIF", Font.BOLD, 15));
		add(label);
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == start)
			newTask();
		else if (evt.getSource() == submit || evt.getSource() == score) {
			try {
				int grade = Integer.parseInt(score.getText());
				if (grade < 0 || grade > question.worth)
					throw new IllegalArgumentException("Wrong number :P");
				task.applyScore(grade);
				GradeManager.submit(task);
				newTask();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(this, "Please enter an integer score,\n from 0 to " + question.worth);
			}
			score.setText("");
		} else if (evt.getSource() == end) {
			GradeManager.submit(task);
			removeAll();
			add(Box.createGlue());
			add(start);
			add(Box.createGlue());
			score.setText("");
		} else if (evt.getSource() == skip) {
			GradeManager.submit(task);
			newTask();
			score.setText("");
		}

	}

	public class AnswerDisplay extends JPanel {
		private static final long serialVersionUID = 4345924834665088890L;
		JComponent display;

		public AnswerDisplay(Question q, String str, File f) {
			setLayout(new BorderLayout());

			int WIDTH = GradingPanel.this.getWidth();
			int HEIGHT = GradingPanel.this.getHeight() / 4;

			switch (q.type) {
			case 0:
				display = new JPanel(new FlowLayout(FlowLayout.CENTER));
				for (int i = 0; i < q.options.length; i++) {
					JRadioButton b = new JRadioButton(q.options[i], Integer.parseInt(str) == i);
					b.setEnabled(false);
					b.setOpaque(false);
					display.add(b);
				}
				break;
			case 1:
				display = new JPanel(new FlowLayout(FlowLayout.CENTER));
				JRadioButton tr = new JRadioButton("true", str.startsWith("t"));
				JRadioButton fa = new JRadioButton("false", str.startsWith("f"));
				tr.setEnabled(false);
				fa.setEnabled(false);
				tr.setOpaque(false);
				fa.setOpaque(false);
				display.add(tr);
				display.add(fa);
				break;
			case 2:
				display = new JTextArea(str);
				((JTextArea) display).setEditable(false);
				((JTextArea) display).setLineWrap(true);
				((JTextArea) display).setWrapStyleWord(true);
				((JTextArea) display).setOpaque(false);
				((JTextArea) display).setBorder(null);
				break;
			case 3:
				BufferedImage image = null;
				try {
					image = ImageIO.read(f);

				} catch (IOException e) {
					image = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);
				}

				image = new TransperencyFilter().filter(image, new BufferedImage(image.getWidth(), image.getHeight(),
						BufferedImage.TYPE_INT_ARGB));
				display = new JLabel(new ImageIcon(image.getScaledInstance(image.getWidth(null) * HEIGHT
						/ image.getHeight(null), HEIGHT, Image.SCALE_SMOOTH)));
				break;
			case 4:
				display = new MatcherPanel();
				((MatcherPanel) display).SIZE = 20;
				((MatcherPanel) display).fifty = 0;
				((MatcherPanel) display).setMixAndMatchTest(q, false);
				((MatcherPanel) display).text.setText("");

				String[] split = str.split(":");

				for (int i = 0; i < split.length; i += 2) {
					MatcherPanel.Connection conn = ((MatcherPanel) display).new Connection(Integer.parseInt(split[i]),
							Integer.parseInt(split[i + 1]));
					((MatcherPanel) display).connections.add(conn);
				}
				((MatcherPanel) display).show();
				break;
			case 5:
				display = new MazePanel(getFontMetrics(MazePanel.FONT), GradingPanel.this.getWidth());
				((MazePanel) display).setQuestion(q);
				((MazePanel) display).showAnswers();
				display.setOpaque(false);

				break;
			case 6:
				// TODO
				break;
			case 7:
				String[] asdf = str.split(",");
				boolean[] ans = new boolean[q.options.length];

				if (str.equals(""))
					for (int i = 0; i < ans.length; i++)
						ans[i] = false;
				else
					for (int i = 0; i < ans.length; i++)
						for (String s : asdf)
							if (Integer.parseInt(s) == i)
								ans[i] = true;

				display = new JPanel(new FlowLayout(FlowLayout.CENTER));
				for (int i = 0; i < q.options.length; i++) {
					JCheckBox b = new JCheckBox(q.options[i], ans[i]);
					b.setEnabled(false);
					b.setOpaque(false);
					display.add(b);
				}
				break;
			case 8:
				// display = new JLabel("<html><center>" + str.replaceAll("\n",
				// "<br>") + "</html></center>",
				// JLabel.CENTER);
				display = new JTextArea(str);
				((JTextArea) display).setEditable(false);
				((JTextArea) display).setLineWrap(true);
				((JTextArea) display).setWrapStyleWord(true);
				((JTextArea) display).setOpaque(false);
				((JTextArea) display).setBorder(null);

			}
			add(display, BorderLayout.CENTER);
			setOpaque(false);
			display.setOpaque(false);
			display.setPreferredSize(new Dimension(WIDTH, HEIGHT));
			setPreferredSize(new Dimension(WIDTH, HEIGHT));
		}
	}
}
