package admin;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import testing.MazePanel;
import util.Colorful;
import util.JInput;
import util.Methods;
import util.RemovableTextField;
import util.SearchList;
import util.paint.UltraPaint;
import framework.Administrator;
import framework.Course;
import framework.JudgementDriver;
import framework.Question;
import framework.Test;

public class TestCreator extends Colorful implements ActionListener, ItemListener {
	private static final long serialVersionUID = 6495090109041731237L;
	private int defaultNumOptions = 4, defaultWorth = 5;

	ArrayList<CreationUnit> questions = new ArrayList<CreationUnit>();
	ArrayList<JButton> removeButtons = new ArrayList<JButton>();
	ArrayList<JToggleButton> pButtons = new ArrayList<JToggleButton>();

	JButton[] add = new JButton[9];
	String[] names = { "MCQ", "TFQ", "SAQ", "DQ", "MQ", "MAZEQ", "CWQ", "CATAQ", "FRQ" };
	Color[] colors = { Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN, Color.CYAN, Color.BLUE, Color.MAGENTA,
			Color.GRAY, Color.PINK };

	HashMap<JCheckBox, Course> boxToCourse = new HashMap<JCheckBox, Course>();
	private Colorful sidebar = new Colorful(new Color(0, 0, 0, 0), Color.BLACK);

	JButton importQuestions = new JButton("Import");
	JPanel container = new JPanel(null);
	JScrollPane pane = new JScrollPane(container);

	Administrator creator;

	Test test;
	JLabel typeDisplay = new JLabel();

	JLabel passL = new JLabel("Number of intervals (equal): ");
	JLabel avaiL = new JLabel("Test is avaliable: ");
	JLabel retaL = new JLabel("Retake policy: ");
	JLabel dispL = new JLabel("Display questions: ");
	JLabel showL = new JLabel("Feedback policy:");
	JLabel timeL = new JLabel("Time to complete: ");

	JTextField bands = new JTextField("-1");
	JComboBox<String> available = new JComboBox<String>(new String[] { "Always", "Never (dormant)", "When I'm online",
			"When I specify", "By permission", "Part of sequence" });
	JComboBox<String> retakes = new JComboBox<String>(new String[] { "Always", "By permission", "Every so often", "Exactly N", "No" });
	JComboBox<String> displayMode = new JComboBox<String>(new String[] { "Auto", "Single page", "N Questions/page" });
	JCheckBox infiniteTime = new JCheckBox("Timeless");
	JTextField time = new JTextField();

	JTextField n1 = new JTextField();
	JTextField n2 = new JTextField();

	JButton save = new JButton("Save");
	JButton load = new JButton("Load");

	JComboBox<String> showAnswerMode = new JComboBox<String>(new String[] { "Indicate correctness", "Show answers", "Show score",
			"Show NOTHING" });
	JCheckBox[] classes;

	JPanel classPanel = new JPanel(new GridLayout(5, 2));

	public TestCreator(Administrator creator) {
		setLayout(null);

		setOpaque(false);
		test = new Test("UNNAMED");
		sidebar.setOpaque(false);

		classPanel.setOpaque(false);
		infiniteTime.setOpaque(false);

		showL.setBounds(10, 25, 180, 25);
		showAnswerMode.setBounds(10, 50, 180, 25);
		passL.setBounds(10, 80, 180, 25);
		bands.setBounds(20, 105, 100, 25);
		avaiL.setBounds(10, 135, 180, 25);
		available.setBounds(20, 160, 150, 30);
		retaL.setBounds(10, 195, 180, 25);
		retakes.setBounds(20, 220, 100, 25);
		n1.setBounds(125, 220, 50, 25);
		dispL.setBounds(10, 250, 180, 25);
		displayMode.setBounds(20, 275, 120, 25);
		n2.setBounds(145, 275, 50, 25);
		timeL.setBounds(10, 320, 180, 25);
		infiniteTime.setBounds(30, 350, 160, 25);
		time.setBounds(40, 375, 110, 25);

		showAnswerMode.setSelectedIndex(1);

		passL.setForeground(Color.white);
		avaiL.setForeground(Color.WHITE);
		showL.setForeground(Color.WHITE);

		sidebar.setLayout(null);
		add(sidebar);

		n1.setVisible(false);
		n2.setVisible(false);

		sidebar.add(passL);
		sidebar.add(bands);
		sidebar.add(avaiL);
		sidebar.add(available);
		sidebar.add(retaL);
		sidebar.add(retakes);
		sidebar.add(showL);
		sidebar.add(dispL);
		sidebar.add(displayMode);
		sidebar.add(timeL);
		sidebar.add(n1);
		sidebar.add(showAnswerMode);
		sidebar.add(n2);
		sidebar.add(load);
		sidebar.add(save);
		sidebar.add(infiniteTime);
		sidebar.add(time);

		load.addActionListener(this);
		save.addActionListener(this);

		load.setContentAreaFilled(false);
		save.setContentAreaFilled(false);

		load.setForeground(Color.GREEN);
		save.setForeground(Color.CYAN);

		pane.getVerticalScrollBar().setUnitIncrement(16);
		container.add(typeDisplay);
		typeDisplay.setHorizontalAlignment(JLabel.CENTER);

		for (int i = 0; i < 9; i++) {
			add[i] = new JButton("+");
			add(add[i]);
			add[i].setBackground(Methods.colorMeld(new Color(0, 150, 0), new Color(224, 204, 152), (double) i / 8D));
			add[i].setToolTipText(names[i]);
			add[i].addActionListener(this);
			container.add(add[i]);
			add[i].addMouseListener(new TypeHandler());
		}
		displayMode.addItemListener(this);
		retakes.addItemListener(this);

		add(pane);
		sidebar.add(classPanel);

		// container.setBackground(Color.GRAY);

		this.creator = creator;
		setAdmin(creator);
		updateLayout();
	}

	public void itemStateChanged(ItemEvent evt) {
		if (evt.getSource() == retakes)
			n1.setVisible(retakes.getSelectedIndex() == 3);
		else if (evt.getSource() == displayMode)
			n2.setVisible(displayMode.getSelectedIndex() == 2);
	}

	public void setBounds(int a, int b, int c, int d) {
		super.setBounds(a, b, c, d);
		updateLayout();
	}

	public void clear() {
		for (int i = 0; i < questions.size(); i++) {
			container.remove(questions.get(i));
			container.remove(removeButtons.get(i));
			container.remove(pButtons.get(i));
		}

		questions.clear();
		removeButtons.clear();
		pButtons.clear();
		updateLayout();
	}

	public void setAdmin(Administrator a) {
		clear();
		creator = a;

		if (a != null) {
			if (a.classes != null) {
				classPanel.removeAll();
				classPanel.setLayout(new BoxLayout(classPanel, BoxLayout.Y_AXIS));

				classes = new JCheckBox[a.classes.length];
				for (int i = 0; i < classes.length; i++) {
					classes[i] = new JCheckBox(Course.getByNumber(creator.classes[i]).name);
					classes[i].addActionListener(this);
					classes[i].setOpaque(false);
					classes[i].setForeground(Color.CYAN);
					classPanel.add(classes[i]);
					boxToCourse.put(classes[i], Course.getByNumber(creator.classes[i]));
				}
			}
		}
	}

	public void setTest(Test t) {
		if (t == null)
			return;
		clear();

		bands.setText(t.bands + "");
		available.setSelectedIndex(t.available);
		retakes.setSelectedIndex(t.retakes >= 0 ? 3 : -t.retakes - 1);
		n1.setText(t.retakes > 0 ? t.retakes + "" : "");
		displayMode.setSelectedIndex(t.displayMode >= 0 ? 2 : -t.displayMode - 1);
		n2.setText(t.displayMode > 0 ? t.displayMode + "" : "");
		infiniteTime.setSelected(t.time == -999);
		time.setText(t.time == -999 ? "" : t.time + "");
		showAnswerMode.setSelectedIndex(t.showAnswerMode);

		for (int i = 0; i < classes.length; i++) {
			Course c = boxToCourse.get(classes[i]);
			if (c != null)
				if (Methods.contains(t.classes, c.getNumber()))
					classes[i].setSelected(true);
				else
					classes[i].setSelected(false);
		}

		for (Question q : t.questions) {
			CreationUnit comp = new CreationUnit(getQuestionComponent(q), q);

			questions.add(comp);
			container.add(comp);

			JButton button = new JButton(new ImageIcon("Resources/wrong.png"));
			JToggleButton prop = new JToggleButton(new ImageIcon("Resources/wrench.png"));
			prop.setSelectedIcon(new ImageIcon("Resources/wrench2.png"));
			button.addActionListener(this);
			prop.addActionListener(this);
			removeButtons.add(button);
			pButtons.add(prop);

			container.add(button);
			container.add(prop);

			if (q.type == 3)
				try {
					((CDrawQC) comp.comp).paint.load(new File("Links/Keys/"
							+ t.getName().substring(0, t.getName().indexOf(".tst")) + "-"
							+ q.text.replaceAll("\n", "").replaceAll("\t", "") + ".jpg"));
				} catch (Exception e) {
					e.printStackTrace();
				}
		}

		updateLayout();
	}

	public CreateQuestionComponent getQuestionComponent(int type) {
		switch (type) {
		case 0:
			return new CMultChoiceQC();
		case 1:
			return new CTrueFalseQC();
		case 2:
			return new CShortAnsQC();
		case 3:
			return new CDrawQC();
		case 4:
			return new CMatchQC();
		case 5:
			return new CMazeQC();
		case 6:
			return new CChooseWordsQC();
		case 7:
			return new CCheckAllQC();
		case 8:
			return new CFreeResponseQC();
		}
		return null;
	}

	public CreateQuestionComponent getQuestionComponent(Question q) {
		switch (q.type) {
		case 0:
			return new CMultChoiceQC(q);
		case 1:
			return new CTrueFalseQC(q);
		case 2:
			return new CShortAnsQC(q);
		case 3:
			return new CDrawQC(q);
		case 4:
			return new CMatchQC(q);
		case 5:
			return new CMazeQC(q);
		case 6:
			return new CChooseWordsQC(q);
		case 7:
			return new CCheckAllQC(q);
		case 8:
			return new CFreeResponseQC(q);
		}
		return null;
	}

	public void updateLayout() {
		pane.setBounds(200, 0, getWidth() - 200, getHeight());
		sidebar.setBounds(0, 0, 200, getHeight());

		load.setBounds(2, getHeight() - 32, 97, 30);
		save.setBounds(102, getHeight() - 32, 97, 30);

		classPanel.setBounds(10, 410, 180, getHeight() - 445);

		int height = 100;

		for (int v = 0; v < questions.size(); v++) {
			CreationUnit q = questions.get(v);

			q.setBorder(new javax.swing.border.TitledBorder(new javax.swing.border.LineBorder(Color.BLACK, 5),
					"Question " + (v + 1)));

			int dh = q.getPreferredSize().height;
			q.setBounds(50, height, container.getWidth() - 100, dh);
			removeButtons.get(v).setBounds(container.getWidth() - 45, height + dh / 2 - 17, 40, 35);
			pButtons.get(v).setBounds(5, height + dh / 2 - 17, 40, 35);

			height += dh;
		}

		for (double i = 0; i < 9; i++)
			add[(int) i].setBounds((int) (container.getWidth() / 2 - (4.5 - i) * 50), height + 10, 50, 25);

		typeDisplay.setBounds(10, height + 40, container.getWidth() - 20, 200);

		super.revalidate();
		repaint();
		container.setPreferredSize(new Dimension(container.getWidth() - 20, height + 245));
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == save) {
			try {
				test.bands = Integer.parseInt(bands.getText());
				test.available = available.getSelectedIndex();
				test.retakes = retakes.getSelectedIndex() == 3 ? Integer.parseInt(n1.getText()) : -retakes
						.getSelectedIndex() - 1;
				test.displayMode = displayMode.getSelectedIndex() == 2 ? Integer.parseInt(n2.getText()) : -displayMode
						.getSelectedIndex() - 1;
				test.time = infiniteTime.isSelected() ? -999 : Integer.parseInt(time.getText());
				test.showAnswerMode = showAnswerMode.getSelectedIndex();

				ArrayList<Integer> temp = new ArrayList<Integer>();
				for (int i = 0; i < classes.length; i++)
					if (classes[i].isSelected())
						temp.add(boxToCourse.get(classes[i]).getNumber());

				if (temp.size() == 0) {
					JOptionPane.showMessageDialog(null, "Please make the test available for at least one class"
							+ "\n You can do this by checking the boxes on the left (create a class if you have none)");
					return;
				}

				test.classes = Methods.unbox(Methods.createArray(temp, new Integer[temp.size()]));
				test.setName(JOptionPane.showInputDialog("Test Name:"));

				for (CreationUnit q : questions) {
					Question question = q.getQuestion();

					if (question.teacherMustGrade()) {
						File file = new File("Links/Keys/" + test.getName() + "-"
								+ question.text.replaceAll("\n", "").replaceAll("\t", "") + ".jpg");

						if (q.comp.saveGivenAnswerTo(file))
							question.correct = file.getName();
					}

					test.questions.add(question);
				}

			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(null,
						"Invalid field entries\n Make sure that the fields on the left are complete");
				return;
			}

			try {
				Test.saveTest(test);
				Test.refreshTests();
				JOptionPane.showMessageDialog(null, "Test sucessfully saved");
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Test not saved.");
				e.printStackTrace();
			}
		} else if (evt.getSource() == load) {
			JTextField search = new JTextField();
			SearchList<Test> listOTests = new SearchList<Test>(Test.allTests, search, null);
			JScrollPane pane = new JScrollPane(listOTests);

			search.setPreferredSize(new Dimension(200, 30));
			pane.setPreferredSize(new Dimension(200, 200));

			Box b = new Box(BoxLayout.Y_AXIS);
			b.add(search);
			b.add(pane);

			JOptionPane.showMessageDialog(null, b);

			if (listOTests.getSelectedIndex() >= 0) {
				setTest(listOTests.poss.get(listOTests.getSelectedIndex()));
			}
		} else {

			for (int i = 0; i < 9; i++)
				if (evt.getSource() == add[i]) {
					CreationUnit comp = new CreationUnit(getQuestionComponent(i));

					questions.add(comp);
					container.add(comp);

					JButton button = new JButton(new ImageIcon("Resources/wrong.png"));
					JToggleButton prop = new JToggleButton(new ImageIcon("Resources/wrench.png"));
					prop.setSelectedIcon(new ImageIcon("Resources/wrench2.png"));
					button.addActionListener(this);
					prop.addActionListener(this);
					removeButtons.add(button);
					pButtons.add(prop);

					container.add(button);
					container.add(prop);

					updateLayout();
				}

			for (int i = 0; i < questions.size(); i++)
				if (evt.getSource() == removeButtons.get(i)) {
					container.remove(questions.get(i));
					container.remove(removeButtons.get(i));
					container.remove(pButtons.get(i));

					questions.get(i).comp.kill();

					questions.remove(i);
					removeButtons.remove(i);
					pButtons.remove(i);

					updateLayout();
				}
			for (int i = 0; i < questions.size(); i++)
				if (evt.getSource() == pButtons.get(i)) {
					questions.get(i).setEditProperties(pButtons.get(i).isSelected());

				}
		}
	}

	private class TypeHandler extends MouseAdapter implements MouseListener {
		public void mouseEntered(MouseEvent evt) {
			for (int i = 0; i < 9; i++)
				if (evt.getSource() == add[i]) {
					// typeDisplay.setOpaque(true);
					// typeDisplay.setBackground(Methods.colorMeld(Methods.colorMeld(new
					// Color(0, 150, 0), new Color(224,
					// 204, 152), (double) i / 8D), Color.WHITE, .4));
					typeDisplay.setIcon(new ImageIcon("Resources/qimage/" + i + ".png"));
				}

		}

		public void mouseExited(MouseEvent evt) {
			typeDisplay.setOpaque(false);
			typeDisplay.setIcon(null);
		}
	}

	public class CreationUnit extends JPanel {
		private static final long serialVersionUID = -510197191095401433L;

		CreateQuestionComponent comp;
		QuestionPropertiesPanel prop;

		public CreationUnit(CreateQuestionComponent c, Question q) {
			comp = c;
			prop = new QuestionPropertiesPanel(c, q);

			setLayout(new BorderLayout());
			add(comp, BorderLayout.CENTER);
			add(prop, BorderLayout.NORTH);

			setOpaque(false);

			prop.setVisible(false);
		}

		public CreationUnit(CreateQuestionComponent c) {
			this(c, null);
		}

		public Question getQuestion() {
			Question quest = comp.getQuestion();

			quest.blankPolicy = Integer.parseInt(prop.blank.getText());
			quest.worth = Integer.parseInt(prop.worth.getText());
			quest.spec = prop.grading.getSelectedIndex();
			quest.image = prop.image.getText().substring(6);

			return quest;
		}

		public void setEditProperties(boolean b) {
			prop.setVisible(b);
			comp.setVisible(!b);

			setOpaque(b);
			setBackground(b ? new Color(150, 150, 250) : new Color(238, 238, 238));
		}
	}

	public class QuestionPropertiesPanel extends JPanel implements ActionListener {
		private static final long serialVersionUID = -1942690709894239860L;

		JInput blank = new JInput("Points for blank: ", false);
		JInput worth = new JInput("Points for correct: ", false);
		JLabel type;
		JComboBox<String> grading = new JComboBox<String>(new String[] { "Auto-grade", "Case sensitive", "Manual grade",
				"All or nothing" });
		JButton image = new JButton("Image:NONE");

		JList<String> list; // for dialog only

		public QuestionPropertiesPanel(CreateQuestionComponent c) {
			this(c, null);
		}

		public QuestionPropertiesPanel(CreateQuestionComponent c, Question q) {
			// BoxLayout l = new BoxLayout(this, BoxLayout.Y_AXIS);
			setLayout(new GridLayout(5, 1));
			setOpaque(false);

			type = new JLabel(Question.TYPES[c.questionType]);
			type.setHorizontalAlignment(JLabel.CENTER);

			blank.f.setText(q == null ? "0" : "" + q.blankPolicy);
			worth.f.setText(q == null ? "" + defaultWorth : "" + q.worth);

			if ((q != null && q.spec == 2) || (c.questionType == 3) || (c.questionType == 8))
				grading.setSelectedIndex(2);

			if (q != null && !q.image.equals("Image:NONE"))
				image.setText("Image:" + q.image);

			image.addActionListener(this);
			image.setMinimumSize(new Dimension(TestCreator.this.container.getWidth() - 100, 25));

			add(type);
			add(blank);
			add(worth);
			add(grading);
			add(image);

			grading.setOpaque(false);
		}

		public void actionPerformed(ActionEvent evt) {
			list = new JList<String>(new File("Links").list(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					return name.endsWith(".jpg") || name.endsWith(".png") || name.endsWith(".bmp")
							|| name.endsWith(".gif");
				}
			}));

			JScrollPane pane = new JScrollPane(list);
			JButton add = new JButton("ADD");

			pane.setPreferredSize(new Dimension(150, 200));

			JPanel b = new JPanel(new BorderLayout());
			b.add(pane, BorderLayout.CENTER);
			b.add(add, BorderLayout.SOUTH);
			add.setAlignmentX(0);

			add.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					JFileChooser chooser = new JFileChooser();
					if (chooser.showOpenDialog(QuestionPropertiesPanel.this) == JFileChooser.APPROVE_OPTION) {
						try {
							Methods.copy(chooser.getSelectedFile(), new File("Links", chooser.getSelectedFile()
									.getName()));
						} catch (IOException e) {
							e.printStackTrace();
						}

						list.setListData(new File("Links").list());
					}
				}
			});

			JOptionPane.showMessageDialog(null, b);

			image.setText("Image:" + (list.getSelectedIndex() >= 0 ? (String) list.getSelectedValue() : "NONE"));
		}
	}

	abstract class CreateQuestionComponent extends JPanel {
		private static final long serialVersionUID = -7247521044661759026L;
		int questionType;
		protected boolean done = false;

		CreateQuestionComponent(int type) {
			CreateQuestionComponent.this.questionType = type;
			// setOpaque(false);
		}

		public boolean saveGivenAnswerTo(File file) {
			System.out.println("Saving in line");
			return false;
		}

		public int getQuestionType() {
			return questionType;
		}

		protected abstract Question getQuestion();

		public void kill() {
		}
	}

	public class CMultChoiceQC extends CreateQuestionComponent implements ActionListener {
		private static final long serialVersionUID = 5017508469743502737L;
		JTextArea question = new JTextArea(2, 10);
		JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		ArrayList<RemovableTextField> options = new ArrayList<RemovableTextField>();
		ButtonGroup group = new ButtonGroup();

		JButton add = new JButton("+");

		public CMultChoiceQC() {
			super(0);

			setLayout(new BorderLayout());

			for (int i = 0; i < defaultNumOptions; i++) {
				RemovableTextField temp = new RemovableTextField();
				temp.setCheckBox(new JRadioButton("Correct"));
				group.add(temp.getCheckBox());
				options.add(temp);
				temp.getButton().addActionListener(this);
				temp.getCheckBox().addActionListener(this);
				panel.add(temp);
			}

			panel.add(add);
			options.get(0).getCheckBox().setSelected(true);
			options.get(0).getField().setBackground(new Color(190, 190, 255));
			options.get(0).getButton().setEnabled(false);

			add.addActionListener(this);

			add(panel, BorderLayout.SOUTH);
			add(new JScrollPane(question), BorderLayout.CENTER);
		}

		public CMultChoiceQC(Question q) {
			this();

			question.setText(q.text);
			panel.removeAll();
			options.clear();

			int count = 0;

			for (String s : q.options) {
				RemovableTextField temp = new RemovableTextField();
				temp.getField().setText(s);
				temp.setCheckBox(new JRadioButton("Correct"));
				group.add(temp.getCheckBox());
				options.add(temp);
				temp.getButton().addActionListener(this);
				temp.getCheckBox().addActionListener(this);
				panel.add(temp);

				if (q.correct.equals("" + (count++))) {
					temp.getCheckBox().setSelected(true);
					temp.getField().setBackground(new Color(190, 190, 255));
					temp.getButton().setEnabled(false);
				} else
					temp.getCheckBox().setSelected(false);
			}
		}

		public void actionPerformed(ActionEvent evt) {
			if (evt.getSource() == add) {
				RemovableTextField temp = new RemovableTextField();
				temp.setCheckBox(new JRadioButton("Correct"));
				group.add(temp.getCheckBox());
				options.add(temp);
				temp.getButton().addActionListener(this);
				temp.getCheckBox().addActionListener(this);
				panel.add(temp);
				panel.remove(add);
				panel.add(add);
			}
			for (int i = 0; i < options.size(); i++) {
				if (evt.getSource() == options.get(i).getButton()) {
					panel.remove(options.get(i));
					options.remove(i);
				} else if (evt.getSource() == options.get(i).getCheckBox()) {
					options.get(i).getField().setBackground(new Color(190, 190, 255));
					options.get(i).getButton().setEnabled(false);
					for (RemovableTextField f : options) {
						if (f != options.get(i)) {
							f.getField().setBackground(Color.WHITE);
							f.getButton().setEnabled(true);
						}
					}
				}
			}
			panel.revalidate();
			panel.repaint();
			revalidate();
			// panel.setPreferredSize(new Dimension(panel.getWidth(),40));
		}

		public Question getQuestion() {
			String[] opt = new String[options.size()];
			String selected = null;
			for (int i = 0; i < opt.length; i++) {
				opt[i] = options.get(i).getField().getText();
				if (options.get(i).getCheckBox().isSelected())
					selected = i + "";
			}
			return new Question(test.getName(), 0, defaultWorth, question.getText(), selected, opt);
		}
	}

	public class CTrueFalseQC extends CreateQuestionComponent {
		private static final long serialVersionUID = 7384440609504821107L;
		JTextArea question = new JTextArea(2, 10);
		JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		ButtonGroup group = new ButtonGroup();

		JRadioButton t = new JRadioButton("True");
		JRadioButton f = new JRadioButton("False");

		CTrueFalseQC() {
			super(1);

			setLayout(new BorderLayout());

			group.add(t);
			group.add(f);
			panel.add(t);
			panel.add(f);

			t.setSelected(true);

			add(panel, BorderLayout.SOUTH);
			add(new JScrollPane(question), BorderLayout.CENTER);
		}

		CTrueFalseQC(Question q) {
			this();
			question.setText(q.text);
			t.setSelected(q.correct.charAt(0) == 't');
			f.setSelected(q.correct.charAt(0) != 't');
		}

		public Question getQuestion() {
			return new Question(test.getName(), 1, defaultWorth, question.getText(), (t.isSelected() ? "t" : "f"),
					"true", "false");
		}
	}

	public class CShortAnsQC extends CreateQuestionComponent {
		private static final long serialVersionUID = 6188311736543047668L;
		JTextArea question = new JTextArea(2, 10);
		JTextField answer = new JTextField(10);
		JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));

		CShortAnsQC() {
			super(2);
			setLayout(new BorderLayout());

			panel.add(answer);

			add(panel, BorderLayout.SOUTH);
			add(new JScrollPane(question), BorderLayout.CENTER);
		}

		CShortAnsQC(Question q) {
			this();

			question.setText(q.text);
			answer.setText(q.correct);
		}

		public Question getQuestion() {
			return new Question(test.getName(), 2, defaultWorth, question.getText(), answer.getText());
		}
	}

	public class CDrawQC extends CreateQuestionComponent {
		private static final long serialVersionUID = 2071659181958411893L;
		JTextArea question = new JTextArea(3, 10);
		UltraPaint paint = new UltraPaint();

		CDrawQC() {
			super(3);
			setLayout(new BorderLayout());

			paint.setPreferredSize(new Dimension(600, 400));
			paint.setSize(600, 400);

			add(paint, BorderLayout.CENTER);
			add(new JScrollPane(question), BorderLayout.NORTH);
		}

		CDrawQC(Question q) {
			this();

			question.setText(q.text);
			try {
				paint.load(new File("Links/Keys/" + q.correct));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public boolean saveGivenAnswerTo(File f) {
			paint.save(f);
			return true;
		}

		public void kill() {
			paint.kill();
			super.kill();
		}

		public Question getQuestion() {

			return new Question(test.getName(), 3, defaultWorth, question.getText(), "NONE");
		}
	}

	public class CMatchQC extends CreateQuestionComponent implements KeyListener, ActionListener {
		private static final long serialVersionUID = 1709190859667052665L;
		JTextArea question = new JTextArea(2, 10);
		JTextField field = new JTextField(20);
		JButton leftB = new JButton(new ImageIcon("Resources/leftS.png"));
		JButton rightB = new JButton(new ImageIcon("Resources/rightS.png"));
		JButton clear = new JButton("Clear");
		JButton load = new JButton("Load");

		Set<Integer> down = new HashSet<Integer>();

		Display display = new Display();

		CMatchQC() {
			super(4);

			setLayout(new BorderLayout());

			JPanel top = new JPanel(new FlowLayout(FlowLayout.CENTER));
			field.setHorizontalAlignment(JTextField.CENTER);

			top.add(leftB);
			top.add(field);
			top.add(rightB);

			JPanel fix = new JPanel(new BorderLayout());

			fix.add(top, BorderLayout.CENTER);
			fix.add(clear, BorderLayout.EAST);
			fix.add(load, BorderLayout.WEST);
			fix.add(new JScrollPane(question), BorderLayout.NORTH);

			add(display, BorderLayout.CENTER);
			add(fix, BorderLayout.NORTH);

			field.addKeyListener(this);
			leftB.addActionListener(this);
			rightB.addActionListener(this);
			clear.addActionListener(this);
			load.addActionListener(this);

			/*
			 * for(int i = 0; i < 20; i++) { if(Math.random()>.3)
			 * display.left.add((char)(98+i)+"SDF"); if(Math.random()>.3)
			 * display.right.add((char)(64+i)+"WERT"); }
			 * 
			 * display.repaint();
			 */
			field.requestFocus();
		}

		CMatchQC(Question q) {
			this();

			question.setText(q.text);

			for (String s : q.options) {
				String[] split = s.split(":");
				boolean left = !split[0].equals(""), right = !split[0].equals("");

				if (left && ((s.charAt(0) != ':' && split.length == 1) || split.length > 1))
					display.left.add(split[0]);
				if (right)
					if (s.charAt(0) == ':' && split.length == 1)
						display.right.add(split[0]);
					else if (split.length > 1)
						display.right.add(split[1]);
			}

			String[] split = q.correct.split(":");

			for (int i = 0; i < split.length; i += 2) {
				display.connections.add(new Connection(Integer.parseInt(split[i]), Integer.parseInt(split[i + 1])));
			}
		}

		public void actionPerformed(ActionEvent evt) {
			if (evt.getSource() == leftB && !field.getText().equals(""))
				display.left.add(field.getText());
			else if (evt.getSource() == rightB && !field.getText().equals(""))
				display.right.add(field.getText());
			else if (evt.getSource() == clear) {
				display.left.clear();
				display.right.clear();
				display.connections.clear();
				return;
			} else if (evt.getSource() == load) {
				JFileChooser chooser = new JFileChooser();
				int r = chooser.showOpenDialog(this);

				if (r == JFileChooser.APPROVE_OPTION) {
					display.left.clear();
					display.right.clear();
					display.connections.clear();

					ArrayList<String> lines = Methods.getList(Methods.getFileContents(chooser.getSelectedFile()).split(
							"\n"));

					int n = Integer.parseInt(JOptionPane.showInputDialog(this, "Number of lines to grab: (out of "
							+ lines.size() + ")"));

					while (n <= 0 || n > lines.size()) {
						try {
							n = Integer.parseInt(JOptionPane.showInputDialog(this,
									"Please enter an integer between 1 and " + lines.size()
											+ "\n\nNumber of lines to grab: (out of " + lines.size() + ")"));
						} catch (Exception e) {
						}
					}

					for (int i = 0; i < n; i++) {
						int index = (int) (Math.random() * lines.size());

						String line = lines.get(index);
						lines.remove(index);

						display.left.add(line.substring(0, line.indexOf('\t')));
						display.right.add(line.substring(line.indexOf('\t') + 1));
						display.connections.add(new Connection(i, i));
					}
					display.scramble();
				}
			}

			field.requestFocus();

			field.setText("");
			display.repaint();

		}

		public void keyTyped(KeyEvent evt) {
		}

		public void keyPressed(KeyEvent evt) {
			if (evt.getSource() == field)
				down.add(evt.getKeyCode());
		}

		public void keyReleased(KeyEvent evt) {
			if (evt.getSource() == field)
				down.remove(evt.getKeyCode());

			if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
				if (!field.getText().equals("")) {
					if (down.contains(KeyEvent.VK_CONTROL))
						display.right.add(field.getText());
					else
						display.left.add(field.getText());

					field.setText("");
					display.repaint();
				}
			}
		}

		public Question getQuestion() {
			String[] newOpt = new String[Math.max(display.left.size(), display.right.size())];
			StringBuilder build = new StringBuilder();

			for (int i = 0; i < newOpt.length; i++)
				newOpt[i] = (display.left.size() <= i ? "" : display.left.get(i)) + ":"
						+ (display.right.size() <= i ? "" : display.right.get(i));

			for (int i = 0; i < display.connections.size(); i++)
				build.append(":" + display.connections.get(i).a + ":" + display.connections.get(i).b);

			if (build.length() > 0)
				build.deleteCharAt(0);

			return new Question(test.getName(), 4, defaultWorth, question.getText(), build.toString(), newOpt);
		}

		private class Display extends JPanel implements MouseListener, MouseMotionListener {
			private static final long serialVersionUID = -4297595237027222646L;
			private Selection selected, mouseOver;
			private Point to;

			ArrayList<String> left = new ArrayList<String>(), right = new ArrayList<String>();

			ArrayList<Connection> connections = new ArrayList<Connection>();

			Display() {
				super(true);
				addMouseMotionListener(this);
				addMouseListener(this);
				setPreferredSize(new Dimension(600, 300));
			}

			public void scramble() {
				for (int i = 0; i < left.size(); i++) {
					int newIndex = (int) (Math.random() * left.size());
					for (Connection c : connections) {
						if (c.a == i)
							c.a = newIndex;
						else if (c.a == newIndex)
							c.a = i;
					}

					String temp = left.get(i);
					left.set(i, left.get(newIndex));
					left.set(newIndex, temp);
				}

				for (int i = 0; i < right.size(); i++) {
					int newIndex = (int) (Math.random() * right.size());
					for (Connection c : connections) {
						if (c.b == i)
							c.b = newIndex;
						else if (c.b == newIndex)
							c.b = i;
					}
					String temp = right.get(i);
					right.set(i, right.get(newIndex));
					right.set(newIndex, temp);
				}
			}

			public void mousePressed(MouseEvent evt) {
				if (evt.getX() < 120 && evt.getX() > 20) {
					for (int i = 0; i < left.size(); i++)
						if (evt.getY() < i * 20 + 25 && evt.getY() > i * 20 + 5)
							selected = new Selection(i, true);
				} else if (evt.getX() > 200) {
					for (int i = 0; i < right.size(); i++)
						if (evt.getY() < i * 20 + 25 && evt.getY() > i * 20 + 5)
							selected = new Selection(i, false);
				} else
					selected = null;
			}

			public void mouseReleased(MouseEvent evt) {
				to = evt.getPoint();
				if (selected != null) {
					boolean good = true;

					if (getNumberForPos(to) == null)
						good = false;
					else
						for (Connection c : connections)
							if (c.is(selected, getNumberForPos(to)))
								good = false;

					if (good && selected.side ^ getNumberForPos(to).side)
						connections.add(new Connection(selected, getNumberForPos(to)));

					to = null;
					selected = null;
					mouseOver = null;
					repaint();
				}
			}

			private Selection getNumberForPos(Point p) {
				if (p.x < 120)
					for (int i = 0; i < left.size(); i++)
						if (p.y < i * 20 + 25 && p.y > i * 20 + 5)
							return new Selection(i, true);
				if (p.x > 200)
					for (int i = 0; i < right.size(); i++)
						if (p.y < i * 20 + 25 && p.y > i * 20 + 5)
							return new Selection(i, false);

				return null;
			}

			public void mouseEntered(MouseEvent evt) {
			}

			public void mouseMoved(MouseEvent evt) {
			}

			public void mouseDragged(MouseEvent evt) {
				to = evt.getPoint();
				mouseOver = getNumberForPos(evt.getPoint());
				repaint();
			}

			public void mouseExited(MouseEvent evt) {
			}

			public void mouseClicked(MouseEvent evt) {
				if (evt.getClickCount() == 2 || evt.isMetaDown()) {
					Selection s = getNumberForPos(evt.getPoint());

					if (s != null) {
						for (int i = 0; i < connections.size(); i++) {
							if (connections.get(i).has(s)) {
								connections.remove(i);
								i--;
							}
						}

						if (evt.isMetaDown()) {
							if (s.side)
								left.remove(s.val);
							else
								right.remove(s.val);

							for (int i = 0; i < connections.size(); i++) {
								if (s.side && connections.get(i).a > s.val) {
									connections.get(i).a--;
									i = 0;
								}

								if (!s.side && connections.get(i).b > s.val) {
									connections.get(i).b--;
									i = 0;
								}

							}
						}

						repaint();
					}
				}
			}

			public void paintComponent(Graphics asdf) {
				super.paintComponent(asdf);

				if (getPreferredSize().height != 20 * Math.max(left.size(), right.size()) + 40) {

					setPreferredSize(new Dimension(600, 20 * Math.max(left.size(), right.size()) + 40));
					CMatchQC.this.revalidate();
					updateLayout();
				}

				Graphics2D g = (Graphics2D) asdf;
				g.setColor(Color.BLACK);

				for (int i = 0; i < left.size(); i++) {
					g.setColor(Methods.randomColor(connections.size(), 150));
					if (mouseOver != null && mouseOver.val == i && mouseOver.side && selected != null)
						g.fillRect(20, i * 20 + 10, 100, 20);

					g.setColor(Methods.randomColor(connections.size(), 150));
					if (selected != null && selected.val == i && selected.side)
						g.fillRect(20, i * 20 + 10, 100, 20);

					g.setColor(Color.BLACK);
					g.drawString(left.get(i), 20, i * 20 + 25);
				}

				for (int i = 0; i < right.size(); i++) {
					g.setColor(Color.BLACK);
					g.drawString(right.get(i), 200, i * 20 + 25);

					g.setColor(Methods.randomColor(connections.size(), 150));
					if (mouseOver != null && mouseOver.val == i && !mouseOver.side && selected != null)
						g.fillRect(200, i * 20 + 10, 800, 20);

					g.setColor(Methods.randomColor(connections.size(), 150));
					if (selected != null && selected.val == i && !selected.side)
						g.fillRect(200, i * 20 + 10, 800, 20);
				}

				for (int i = 0; i < connections.size(); i++) {
					g.setColor(Methods.randomColor(i, 150));
					g.fillRect(200, connections.get(i).b * 20 + 10, 800, 20);
					g.fillRect(20, connections.get(i).a * 20 + 10, 100, 20);
					g.setStroke(new BasicStroke(5));
					g.drawLine(120, connections.get(i).a * 20 + 20, 200, connections.get(i).b * 20 + 20);
					g.setStroke(new BasicStroke(1));
				}

				g.setStroke(new BasicStroke(5));

				g.setColor(Methods.randomColor(connections.size(), 150));
				if (selected != null && to != null)
					g.drawLine(selected.side ? 120 : 200, selected.val * 20 + 20, to.x, to.y);

				g.setStroke(new BasicStroke(1));
			}
		}

		public class Connection {
			int a, b;

			Connection(int a, int b) {
				this.a = a;
				this.b = b;
			}

			Connection(Selection a, Selection b) {
				if (a.side) {
					this.a = a.val;
					this.b = b.val;
				} else {
					this.a = b.val;
					this.b = a.val;
				}
			}

			public boolean is(int a, int b) {
				return a == this.a && b == this.b;
			}

			public boolean has(Selection a) {
				return (a.side && (a.val == this.a)) || (!a.side && (a.val == this.b));
			}

			public boolean is(Selection a, Selection b) {
				return (a.side ? a.val == this.a : a.val == this.b) && (b.side ? b.val == this.a : b.val == this.b);
			}
		}

		public class Selection {
			boolean side;
			int val;

			Selection(int a, boolean b) {
				this.val = a;
				this.side = b;
			}

			public boolean is(int a, boolean b) {
				return a == this.val && b == this.side;
			}
		}

	}

	public class CMazeQC extends CreateQuestionComponent implements ActionListener, DocumentListener {
		private static final long serialVersionUID = 3114131218167838962L;
		JTextArea question = new JTextArea(10, 10);
		MazePanel mp = new MazePanel(JudgementDriver.program.getFontMetrics(MazePanel.FONT), container.getWidth() - 100);
		JButton next = new JButton("Specify Answers");
		JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		boolean ans = false;
		JScrollPane scroll = new JScrollPane(question);
		JTextField time = new JTextField("180");

		CMazeQC() {
			super(5);
			setLayout(new BorderLayout(5, 5));

			setBackground(new Color(200, 255, 200));

			add(new JLabel(
					"<html><center> Enter the question text; use square brackets and commas to indicate chosable words.<br>"
							+ "Example: The teacher [writes, write, writing] tests.</center></html>", JLabel.CENTER),
					BorderLayout.NORTH);

			add(scroll, BorderLayout.CENTER);
			add(panel, BorderLayout.SOUTH);

			panel.add(time);
			panel.add(next);

			panel.setOpaque(false);

			question.getDocument().addDocumentListener(this);
			mp.addMouseListener(new MouseHandler());
			next.addActionListener(this);
		}

		CMazeQC(Question q) {
			this();
			mp.setQuestion(q);
			System.out.println("FM: " + JudgementDriver.program.getFontMetrics(mp.font));
			mp.loadAnswers(q.correct);
			question.setText(q.text);
			time.setText(q.options[0]);
		}

		public Question getQuestion() {
			return new Question(test.getName(), 5, defaultWorth, question.getText(), mp.getAnswer(), time.getText());
		}

		@Override
		public void actionPerformed(ActionEvent evt) {
			if (evt.getSource() == next) {
				ans = !ans;

				if (ans && getBackground().getRed() == 255)
					try {
						mp.setText(question.getText());
					} catch (Exception e) {
						JOptionPane.showMessageDialog(this, "Brace misalignment");
						mp.setText("");
					}

				panel.setLayout(new FlowLayout(ans ? FlowLayout.RIGHT : FlowLayout.LEFT));
				next.setText(ans ? "Change text" : "Specify Answers");
				remove(ans ? scroll : mp);
				add(ans ? mp : scroll, BorderLayout.CENTER);
				revalidate();
			}
		}

		public void changed() {
			if (Methods.count(question.getText(), '[') != Methods.count(question.getText(), ']')
					|| Methods.count(question.getText(), '[') != mp.options.size()) {
				setBackground(new Color(255, 200, 200));
			}
		}

		public void changedUpdate(DocumentEvent evt) {
			changed();
		}

		public void insertUpdate(DocumentEvent arg0) {
			changed();
		}

		public void removeUpdate(DocumentEvent arg0) {
			changed();
		}

		private class MouseHandler extends MouseAdapter implements MouseListener {
			public void mouseReleased(MouseEvent evt) {
				if (mp.options.size() > 0 || !question.getText().contains("["))
					setBackground(new Color(200, 255, 200));
			}
		}
	}

	public class CChooseWordsQC extends CreateQuestionComponent {
		private static final long serialVersionUID = 6707144497325388321L;
		JTextArea question = new JTextArea();

		CChooseWordsQC() {
			super(6);
		}

		CChooseWordsQC(Question q) {
			this();// TODO
		}

		public Question getQuestion() {
			return null;
		}
	}

	public class CCheckAllQC extends CreateQuestionComponent implements ActionListener {
		private static final long serialVersionUID = 7286104753228820047L;
		JTextArea question = new JTextArea(2, 10);
		JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		ArrayList<RemovableTextField> options = new ArrayList<RemovableTextField>();

		JButton add = new JButton("+");

		CCheckAllQC() {
			super(7);

			setLayout(new BorderLayout());

			for (int i = 0; i < defaultNumOptions; i++) {
				RemovableTextField temp = new RemovableTextField();
				temp.getCheckBox().setText("Correct");
				options.add(temp);
				temp.getButton().addActionListener(this);
				temp.getCheckBox().addActionListener(this);
				panel.add(temp);
			}

			panel.add(add);
			options.get(0).getCheckBox().setSelected(true);
			options.get(0).getField().setBackground(new Color(190, 190, 255));
			options.get(0).getButton().setEnabled(false);

			add.addActionListener(this);

			add(panel, BorderLayout.SOUTH);
			add(new JScrollPane(question), BorderLayout.CENTER);
		}

		CCheckAllQC(Question q) {
			this();
			question.setText(q.text);
			options.clear();
			panel.removeAll();

			String[] asdf = q.correct.split(",");
			boolean[] ans = new boolean[q.options.length];

			for (int i = 0; i < ans.length; i++)
				for (String s : asdf)
					if (Integer.parseInt(s) == i)
						ans[i] = true;

			for (int i = 0; i < q.options.length; i++) {
				RemovableTextField temp = new RemovableTextField();
				temp.getField().setText(q.options[i]);
				temp.setCheckBox(new JCheckBox("Correct"));
				options.add(temp);
				temp.getButton().addActionListener(this);
				temp.getCheckBox().addActionListener(this);
				panel.add(temp);

				if (ans[i]) {
					temp.getCheckBox().setSelected(true);
					temp.getField().setBackground(new Color(190, 190, 255));
					temp.getButton().setEnabled(false);
				} else
					temp.getCheckBox().setSelected(false);

			}
		}

		public void actionPerformed(ActionEvent evt) {
			if (evt.getSource() == add) {
				RemovableTextField temp = new RemovableTextField();
				temp.getCheckBox().setText("Correct");
				options.add(temp);
				temp.getButton().addActionListener(this);
				temp.getCheckBox().addActionListener(this);
				panel.add(temp);
				panel.remove(add);
				panel.add(add);
			}
			for (int i = 0; i < options.size(); i++) {
				if (evt.getSource() == options.get(i).getButton()) {
					panel.remove(options.get(i));
					options.remove(i);
				} else if (evt.getSource() == options.get(i).getCheckBox()) {
					boolean bool = options.get(i).getCheckBox().isSelected();
					options.get(i).getField().setBackground(bool ? new Color(190, 190, 255) : Color.WHITE);
					options.get(i).getButton().setEnabled(!bool);
				}
			}
			panel.revalidate();
			panel.repaint();
			revalidate();
			// panel.setPreferredSize(new Dimension(panel.getWidth(),40));
		}

		public Question getQuestion() {
			String[] opt = new String[options.size()];
			String selected = "";
			for (int i = 0; i < opt.length; i++) {
				opt[i] = options.get(i).getField().getText();
				if (options.get(i).getCheckBox().isSelected())
					selected += (i == 0 ? "" : ",") + i;
			}
			return new Question(test.getName(), 7, defaultWorth, question.getText(), selected, opt);
		}
	}

	public class CFreeResponseQC extends CreateQuestionComponent {
		private static final long serialVersionUID = -159961897289062573L;
		JTextArea question = new JTextArea(4, 4);
		JTextArea rubric = new JTextArea(10, 10);

		CFreeResponseQC() {
			super(8);
			setLayout(new BorderLayout());

			add(new JScrollPane(question), BorderLayout.NORTH);
			add(new JScrollPane(rubric), BorderLayout.CENTER);

			question.setText("Enter question here");
			add(new JLabel("<html>Enter rubric<br>and/or grading<br>thoughts here</html>"), BorderLayout.WEST);
		}

		CFreeResponseQC(Question q) {
			this();
			question.setText(q.text);

			try {
				File f = new File("Links/Keys/" + q.correct);
				if (f.exists())
					rubric.setText(Methods.getFileContents(f));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public boolean saveGivenAnswerTo(File f) {
			try {
				FileWriter fw = new FileWriter(f);
				fw.write(rubric.getText());
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return true;
		}

		public Question getQuestion() {
			return new Question(test.getName(), 8, defaultWorth, question.getText(), "NONE");
		}
	}

	public static void main(String[] args) throws Exception {
		JudgementDriver.main(args);
	}
}