package admin;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;

import util.Colorful;
import util.Methods;
import framework.Course;
import framework.User;

public class CreateUserPanel extends Colorful implements ActionListener {
	private static final long serialVersionUID = -6517634524608966271L;
	javax.swing.event.ChangeListener listener;
	JLabel usernameL = new JLabel("Username: ");
	JLabel passwordL = new JLabel("Password: ");
	JLabel confirmL = new JLabel("Confirm:");
	JLabel fNameL = new JLabel("First Name:");
	JLabel lNameL = new JLabel("Last Name:");
	JLabel classL = new JLabel("Class:");
	JLabel genderL = new JLabel("Gender: ");
	JLabel gradeL = new JLabel("Grade: ");
	JLabel courseL = new JLabel("Courses: (not set in stone)");

	JTextField usernameF = new JTextField();
	JPasswordField passwordF = new JPasswordField();
	JPasswordField confirmF = new JPasswordField();
	JTextField fNameF = new JTextField();
	JTextField lNameF = new JTextField();
	JComboBox<String> genderF = new JComboBox<String>(new String[] { "Male", "Female" });
	JComboBox<String> gradeF = new JComboBox<String>(new String[] { "7", "8", "9", "10", "11", "12" });
	ArrayList<JCheckBox> courses = new ArrayList<JCheckBox>();
	JPanel courseP = new JPanel();
	JScrollPane courseS = new JScrollPane(courseP);
	boolean done = false;

	boolean isAdding = true;

	JButton add = new JButton();

	public CreateUserPanel(javax.swing.event.ChangeListener listener, User u, String txt) {
		super(new Color(200, 200, 250), Color.BLACK);
		if (u != null) {
			usernameF.setText(u.id);
			passwordF.setText(u.password);
			lNameF.setText(u.lName);
			fNameF.setText(u.fName);
		}

		courseP.setLayout(new BoxLayout(courseP, BoxLayout.Y_AXIS));
		for (Course c : Course.allCourses) {
			JCheckBox jcb = new JCheckBox(c.name);
			jcb.setForeground(Color.WHITE);

			if (u != null && Methods.contains(u.classes, c.getNumber()))
				jcb.setSelected(true);
			else
				jcb.setSelected(false);
			jcb.setOpaque(false);
			courses.add(jcb);
			courseP.add(jcb);
		}

		setBorder(new javax.swing.border.LineBorder(Color.BLACK));

		setLayout(null);
		this.listener = listener;
		setPreferredSize(new Dimension(450, 210));

		usernameL.setBounds(20, 10, 70, 21);
		passwordL.setBounds(40, 34, 70, 21);
		confirmL.setBounds(40, 58, 70, 21);
		fNameL.setBounds(20, 82, 70, 21);
		lNameL.setBounds(20, 106, 70, 21);
		gradeL.setBounds(20, 130, 80, 21);
		genderL.setBounds(20, 154, 80, 21);
		courseL.setBounds(270, 5, 160, 30);

		usernameF.setBounds(90, 10, 120, 21);
		passwordF.setBounds(110, 34, 90, 21);
		confirmF.setBounds(110, 58, 90, 21);
		fNameF.setBounds(90, 82, 100, 21);
		lNameF.setBounds(90, 106, 100, 21);
		gradeF.setBounds(90, 130, 80, 21);
		genderF.setBounds(90, 154, 80, 21);

		add.setBounds(200, 130, 90, 70);
		add.setBackground(Color.BLUE);
		add.setForeground(Color.ORANGE);
		add.setFont(new Font("SERIF", Font.BOLD, 18));
		add.setBorder(new BevelBorder(BevelBorder.RAISED));
		add.addActionListener(this);
		add.setText(txt);
		courseS.setBounds(300, 35, 140, 170);

		courseS.setOpaque(false);
		courseS.getViewport().setOpaque(false);
		courseP.setOpaque(false);
		courseS.setBorder(null);
		courseL.setForeground(Color.CYAN);

		add(add);
		add(usernameL);
		add(passwordL);
		add(confirmL);
		add(courseL);
		add(fNameL);
		add(lNameL);
		add(usernameF);
		add(gradeF);
		add(genderF);
		add(gradeL);
		add(genderL);
		add(passwordF);
		add(confirmF);
		add(fNameF);
		add(lNameF);
		add(courseS);
	}

	CreateUserPanel(javax.swing.event.ChangeListener listener, String txt) {
		this(listener, null, txt);
	}

	CreateUserPanel(javax.swing.event.ChangeListener listener, String txt, boolean add) {
		this(listener, null, txt);
		this.isAdding = add;
	}

	public CreateUserPanel(javax.swing.event.ChangeListener listener) {
		this(listener, null, "Add Me!");
	}

	public User getUser() {
		String error = "Your request could not be completed because\n\n";
		String tab = "        - ";
		boolean isError = false;
		if (!new String(passwordF.getPassword()).equals(new String(confirmF.getPassword()))) {
			error += tab + "the passwords didn't match\n";
			isError = true;
		}
		if (passwordF.getPassword().length == 0) {
			error += tab + "you didn't enter a password\n";
			isError = true;
		} else if (passwordF.getPassword().length < 3) {
			error += tab + "the password was less than 3 characters\n";
			isError = true;
		}
		if (usernameF.getText().equals("")) {
			error += tab + "you didn't enter a username\n";
			isError = true;
		}
		if (fNameF.getText().equals("")) {
			error += tab + "you didn't enter your first name\n";
			isError = true;
		}
		if (lNameF.getText().equals("")) {
			error += tab + "you didn't enter your last name\n";
			isError = true;
		}
		for (User u : User.allUsers) {
			if (u.id.equals(usernameF.getText()) && isAdding) {
				error += "The specified username already exists\n";
				isError = true;
			}
		}
		if (isError) {
			JOptionPane.showMessageDialog(null, error, "Problems, Problems...", JOptionPane.WARNING_MESSAGE);
			return null;
		}
		// JOptionPane.showMessageDialog(null, "Thank you!");
		User u = new User(fNameF.getText(), lNameF.getText(), usernameF.getText(), new String(passwordF.getPassword()),
				genderF.getSelectedIndex() == 0, gradeF.getSelectedIndex() + 7);

		int num = 0;
		for (JCheckBox bx : courses)
			if (bx.isSelected())
				num++;

		int[] classes = new int[num];
		num = 0;
		for (int i = 0; i < courses.size(); i++)
			if (courses.get(i).isSelected())
				classes[num++] = Course.getByName(courses.get(i).getText()).getNumber();

		return u;
	}

	public void setUser(User u) {
		if (u != null) {
			usernameF.setText(u.id);
			passwordF.setText(u.password);
			confirmF.setText(u.password);
			lNameF.setText(u.lName);
			fNameF.setText(u.fName);
			gradeF.setSelectedIndex(Math.min(u.grade - 7, 5));
			genderF.setSelectedIndex(0);

			for (JCheckBox bx : courses)
				bx.setSelected(false);

			for (JCheckBox bx : courses)
				for (int i : u.classes)
					if (Course.getByNumber(i).name.equals(bx.getText()))
						bx.setSelected(true);
		}
	}

	public void clear() {
		usernameF.setText("");
		lNameF.setText("");
		fNameF.setText("");
		passwordF.setText("");
		confirmF.setText("");
		gradeF.setSelectedItem(0);
		genderF.setSelectedIndex(0);

		for (JCheckBox c : courses)
			c.setSelected(false);
	}

	public static User createAndShowDialog() {
		return new CreateUserPanel(null).showDialog();
	}

	public User showDialog() {
		done = false;
		JDialog dialog = new JDialog();
		dialog.add(this);
		dialog.pack();
		dialog.setLocationRelativeTo(null);

		dialog.setVisible(true);
		dialog.setAlwaysOnTop(true);

		while (!done)
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}

		dialog.setVisible(false);
		return getUser();
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == add && evt.getID() == ActionEvent.ACTION_FIRST) {
			done = true;
			if (getUser() == null)
				return;

			if (listener != null)
				listener.stateChanged(new javax.swing.event.ChangeEvent(this));
		}
	}
}