package admin;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import login.Menu;
import testing.Tester;
import util.BarGraph;
import util.Colorful;
import util.DynamicSizeList;
import util.Encoder;
import util.GradientField;
import util.Methods;
import util.PieGraph;
import util.RadialColorful;
import util.SearchList;
import util.SearchObject;
import framework.Administrator;
import framework.Course;
import framework.JudgementDriver;
import framework.MenuListener;
import framework.Question;
import framework.Score;
import framework.Test;
import framework.User;

public class AdminControlPanel extends RadialColorful implements ActionListener, ListSelectionListener, MenuListener {
	private static final long serialVersionUID = 1496902874854321128L;
	public static final Image ARROW = new ImageIcon("Resources/ARROW_II.png").getImage();
	JTextField search = new JTextField();
	JLabel searchL = new JLabel("Search: ");

	DynamicSizeList<SearchObject> dsl = new DynamicSizeList<SearchObject>(new ArrayList<SearchObject>(), search, null);

	DisplayInfoPanel dip = new DisplayInfoPanel();
	TestCreator creator;
	GradingPanel grades = new GradingPanel();
	ActivityCreator activityCreator = new ActivityCreator();
	// EditClassesPanel changeCourses = new EditClassesPanel();

	JButton createUser = new JButton("Create User");

	JButton logout = new JButton("Log out", new ImageIcon("Resources/key.png"));
	JButton exit = new JButton("Exit", new ImageIcon("Resources/exit.png"));

	JTabbedPane container = new JTabbedPane(JTabbedPane.RIGHT);
	JLabel content;
	Menu menu;
	Tester tester = new Tester(this);

	// modes: Look at data, Create/change tests, Host activity, take test,
	// grading.

	JudgementDriver owner;
	Administrator admin;

	public AdminControlPanel(JudgementDriver jb, Administrator admin) {
		super(300, new Point(0, 0), new Point(140, 140), new Color(0, 150, 0), new Color(0, 0, 150),
				new Color(0, 0, 0), new Color(242, 207, 141));

		owner = jb;
		this.admin = admin;
		menu = new Menu(this);

		creator = new TestCreator(admin);
		menu.setTheme(Color.WHITE);

		setLayout(null);
		setOpaque(false);
		menu.setUser(admin);

		// container.setBorder(new javax.swing.border.EmptyBorder(1,1,1,1));
		// container.setOpaque(false);
		container.setUI(new javax.swing.plaf.basic.BasicTabbedPaneUI() {
			protected void paintTabBackground(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h,
					boolean isSelected) {
				if (!isSelected)
					super.paintTabBackground(g, tabPlacement, tabIndex, x, y, w, h, isSelected);
				else {
					g.setColor(Color.BLACK);
					g.fillRect(x, y, w, h);
				}
			}

			protected void paintContentBorder(Graphics g, int tabPlacement, int selectedIndex) {
			}
		});

		search.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				if (arg0.getOppositeComponent() != dsl) {
					System.out.println("NOT CHANGED TO DSL");
					search.setText("");
					dsl.search("H%#@ELLO W%^q#ORLD MY2$NA�8ME $#^ IS SILLY PaANTS~~");
				}
			}

		});
		dsl.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				if (arg0.getOppositeComponent() != search) {
					System.out.println(" NOT CHANGED TO SEARCH");
					search.setText("");
					dsl.search("H%#@ELLO W%^q#ORLD MY2$NA�8ME $#^ IS SILLY PaANTS~~");
				}
			}

		});

		content = new JLabel("Welcome!", JLabel.CENTER);
		content.setFont(new Font("SERIF", Font.BOLD, 45));
		content.setForeground(new Color(120, 0, 0));
		content.setOpaque(true);
		content.setBackground(new Color(0, 0, 0, 50));

		JPanel p = new JPanel(null);
		p.add(menu);
		p.add(tester);
		tester.setVisible(false);

		container.addTab("Home", null, content);
		container.addTab("Review", null, dip);
		container.addTab("Create", null, creator);
		container.addTab("Activity", null, activityCreator);
		container.addTab("Grading", null, grades);
		container.addTab("Classes", null, p);

		container.setTabComponentAt(0, new JLabel(new ImageIcon("Resources/home.png")));
		container.setTabComponentAt(1, new JLabel(new ImageIcon("Resources/review.png")));
		container.setTabComponentAt(2, new JLabel(new ImageIcon("Resources/create.png")));
		container.setTabComponentAt(3, new JLabel(new ImageIcon("Resources/activity.png")));
		container.setTabComponentAt(4, new JLabel(new ImageIcon("Resources/grade.png")));
		container.setTabComponentAt(5, new JLabel(new ImageIcon("Resources/mask.png")));

		dsl.setBounds(170, 32, 230, 0);

		exit.addActionListener(this);
		logout.addActionListener(this);
		dsl.addListSelectionListener(this);

		add(searchL);
		add(dsl);
		add(container);
		add(exit);
		add(dsl);
		add(logout);
		add(search);

		searchL.setForeground(Color.WHITE);

		dsl.setBorder(new javax.swing.border.LineBorder(Color.BLACK, 2));
		logout.setFocusPainted(false);
		exit.setFocusPainted(false);
		dip.adjustListSizes();

		for (Test t : Test.getAvailableTests(admin))
			dsl.total.add(t);
		for (Course c : Course.allCourses)
			dsl.total.add(c);
		for (User u : (admin == null ? new ArrayList<User>() : admin.getPeers()))
			dsl.total.add(u);

		setComponentZOrder(dsl, 0);
		setAdmin(admin);
	}

	public void reset() {
		dip.reset();
		dsl.total.clear();
		container.setSelectedIndex(0);
	}

	public void setBounds(int a, int b, int c, int d) {
		search.setBounds(130, 5, 270, 25);
		searchL.setBounds(30, 5, 100, 25);
		container.setBounds(20, 60, c - 50, d - 120);
		exit.setBounds(120, d - 50, 120, 45);
		logout.setBounds(0, d - 50, 120, 45);

		menu.setBounds(0, 0, c - 50, d - 120);
		tester.setBounds(0, 0, c - 50, d - 120);

		super.setBounds(a, b, c, d);
		dip.adjustListSizes();
	}

	@Override
	public void startTest(Test t) {
		tester.startTest(t);
		tester.setUser(admin);
		tester.setVisible(true);
		menu.setVisible(false);
	}

	public void setAdmin(Administrator a) {
		this.admin = a;
		creator.setAdmin(a);
		dip.update();
		grades.setOwner(a);
		// changeCourses.update();
		menu.setUser(admin);
		dip.update();

		for (Test t : Test.getAvailableTests(admin))
			dsl.total.add(t);
		for (Course c : Course.allCourses)
			dsl.total.add(c);
		for (User u : (admin == null ? new ArrayList<User>() : admin.getPeers()))
			dsl.total.add(u);

		if (a != null) {
			content.setText("<HTML> Welcome " + a.lName + "<BR>"
					+ "<center><font color=\"#0000ff\"> - Grading</center> </HTML>");
		}
	}

	public void toMenu() {
		tester.setVisible(false);
		menu.setVisible(true);
	}

	public void valueChanged(ListSelectionEvent evt) {
		if (evt.getSource() == dsl) {
			if (dsl.getSelectedIndex() < 0)
				return;
			if (dsl.poss.get(dsl.getSelectedIndex()) instanceof Course)
				dip.setData((Course) dsl.poss.get(dsl.getSelectedIndex()));
			else if (dsl.poss.get(dsl.getSelectedIndex()) instanceof User)
				dip.setData((User) dsl.poss.get(dsl.getSelectedIndex()));
			else if (dsl.poss.get(dsl.getSelectedIndex()) instanceof Test)
				dip.setData((Test) dsl.poss.get(dsl.getSelectedIndex()));
		}
	}

	public void keyPressed(KeyEvent evt) {
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == logout) {
			owner.logout();
		} else if (evt.getSource() == exit)
			System.exit(0);
	}

	public static void drawSegment(Graphics g, Point p, double theta, int r, int w, Color c) {
		g.setColor(c);
		g.drawLine(p.x + (int) ((r - w / 2) * Math.sin(theta)), p.y - (int) ((r - w / 2) * Math.cos(theta)), p.x
				+ (int) ((r + w / 2) * Math.sin(theta)), p.y - (int) ((r + w / 2) * Math.cos(theta)));
	}

	public static void drawCircularText(Graphics2D g, Point p, String s, double theta, int r, int w, Color c) {
		g.setColor(c);
		g.translate(p.x + (int) ((r - w / 3) * Math.sin(theta)), p.y - (int) ((r - w / 3) * Math.cos(theta)));
		g.rotate(theta);
		g.drawString(s, -g.getFontMetrics().stringWidth(s) / 2, 0);
		g.rotate(-theta);
		g.translate(-p.x - (int) ((r - w / 3) * Math.sin(theta)), -p.y + (int) ((r - w / 3) * Math.cos(theta)));
	}

	// private class EditClassesPanel extends Colorful implements ActionListener
	// {
	// private static final long serialVersionUID = -2002329977975462057L;
	//
	// JList list = new JList();
	//
	// JPanel side = new JPanel();
	// JButton create = new JButton("+");
	// JButton remove = new JButton("-");
	// JButton rename = new JButton(">");
	// JButton time = new JButton("T");
	//
	// public EditClassesPanel() {
	// super(Color.WHITE, new Color(0, 0, 0, 0));
	// setLayout(new BorderLayout());
	//
	// list.setListData(Methods.createArray(Course.getCourses(admin),
	// new Course[Course.getCourses(admin).size()]));
	//
	// side.setLayout(new FlowLayout(FlowLayout.CENTER));
	// side.add(create);
	// side.add(remove);
	// side.add(rename);
	// side.add(time);
	//
	// time.addActionListener(this);
	// create.addActionListener(this);
	// rename.addActionListener(this);
	// remove.addActionListener(this);
	//
	// add(side, BorderLayout.SOUTH);
	// add(list, BorderLayout.CENTER);
	//
	// update();
	// }
	//
	// public void update() {
	// list.setListData(Methods.createArray(Course.getCourses(admin),
	// new Course[Course.getCourses(admin).size()]));
	// }
	//
	// }

	private class DisplayInfoPanel extends Colorful implements javax.swing.event.ChangeListener, ListSelectionListener,
			ActionListener {
		String[] options = { "Class", "Student", "Test" };

		private static final long serialVersionUID = -7030057068279919016L;
		JPanel displayTest = new JPanel(null);
		BarGraph testGraph = new BarGraph("Statistics", "");

		JPanel displayUser = new JPanel(null);
		BarGraph userGraph = new BarGraph("Test", "Score");
		CreateUserPanel userDisplay = new CreateUserPanel(this, "Change", false);

		JPanel displayQuestion = new JPanel(null);
		JLabel questionDisp = new JLabel();
		JTextArea qinfo = new JTextArea();
		PieGraph qGraph = new PieGraph();

		GradientField percentCorrect = new GradientField(new Color(0, 0, 0, 0), Color.BLACK, new Point(230, 25),
				new Point(170, 0));

		JPanel displayCourse = new JPanel(null);
		BarGraph classGraph = new BarGraph("Class", "Average Score");
		ChangeCoursePanel courseDisplay = new ChangeCoursePanel(null);

		boolean running = false, updated = false;

		SearchList<User> dslU;
		SearchList<Test> dslT;
		SearchList<Question> dslQ;
		SearchList<Course> dslC;

		JScrollPane js1, js2, js3, js4;

		JButton create = new JButton("Create", new ImageIcon("Resources/Other/create.png")), remove = new JButton(
				"Remove", new ImageIcon("Resources/wrong.png"));

		int selected = -1, createTo = 0;
		double sliding = -.5, createSlide = 0;

		int r = 200;

		User user;

		DisplayInfoPanel() {
			// super(new Color(0, 0, 0, 0), new Color(0, 0, 0, 180), new Point(
			// 1000, 600), new Point(500, 400));\
			super(new Color(0, 0, 0, 0), Color.BLACK);
			setLayout(null);

			dslU = new SearchList<User>(((admin == null) ? new ArrayList<User>() : admin.getPeers()), null, null);
			dslT = new SearchList<Test>(Test.getAvailableTests(admin), null, null);
			dslQ = new SearchList<Question>(new ArrayList<Question>(), null, null);
			dslC = new SearchList<Course>(Course.getCourses(admin), null, null);

			js1 = new JScrollPane(dslC);
			js2 = new JScrollPane(dslU);
			js3 = new JScrollPane(dslT);
			js4 = new JScrollPane(dslQ);

			setOpaque(false);

			create.setPressedIcon(new ImageIcon("Resources/Other/SELECTED_CREATE.png"));
			remove.setPressedIcon(new ImageIcon("Resources/Other/SELECTED_WRONG.png"));

			create.setRolloverIcon(new ImageIcon("Resources/Other/ROLLOVER_CREATE.png"));
			remove.setRolloverIcon(new ImageIcon("Resources/Other/ROLLOVER_WRONG.png"));

			js4.setBounds(10, 310, 150, 370);
			testGraph.setBounds(310, 10, 420, 240);
			displayQuestion.setBounds(170, 350, 550, 330);

			qinfo.setForeground(Color.WHITE);
			qinfo.setLineWrap(true);
			qinfo.setWrapStyleWord(true);
			questionDisp.setForeground(Color.CYAN);

			qinfo.setOpaque(false);
			qinfo.setEditable(false);
			userDisplay.setBounds(20, 10, userDisplay.getPreferredSize().width, userDisplay.getPreferredSize().height);
			userGraph.setBounds(100, userDisplay.getPreferredSize().height + 30, 500, 300);

			courseDisplay.setBounds(20, 10, 300, 210);
			classGraph.setBounds(100, 250, 500, 300);

			add(remove);
			add(create);

			displayTest.add(displayQuestion);
			displayTest.add(testGraph);
			displayTest.add(js4);

			displayCourse.add(courseDisplay);
			displayCourse.add(classGraph);

			displayUser.add(userDisplay);
			displayUser.add(userGraph);

			displayQuestion.add(qGraph);
			displayQuestion.add(questionDisp);
			displayQuestion.add(qinfo);
			displayQuestion.add(percentCorrect);

			qGraph.setBounds(210, 75, 330, 230);
			questionDisp.setBounds(0, 0, 550, 70);
			qinfo.setBounds(10, 80, 190, 230);
			questionDisp.setHorizontalAlignment(JLabel.CENTER);

			questionDisp.setBackground(Color.ORANGE);
			percentCorrect.setBounds(310, 300, 230, 25);

			percentCorrect.setOpaque(true);

			displayTest.setBounds(240, 20, 750, 700);
			displayUser.setBounds(240, 20, 750, 700);
			displayCourse.setBounds(240, 20, 750, 700);

			displayTest.setOpaque(false);
			displayUser.setOpaque(false);
			displayQuestion.setOpaque(false);
			displayCourse.setOpaque(false);

			dslC.addListSelectionListener(this);
			dslU.addListSelectionListener(this);
			dslT.addListSelectionListener(this);
			dslQ.addListSelectionListener(this);

			create.addActionListener(this);
			remove.addActionListener(this);

			js1.setDoubleBuffered(true);
			js2.setDoubleBuffered(true);
			js3.setDoubleBuffered(true);

			js1.setBorder(new javax.swing.border.LineBorder(Color.BLACK, 2));
			dslU.setBackground(new Color(255, 255, 200));
			js2.setBorder(new javax.swing.border.LineBorder(Color.BLACK, 2));
			dslT.setBackground(new Color(200, 255, 200));
			js3.setBorder(new javax.swing.border.LineBorder(Color.BLACK, 2));
			dslQ.setBackground(new Color(200, 200, 255));

			js4.setBorder(new javax.swing.border.LineBorder(Color.BLACK, 2));
			dslC.setBackground(new Color(200, 255, 255));
			percentCorrect.setHorizontalAlignment(JLabel.CENTER);

			addMouseListener(new MouseHandler());
			createClearButton(create);
			createClearButton(remove);

			add(js1);
			add(js3);
			add(js2);

			add(displayTest);
			add(displayUser);
			add(displayCourse);
			setDisplay(-1);
		}

		public void reset() {
			selected = -1;
			createTo = 0;
			sliding = -.5;
			createSlide = 0;

			adjustListSizes();
			setDisplay(-1);
		}

		@Override
		public void actionPerformed(ActionEvent evt) {
			if (evt.getSource() == create) {
				if (selected == 0) {
					ChangeCoursePanel panel = new ChangeCoursePanel(admin);
					Course c = panel.showDialog();

					if (c == null)
						return;

					int[] copy = new int[admin.classes.length + 1];
					for (int i = 0; i < admin.classes.length; i++)
						copy[i] = admin.classes[i];

					copy[admin.classes.length] = c.getNumber();
					admin.classes = copy;

					Course.allCourses.add(c);
					Administrator.saveAll();
					Course.saveAll();
					update();
					creator.setAdmin(admin);
				} else if (selected == 1) {
					new Thread(new Runnable() {
						public void run() {
							CreateUserPanel panel = new CreateUserPanel(null);
							User u = panel.showDialog();

							if (u == null)
								return;

							User.allUsers.add(u);
							User.saveAll();
							update();
							creator.setAdmin(admin);
						}
					}).start();

				} else if (selected == 2) {
					container.setSelectedIndex(2);
				}
			} else if (evt.getSource() == remove) {
				if (selected == 0 && JOptionPane.showConfirmDialog(null, "You sure?") == 0) {
					if (dslC.getSelectedIndex() >= 0)
						Course.allCourses.remove(dslC.poss.get(dslC.getSelectedIndex()));
					Course.saveAll();
				} else if (selected == 1 && JOptionPane.showConfirmDialog(null, "You sure?") == 0) {

					if (dslU.getSelectedIndex() >= 0) {
						if (dslU.poss.get(dslU.getSelectedIndex()).isAdmin()) {
							Administrator.allAdmins.remove((Administrator) (dslU.poss.get(dslU.getSelectedIndex())));
							Administrator.saveAll();

						} else {
							User.allUsers.remove(dslU.poss.get(dslU.getSelectedIndex()));
							User.saveAll();

						}
						update();

					}
				}
			}/*
			 * else if (evt.getSource() == rename) { if (list.getSelectedIndex()
			 * >= 0) { String str = JOptionPane.showInputDialog("New name:");
			 * Course.allCourses.get(list.getSelectedIndex()).name = str == null
			 * ? Course.allCourses .get(list.getSelectedIndex()).name : str;
			 * Course.saveAll(); } } else if (evt.getSource() == time) { if
			 * (list.getSelectedIndex() >= 0) { String str =
			 * JOptionPane.showInputDialog("New time:");
			 * Course.getCourses(admin).get(list.getSelectedIndex()).time = str
			 * == null ? Course.allCourses .get(list.getSelectedIndex()).time :
			 * str; Course.saveAll(); } }
			 */
			dslC.setListData(Course.getCourses(admin));
		}

		public void createClearButton(JButton b) {
			b.setContentAreaFilled(false);
			b.setBorder(null);
			b.setFocusPainted(false);
		}

		public void setBounds(int a, int b, int c, int d) {
			super.setBounds(a, b, c, d);

			create.setBounds(0, d - 100, 130, 50);
			remove.setBounds(0, d - 50, 130, 50);
		}

		public void update() {
			dslU.setListData(admin == null ? new ArrayList<User>() : admin.getPeers());
			dslT.setListData(Test.getAvailableTests(admin));
			dslC.setListData(Course.getCourses(admin));
		}

		public void adjustListSizes() {
			int d = getHeight() - 270;

			int y1 = (int) Math.min(Math.max(((2 * options.length) * d * (Math.PI / (4 * options.length) - sliding))
					/ Math.PI, 0), d - 30);
			int y2 = (int) Math.min(Math.max(
					((2 * options.length) * d * (3 * Math.PI / (4 * options.length) - sliding)) / Math.PI, 10), d - 20);
			int y3 = (int) Math.min(Math.max(
					((2 * options.length) * d * (5 * Math.PI / (4 * options.length) - sliding)) / Math.PI, 20), d - 10);
			int y4 = (int) Math.min(Math.max(
					((2 * options.length) * d * (7 * Math.PI / (4 * options.length) - sliding)) / Math.PI, 30), d);

			/*
			 * int y5 = (int) Math.min(Math.max(((2 * options.length) * d * (9
			 * Math.PI / (4 * options.length) - sliding))
			 * / Math.PI, 40), d);
			 */

			js1.setBounds(10, 35 + y1, 150, y2 - y1 - 5);
			js2.setBounds(10, 35 + y2, 150, y3 - y2 - 5);
			js3.setBounds(10, 35 + y3, 150, y4 - y3 - 5);

			js1.revalidate();
			js2.revalidate();
			js3.revalidate();
		}

		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D g2d = (Graphics2D) g;

			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g2d.setStroke(new BasicStroke(40));
			g2d.setPaint(Color.BLACK);
			g2d.drawOval(0 - r, getHeight() - r, 2 * r, 2 * r);
			g2d.setStroke(new BasicStroke(4));
			g2d.setFont(new Font("SERIF", Font.PLAIN, 20));

			for (int n = 0; n < options.length; n++) {
				drawSegment(g2d, new Point(0, getHeight()), (n * Math.PI / (2 * options.length)), r, 30, new Color(0,
						100, 0));
				drawCircularText(g2d, new Point(0, getHeight()), options[n], (n * Math.PI / (2 * options.length))
						+ Math.PI / (4 * options.length), r, 40, selected == n ? Color.ORANGE : Color.WHITE);
			}
			g2d.setColor(new Color(0, 100, 0));

			g2d.drawOval(0 - r + 20, getHeight() - r + 20, 2 * r - 40, 2 * r - 40);
			g2d.drawOval(0 - r - 20, getHeight() - r - 20, 2 * r + 40, 2 * r + 40);

			g2d.setColor(Methods.randomColor(120 + selected * 4, 255));
			int[] x = { (int) (Math.sin(sliding) * (r + 10)), (int) (Math.sin(sliding + .1) * (r + 30)),
					(int) (Math.sin(sliding - .1) * (r + 30)) };
			int[] y = { (int) (getHeight() - Math.cos(sliding) * (r + 10)),
					(int) (getHeight() - Math.cos(sliding + .1) * (r + 30)),
					(int) (getHeight() - Math.cos(sliding - .1) * (r + 30)) };
			g2d.fillPolygon(x, y, 3);
			g2d.setColor(Color.BLACK);
			g2d.drawPolygon(x, y, 3);
			g2d.setColor(Methods.randomColor(120 + selected * 4, 255));

			x = new int[] { 160, 180, 180 };
			y = new int[] { (int) (sliding * 160 * (2 * options.length) / Math.PI) + 30,
					(int) (sliding * 160 * (2 * options.length) / Math.PI) + 20,
					(int) (sliding * 160 * (2 * options.length) / Math.PI) + 40 };

			super.p2 = new Point(30, 30);
			super.p = new Point(getWidth(), getHeight());
			super.paint();

			g2d.setStroke(new BasicStroke(1));
		}

		public class MouseHandler extends MouseAdapter implements MouseListener, Runnable {

			MouseHandler() {
				new Thread(this).start();
			}

			public void mouseReleased(MouseEvent evt) {
				if (distance(evt.getPoint(), new Point(0, getHeight())) < r + 20
						&& distance(evt.getPoint(), new Point(0, getHeight())) > r - 20) {
					double angleFromNorth = Math.atan2(evt.getX(), (getHeight() - evt.getY()));
					int temp = (int) (angleFromNorth / (Math.PI / (2 * options.length)));
					selected = (temp == selected) ? -1 : temp;
				}
			}

			public void run() {
				if (running) {
					return;
				}

				running = true;

				while (true) {
					try {
						Thread.sleep(20);
					} catch (Exception e) {
					}
					createSlide += (createTo - createSlide) / 10;
					sliding += ((selected * Math.PI / (2 * options.length)) + Math.PI / (4 * options.length) - sliding) / 10;
					AdminControlPanel.this.repaint();
					adjustListSizes();
					// r = (int)(100*sliding)+200;

					if (Math
							.abs((selected * Math.PI / (2 * options.length)) + Math.PI / (4 * options.length) - sliding) < .05) {
						if (!updated) {
							updated = true;
							if (selected == 0 && dslC.getSelectedIndex() >= 0)
								dip.setData(dslC.poss.get(dslC.getSelectedIndex()));
							else if (selected == 1 && dslU.getSelectedIndex() >= 0)
								dip.setData(dslU.poss.get(dslU.getSelectedIndex()));
							else if (selected == 2 && dslT.getSelectedIndex() >= 0)
								dip.setData(dslT.poss.get(dslT.getSelectedIndex()));
							else
								dip.setDisplay(-1);
						}
					} else
						updated = false;
				}
			}

		}

		public void valueChanged(ListSelectionEvent evt) {
			if (selected == 0 && dslC.getSelectedIndex() >= 0)
				setData(dslC.poss.get(dslC.getSelectedIndex()));
			else if (selected == 1 && dslU.getSelectedIndex() >= 0)
				setData(dslU.poss.get(dslU.getSelectedIndex()));
			else if (selected == 2 && dslT.getSelectedIndex() >= 0 && evt.getSource() == dslT)
				setData(dslT.poss.get(dslT.getSelectedIndex()));

			else if (dslQ.getSelectedIndex() >= 0) {
				setQData(dslQ.poss.get(dslQ.getSelectedIndex()));
			}
		}

		public void setData(Test test) {
			ArrayList<Course> cs = Course.getCourses(admin);
			double[][] data = new double[cs.size()][4];
			for (int j = 0; j < cs.size(); j++) {
				Course c = cs.get(j);
				ArrayList<User> participents = c.getParticipants();

				ArrayList<Double> scores = new ArrayList<Double>();

				for (int i = 0; i < participents.size(); i++) {
					if (participents.get(i).hasTaken(test)) {
						Score s = Score.fetch(participents.get(i), test);
						scores.add(s.grade() / s.possible());
					}
				}

				double[] array = Methods.unbox(scores.toArray(new Double[scores.size()]));

				data[j][0] = Methods.mean(array);
				data[j][1] = Methods.median(array);
				data[j][2] = Methods.mode(array);
				data[j][3] = Methods.stdev(array);
			}

			testGraph.setData(data);

			String[] classNames = new String[admin.classes.length];
			for (int i = 0; i < classNames.length; i++)
				classNames[i] = Course.getByNumber(admin.classes[i]).name;

			testGraph.setKey(classNames);
			testGraph.setLabels("Avg", "Median", "Mode", "STD");

			dslQ.setListData(test.questions);

			setDisplay(2);
			selected = 2;
			dslT.setSelectedIndex(dslT.poss.indexOf(test));
			container.setSelectedIndex(1);
		}

		public void setData(User u) {
			java.util.List<Test> tests = Test.getAvailableTests(u);
			int size = 0;
			for (Test t : tests)
				if (u.hasTaken(t))
					size++;

			double[] data = new double[size];
			double[] avg = new double[size];
			String[] testNames = new String[size];

			int count = 0;
			for (Test t : tests) {
				if (u.hasTaken(t)) {
					Score s = Score.fetch(u, t);
					data[count] = s.grade() * 100 / s.possible();
					testNames[count] = t.getName();
					avg[count++] = t.getAvg();
				}
			}

			userGraph.setData(data, avg);
			userGraph.setKey(u.id, "Average");
			userGraph.setLabels(testNames);

			userDisplay.setUser(u);
			this.user = u;
			selected = 1;
			setDisplay(1);
			container.setSelectedIndex(1);
			dslU.setSelectedIndex(dslU.poss.indexOf(u));
		}

		public void setData(Course c) {
			java.util.List<Test> tests = new ArrayList<Test>();
			for (Test t : Test.allTests)
				if (Methods.contains(t.classes, c.getNumber()))
					tests.add(t);

			double[] data = new double[tests.size()];
			double[] avg = new double[tests.size()];
			String[] testNames = new String[tests.size()];

			int count = 0;
			for (Test t : tests) {
				double tot = 0;
				int count2 = 0;
				for (User u : c.getParticipants()) {
					if (u.hasTaken(t)) {
						Score s = Score.fetch(u, t);
						tot += s.grade() * 100 / s.possible();
						count2++;
					}
				}
				tot /= count2;

				data[count] = tot;
				testNames[count] = t.getName();

				tot = 0;
				count2 = 0;
				for (User u : User.allUsers) {
					if (u.hasTaken(t)) {
						Score s = Score.fetch(u, t);
						tot += s.grade() * 100 / s.possible();
						count2++;
					}
				}
				tot /= count2;

				avg[count++] = tot;
			}

			classGraph.setData(data, avg);
			classGraph.setKey(c.name, "Average");
			classGraph.setLabels(testNames);

			courseDisplay.setData(c);

			setDisplay(0);
			selected = 0;
			container.setSelectedIndex(1);
			dslC.setSelectedIndex(dslC.poss.indexOf(c));
		}

		public void setQData(Question question) {
			questionDisp.setText("<HTML><center><font size=\"4\">" + question.text.replaceAll("\n", "<br>")
					+ "</font><center></HTML>");
			Map<String, Integer> map = Question.getAllGivenAnswers(question, Test.fromName(question.testName));

			String[] labels = new String[map.keySet().size()];
			double[] data = new double[map.keySet().size()];
			double correct = 0;

			int counter = 0;
			for (String key : map.keySet()) {
				labels[counter] = question.getAnswer(key);
				data[counter] = map.get(key);

				if (question.isCorrect(key))
					correct += map.get(key);
				counter++;
			}
			correct /= Methods.sum(data);

			if (question.teacherMustGrade()) {
				percentCorrect.setBackground(Color.WHITE);
				percentCorrect.setText("Ungradable");

				double pts = 0, total = 0;

				for (java.io.File f : new java.io.File("Results").listFiles()) {
					if (!Encoder.decode(f.getName()).startsWith(question.testName))
						continue;

					String temp = Methods.getFileContents(f).split("\n")[question.index + 4];
					String answer = temp.substring(0, temp.indexOf("\t"));
					total += question.worth;
					pts += Integer.parseInt(answer);
				}

				double[] dat = new double[] { pts, total - pts };

				qGraph.setData(dat);
				qGraph.setLabels(new String[] { "Points earned", "Points not earned" });

			} else {
				percentCorrect.setBackground(Methods.colorMeld(Color.RED, Color.GREEN, correct));
				percentCorrect.setText((int) (correct * 100) + " percent correct");
				qGraph.setData(data);
				qGraph.setLabels(labels);
			}
			qinfo.setText("Answer: " + question.getAnswer(question.correct) + "\n" + "Points: " + question.worth);

			dslQ.setSelectedIndex(dslQ.poss.indexOf(question));
			container.setSelectedIndex(1);
		}

		public void stateChanged(javax.swing.event.ChangeEvent evt) {
			// user = userDisplay.getUser()
			User.allUsers.set(User.allUsers.indexOf(user), userDisplay.getUser());
			User.saveAll();
		}

		void setDisplay(int a) {
			switch (a) {
			case -1:
				create.setBackground(Color.white);
				remove.setBackground(Color.white);
				create.setText("Create");
				remove.setText("Remove");
				break;
			case 0:
				create.setBackground(new Color(200, 255, 255));
				remove.setBackground(new Color(200, 255, 255));
				create.setText("Create Course");
				remove.setText("Remove Course");
				break;
			case 1:
				create.setBackground(new Color(255, 255, 200));
				remove.setBackground(new Color(255, 255, 200));
				create.setText("Create User");
				remove.setText("Remove User");
				break;
			case 2:
				create.setBackground(new Color(200, 255, 200));
				remove.setBackground(new Color(200, 255, 200));
				create.setText("Create Test");
				remove.setText("Remove Test");
				break;
			case 3:
				create.setBackground(new Color(200, 200, 255));
				remove.setBackground(new Color(200, 200, 255));
				create.setText("Create Question");
				remove.setText("Remove Question");
				break;
			}
			create.setEnabled(selected >= 0);
			remove.setEnabled(selected >= 0);
			// create.setVisible(selected >= 0 && selected != 3);

			displayCourse.setVisible(a == 0);
			displayUser.setVisible(a == 1);
			displayTest.setVisible(a == 2);
		}
	}

	public static double distance(java.awt.Point p, java.awt.Point p2) {
		return Math.sqrt((p.x - p2.x) * (p.x - p2.x) + (p.y - p2.y) * (p.y - p2.y));
	}

	@Override
	public void logout() {
		// TODO Auto-generated method stub

	}
}