package admin;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import util.SearchList;
import framework.Administrator;
import framework.JudgementDriver;

public class SuperAdminPanel extends JTabbedPane implements ActionListener {
	private static final long serialVersionUID = -3899070216087732693L;

	JTextField search = new JTextField();
	SearchList<Administrator> admins = new SearchList<Administrator>(Administrator.allAdmins, search, null);
	JButton add = new JButton("Add"), remove = new JButton("Remove");
	AdminControlPanel normal;

	JudgementDriver owner;

	public SuperAdminPanel(JudgementDriver d) {
		this.owner = d;
		normal = new AdminControlPanel(d, Administrator.SUPER_ADMINISTRATOR);

		JPanel bottomWest = new JPanel(new FlowLayout(FlowLayout.CENTER));

		JPanel west = new JPanel(new BorderLayout(10, 10));

		west.setOpaque(false);
		setOpaque(false);
		bottomWest.setOpaque(false);

		bottomWest.add(add);
		bottomWest.add(remove);

		add.addActionListener(this);
		remove.addActionListener(this);

		west.add(search, BorderLayout.NORTH);
		west.add(admins, BorderLayout.CENTER);
		west.add(bottomWest, BorderLayout.SOUTH);

		addTab("Administration Panel", normal);
		addTab("Add or Remove Administrators", west);
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == add) {
			new Thread(new Runnable() {
				public void run() {
					Administrator a = CreateUserPanel.createAndShowDialog().getElevatedSelf();
					Administrator.addAdmin(a);
					admins.setListData(Administrator.allAdmins);
				}
			}).start();
		} else if (evt.getSource() == remove) {
			new Thread(new Runnable() {
				public void run() {
					Administrator a = admins.poss.get(admins.getSelectedIndex());
					Administrator.removeAdmin(a);
					admins.setListData(Administrator.allAdmins);
				}
			}).start();
		}
	}
}