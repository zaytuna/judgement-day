package admin;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import framework.User;

public class ChangeDataPanel extends JPanel implements ActionListener, KeyListener {
	private static final long serialVersionUID = 5199876239142367333L;
	ArrayList<ActionListener> listeners = new ArrayList<ActionListener>();
	public JButton change = new JButton("Save Changes");

	JTextField lNameField = new JTextField();
	JTextField fNameField = new JTextField();
	JTextField passField = new JTextField();
	JTextField ansField = new JTextField();
	JTextField idField = new JTextField();

	JLabel lnLabel = new JLabel("Last Name:");
	JLabel fnLabel = new JLabel("First Name:");
	JLabel psLabel = new JLabel("Password: ");
	JLabel ansLabel = new JLabel("Answer: ");
	JLabel idLabel = new JLabel("ID #: ");
	JComboBox<String> qBox = new JComboBox<String>(new String[] { "What's the first thing you'd save in a fire?",
			"What's your father's middle name?", "Who's your favorite charater from a book?",
			"What do you eat for breakfast?", "What do you like least about school?",
			"What do you like most about school?" });
	JComboBox<String> genderBox = new JComboBox<String>(new String[] { "Male", "Female" });
	JComboBox<String> gradeBox = new JComboBox<String>(new String[] { "7", "8", "9", "10", "11", "12" });

	public ChangeDataPanel() {
		setLayout(null);
		setOpaque(false);
		qBox.setEditable(true);

		lnLabel.setBounds(10, 40, 100, 25);
		fnLabel.setBounds(10, 70, 100, 25);
		psLabel.setBounds(10, 100, 100, 25);
		qBox.setBounds(10, 130, 320, 25);
		ansLabel.setBounds(10, 160, 100, 25);
		idLabel.setBounds(10, 10, 200, 25);
		lNameField.setBounds(110, 40, 220, 25);
		fNameField.setBounds(110, 70, 220, 25);
		passField.setBounds(110, 100, 220, 25);
		ansField.setBounds(110, 160, 220, 25);
		idField.setBounds(110, 10, 220, 25);

		change.setBounds(120, 200, 120, 25);

		add(lnLabel);
		add(fnLabel);
		add(psLabel);
		add(qBox);
		add(lNameField);
		add(fNameField);
		add(passField);
		add(idLabel);
		add(idField);
		add(ansField);
		add(ansLabel);
		add(change);

		lNameField.setEditable(false);
		fNameField.setEditable(false);
		idField.setEditable(false);

		change.addActionListener(this);
		
		passField.addKeyListener(this);
		ansField.addKeyListener(this);
		qBox.addKeyListener(this);
	}

	public void setUser(User u) {
		if (u == null) {
			passField.setText("");
			idField.setText("");
			lNameField.setText("");
			fNameField.setText("");
			ansField.setText("");
			qBox.setSelectedItem("");
		} else {
			passField.setText(u.password);
			idField.setText(u.id);
			lNameField.setText(u.lName);
			fNameField.setText(u.fName);
			ansField.setText(u.answer);
			qBox.setSelectedItem(u.question);
		}
	}

	public User createUser(User old) {
		/*
		 * return new User(fNameField.getText(), lNameField.getText(), idField
		 * .getText(), passField.getText(), (String) qBox
		 * .getSelectedItem(), ansField.getText(), (genderBox
		 * .getSelectedIndex() == 0), gradeBox.getSelectedIndex() + 7);
		 */

		/*
		 * old.fName = fNameField.getText();
		 * old.lName = lNameField.getText();
		 * old.id = idField.getText();
		 */

		old.password = passField.getText();
		old.answer = ansField.getText();
		old.gender = (genderBox.getSelectedIndex() == 0);
		old.grade = (gradeBox.getSelectedIndex() + 7);
		old.question = (String)qBox.getSelectedItem();
		
		passField.setBackground(Color.WHITE);
		ansField.setBackground(Color.WHITE);
		qBox.setBackground(Color.WHITE);

		
		return old;
	}

	public void addActionListener(ActionListener l) {
		listeners.add(l);
	}

	public void actionPerformed(ActionEvent evt) {
		for (ActionListener l : listeners) {
			l.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "Custom"));
		}
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent evt) {
		if (evt.getSource() instanceof JComponent)
			((JComponent) evt.getSource()).setBackground(new Color(255, 200, 200));
	}
}