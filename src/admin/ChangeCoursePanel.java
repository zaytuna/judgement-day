package admin;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import util.Colorful;
import framework.Administrator;
import framework.Course;

public class ChangeCoursePanel extends Colorful {
	private static final long serialVersionUID = 8215620907042609623L;

	JTextField name = new JTextField(), creator = new JTextField(),
			time = new JTextField(), number = new JTextField();

	Administrator admin;

	ChangeCoursePanel(Administrator ad) {
		// super(Color.MAGENTA, Color.CYAN);

		setOpaque(false);

		setLayout(new GridLayout(4, 2));

		creator.setEnabled(false);
		number.setEnabled(false);

		creator.setBackground(new Color(238, 238, 238));
		number.setBackground(new Color(238, 238, 238));

		add(new JLabel("Creator:"));
		add(creator);
		add(new JLabel("Number: "));
		add(number);
		add(new JLabel("Course Name:", JLabel.LEFT));
		add(name);
		add(new JLabel("Time:", JLabel.LEFT));
		add(time);

		setAdmin(ad);
	}

	public void setData(Course c) {
		if (c == null) {
			name.setText("");
			number.setText("");
			time.setText("");
			setAdmin(null);
		} else {
			name.setText(c.name);
			number.setText("" + c.getNumber());
			time.setText(c.time);
			setAdmin(Administrator.fromID(c.teacher));
		}
	}

	public void setAdmin(Administrator a) {
		this.admin = a;

		if (a != null)
			creator.setText(a.lName);
	}

	public Course getCourse() {
		if (name.getText().equals(""))
			return null;
		return new Course(name.getText(), admin.id, time.getText());
	}

	public Course showDialog() {
		JOptionPane.showMessageDialog(this, this);
		return getCourse();
	}
}
