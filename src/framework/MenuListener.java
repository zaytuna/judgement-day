package framework;
public interface MenuListener
{
	public void startTest(Test t);
	public void toMenu();
	public void logout();
}