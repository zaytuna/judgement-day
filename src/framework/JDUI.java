package framework;

import java.awt.Color;
import java.awt.Font;

public class JDUI {

	private JDUI() {

	}

	public Color testColor;
	public Color reviewColor;
	public Color loginColor;
	public Color menuBack;
	public Color reveiewForeground;
	public Color testForeground;
	public Color loginColor2;
	public Color back = Color.black;
	public Color[] loginButtonColors;

	public String logo;
	public boolean marks;
	public boolean music = false;
	public String name;
	public Font questionFont = new Font("SANS_SERIF", Font.ITALIC, 15);

	public static JDUI createGoodUI() {
		JDUI ui = new JDUI();
		ui.testColor = new Color(200, 200, 250);
		ui.reviewColor = new Color(0, 50, 0);
		ui.loginColor = new Color(200, 220, 255);
		ui.menuBack = new Color(224, 204, 152);
		ui.reveiewForeground = Color.white;
		ui.testForeground = new Color(51, 51, 51);
		ui.loginColor2 = new Color(0, 50, 0);
		ui.loginButtonColors = new Color[] { new Color(172, 98, 157),  new Color(200, 152, 150),  new Color(225, 210, 146),
				new Color(187, 157, 89)};//new Color(150, 113, 56) 
		//ui.loginButtonColors = new Color[] { new Color(43, 59, 0), new Color(99, 110, 0), new Color(247, 177, 0),
		//		new Color(107, 0, 25) };
		ui.back = new Color(0, 0, 60);

		ui.logo = "Resources/Icon2.png";
		ui.marks = true;
		ui.music = true;
		ui.name = "OLIVER!!";

		return ui;
	}

	public static JDUI createClientUI() {
		JDUI ui = new JDUI();
		ui.testColor = new Color(224, 204, 152);
		ui.reviewColor = new Color(104, 134, 112);
		ui.loginColor = new Color(224, 204, 152);
		ui.menuBack = new Color(224, 204, 152);
		ui.reveiewForeground = Color.black;
		ui.testForeground = new Color(51, 51, 51);
		ui.back = new Color(25, 50, 25);
		ui.loginColor2 = new Color(0, 20, 0);
		ui.loginButtonColors = new Color[] { new Color(43, 59, 0), new Color(99, 110, 0), new Color(247, 177, 0),
				new Color(107, 0, 25) };

		ui.logo = "Resources/Canyons2.png";
		ui.marks = false;
		ui.name = "test";

		return ui;
	}
}
