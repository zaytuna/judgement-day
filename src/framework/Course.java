package framework;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import util.Methods;
import util.SearchObject;

public class Course implements SearchObject {
	public static ArrayList<Course> allCourses = new ArrayList<Course>();

	public static int maxNum = 1000;

	private int number;
	public String name;
	public String teacher;
	public String time;

	private Course(int num, String name, String teacher, String time) {
		maxNum = num;
		this.number = num;
		this.teacher = teacher;
		this.time = time;
		this.name = name;
	}

	public Course(String name, String teacher, String time) {
		this(maxNum + (int) (Math.random() * 10) + 1, name, teacher, time);
	}

	static {
		try {
			File f = new File("Classes/a7You83562Can't32w65Read246Me0e14.dll");

			if (!f.exists())
				f.createNewFile();

			BufferedReader in = new BufferedReader(new FileReader(f));

			while (true) {
				String temp = in.readLine();

				if (temp == null)
					break;

				allCourses.add(readFrom(temp));
			}
			in.close();
		} catch (Exception e) {
		}
	}

	public int getNumber() {
		return number;
	}

	public static Course getByName(String s) {
		for (Course c : allCourses)
			if (c.name == s)
				return c;
		return null;
	}

	public static Course getByNumber(int i) {
		for (Course c : allCourses)
			if (c.number == i)
				return c;
		return null;
	}

	public static ArrayList<String> getAllCourseNames() {
		ArrayList<String> returnValue = new ArrayList<String>();

		for (Course course : allCourses) {
			returnValue.add(course.name);
		}
		return returnValue;
	}

	public static ArrayList<Course> getCourses(User u) {
		if (u == Administrator.SUPER_ADMINISTRATOR)
			return allCourses;

		if (u == null || u.classes == null)
			return new ArrayList<Course>();

		ArrayList<Course> returnValue = new ArrayList<Course>();

		for (Course course : allCourses) {
			for (int c : u.classes)
				if (c == course.number)
					returnValue.add(course);
		}
		return returnValue;
	}

	public ArrayList<User> getParticipants() {
		ArrayList<User> users = new ArrayList<User>();

		for (User u : User.allUsers)
			if (Methods.contains(u.classes, number))
				users.add(u);

		return users;
	}

	public static ArrayList<String> getCourseNames(User u) {
		ArrayList<String> returnValue = new ArrayList<String>();

		if (u == null || u.classes == null)
			return returnValue;

		for (Course course : allCourses) {
			for (int c : u.classes)
				if (c == course.number || (u == Administrator.SUPER_ADMINISTRATOR))
					returnValue.add(course.name);
		}
		return returnValue;
	}

	public static Course readFrom(String s) {
		String[] split = s.split("\t");
		return new Course(Integer.parseInt(split[0]), split[1], split[2], split[3]);
	}

	public static String writeTo(Course c) {
		return c.number + "\t" + c.name + "\t" + c.teacher + "\t" + c.time;
	}

	public String toString() {
		return number + "        " + name;
	}

	public static void saveAll() {
		try {
			File f = new File("Classes/a7You83562Can't32w65Read246Me0e14.dll");

			if (!f.exists())
				f.createNewFile();

			BufferedWriter out = new BufferedWriter(new FileWriter(f));

			for (Course c : allCourses) {
				out.write(writeTo(c) + "\n");
			}
			out.close();
		} catch (Exception e) {
		}
	}

	@Override
	public String getDisplay() {
		return toString();
	}

	@Override
	public String getOutputString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getSearchableFields() {
		ArrayList<String> list = new ArrayList<String>();

		list.add("" + number);
		list.add(name);
		list.add(time);
		list.add(teacher);

		return list;
	}

	public static int[] getCourseNumbers() {
		int[] numbers = new int[allCourses.size()];

		int count = 0;
		for (Course c : allCourses)
			numbers[count++] = c.getNumber();

		return numbers;
	}
}