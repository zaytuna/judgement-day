package framework;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import util.Encoder;
import util.Methods;

public class Score {
	public Test test;
	public long mstime;
	public int correct, incorrect, blank, total;
	public String[] answers;
	public int[] scores;
	public int ring = 0;

	final static String currentDir = new File("").getAbsolutePath();

	public Score(Test t, int c, int i, int b, int tot, String[] ans, int[] s) {
		test = t;
		correct = c;
		incorrect = i;
		answers = ans;
		blank = b;
		scores = s;
		total = tot;
		ring = (int) (getCorrectPercent() * test.bands);
	}

	public double grade() {
		double score = 0;

		for (int i = 0; i < scores.length; i++)
			score += scores[i];

		return score;
	}

	public void setTime(long time) {
		mstime = time;
	}

	public double possible() {
		double poss = 0;
		for (int i = 0; i < scores.length; i++)
			if (!test.questions.get(i).teacherMustGrade() || answers[i].equals("link-graded"))
				poss += test.questions.get(i).worth;

		return poss;
	}

	public double getUngradedPoints() {
		double poss = 0;
		for (int i = 0; i < scores.length; i++)
			if (test.questions.get(i).teacherMustGrade() && !answers[i].equals("link-graded"))
				poss += test.questions.get(i).worth;

		return poss;
	}

	public double getCorrectPercent() {
		return (double) correct / (double) (correct + incorrect + blank);
	}

	public String toString() {
		return "Score of " + correct + "/" + (blank + incorrect + correct) + " with " + blank + " unanswered";
	}

	/*
	 * Want file in format:
	 * 
	 * 0 ring
	 * 1 NUMTRIES
	 * 2 besttime avgtime lasttime
	 * 3 c,i,b
	 * 4 "Adumbrate"
	 * 5 "Onus"
	 * 6 .. .. ..
	 */

	public static void recordScore(Score s, User u, Test test) {
		try {
			File f = new File(currentDir + "\\Results\\", Encoder.encode(test.getName() + "- " + u.id) + ".rslt");
			System.out.println(f.getAbsolutePath());
			if (f.exists()) {
				String file = Methods.getFileContents(f);
				String[] lines = file.split("\n");
				int numTimes = Integer.parseInt(lines[1]) + 1;
				long avgtime = (Long.parseLong(lines[2].substring(lines[2].indexOf("\t") + 1, lines[2]
						.lastIndexOf("\t")))
						* (numTimes - 1) + s.mstime)
						/ numTimes;
				long besttime = Long.parseLong(lines[2].substring(0, lines[2].indexOf("\t")));

				BufferedWriter out = new BufferedWriter(new FileWriter(f));

				out.write(s.ring + "\n");
				out.write(numTimes + "\n");
				out.write(besttime + "\t" + avgtime + "\t" + s.mstime + "\n");
				out.write(s.correct + "," + s.incorrect + "," + s.blank + "," + s.total);

				for (int i = 0; i < s.answers.length; i++)
					out.write("\n" + s.scores[i] + "\t" + s.answers[i]);

				out.close();
			} else {
				f.createNewFile();
				BufferedWriter out = new BufferedWriter(new FileWriter(f));

				out.write(s.ring + "\n");
				out.write("1\n");
				out.write(s.mstime + "\t" + s.mstime + "\t" + s.mstime + "\n");
				out.write(s.correct + "," + s.incorrect + "," + s.blank + "," + s.total);

				for (int i = 0; i < s.answers.length; i++)
					out.write("\n" + s.scores[i] + "\t" + s.answers[i]);

				out.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Score fetch(User u, Test test) {
		File f = new File(currentDir + "\\Results\\", Encoder.encode(test.getName() + "- " + u.id) + ".rslt");
		String file = Methods.getFileContents(f);
		String[] lines = file.split("\n");

		String[] ans = new String[lines.length - 4];
		int[] sc = new int[lines.length - 4];

		for (int i = 4; i < lines.length; i++) {
			try {
				ans[i - 4] = lines[i].substring(lines[i].indexOf("\t") + 1);
				sc[i - 4] = Integer.parseInt(lines[i].substring(0, lines[i].indexOf("\t")));
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(lines[i]);
			}
		}

		String[] cib = lines[3].split(",");
		return new Score(test, Integer.parseInt(cib[0]), Integer.parseInt(cib[1]), Integer.parseInt(cib[2]), Integer
				.parseInt(cib[3]), ans, sc);
	}

	public static void rewriteGrade(String userID, String testName, int questionNumber, int grade, String link) {
		File f = new File(currentDir + "\\Results\\", Encoder.encode(testName + "- " + userID) + ".rslt");
		System.out.println(f.getAbsolutePath());

		try {
			if (f.exists()) {
				String file = Methods.getFileContents(f);
				String[] lines = file.split("\n");

				FileWriter out = new FileWriter(f);

				out.write(lines[0] + "\n");
				out.write(lines[1] + "\n");
				out.write(lines[2] + "\n");

				String[] ints = lines[3].split(",");
				int cor = Integer.parseInt(ints[0]);
				int icr = Integer.parseInt(ints[1]);
				int bla = Integer.parseInt(ints[2]);
				int tot = Integer.parseInt(ints[3]);

				if (Test.fromName(testName).questions.get(questionNumber).worth == grade)
					cor++;
				else
					icr++;

				out.write(cor + "," + icr + "," + bla + "," + tot);

				for (int i = 4; i < lines.length; i++)
					if (i - 4 == questionNumber)
						out.write("\n" + grade + "\t" + link);
					else
						out.write("\n" + lines[i]);

				out.close();
			}
		} catch (Exception e) {
		}
	}
}