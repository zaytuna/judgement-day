package framework;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Resource extends JLabel implements MouseListener {
	private static final long serialVersionUID = 1750710291451642596L;
	int type; // 0: image, 1:sound

	public Resource(int type, String loc) {
		if (type == 0)
			setIcon(new ImageIcon(loc));
		else if (type == 1)
			setIcon(new ImageIcon("Resources/audio.png"));
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}
}