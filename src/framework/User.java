package framework;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import util.Encoder;
import util.Methods;
import util.SearchObject;

public class User implements SearchObject {
	public static final String DEFAULT_QUESTION = "What is the first thing you'd save in a fire?";
	public static final String DISCLAIMER = "This information won't be released to evil rubber chickens who want to steal you're identity.";

	public static ArrayList<User> allUsers = new ArrayList<User>();

	public String lName;
	public String fName;
	public String id;
	public String password;
	public String answer;
	public String question;
	public boolean gender;// true == male, false = female
	public int grade;

	public int[] classes;

	public User(String fName, String lName, String id, String password, boolean gender, int grade) {
		this(fName, lName, id, password, DEFAULT_QUESTION, "me", gender, grade);
	}

	public User(String fName, String lName, String id, String password, String question, String answer, boolean gender,
			int grade) {
		this.fName = fName;
		this.lName = lName;
		this.id = id;
		this.password = password;
		this.question = question;
		this.answer = answer;
		this.gender = gender;
		this.grade = grade;

		classes = new int[0];
	}

	public boolean isAdmin() {
		return this instanceof Administrator;
	}

	public User() {
	}

	static {
		try {
			File f = new File("Users/users.dll");

			if (!f.exists())
				f.createNewFile();

			BufferedReader in = new BufferedReader(new FileReader(f));

			while (true) {
				String temp = in.readLine();

				if (temp == null)
					break;

				User u = readFrom(temp);
				u.classes = Methods.ensureContainsOnly(u.classes, Course.getCourseNumbers());
				allUsers.add(u);
			}
			in.close();

			for (Administrator a : Administrator.allAdmins)
				allUsers.add(a);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static User readFrom(String line) {
		// line = Encoder.decode(line);
		String[] parts = line.split("\t");
		User u = new User();
		u.id = parts[0];
		u.lName = parts[1];
		u.fName = parts[2];
		u.password = parts[3];
		u.question = parts[4];
		u.answer = parts[5];
		u.gender = parts[6].charAt(0) == 't';
		u.grade = Integer.parseInt(parts[7]);
		u.classes = new int[parts.length - 8];

		for (int i = 8; i < 8 + u.classes.length; i++) {
			u.classes[i - 8] = Integer.parseInt(parts[i]);
		}

		return u;
	}

	public ArrayList<User> getPeers() {
		ArrayList<User> peers = new ArrayList<User>();

		if (this == Administrator.SUPER_ADMINISTRATOR)
			return allUsers;

		for (User u : allUsers) {
			if (!u.isAdmin())
				for (int course : u.classes)
					for (int m : classes)
						if (m == course && m >= 1 && !peers.contains(u))
							peers.add(u);
		}
		return peers;
	}

	public ArrayList<User> getStudents(Course c) {
		ArrayList<User> students = new ArrayList<User>();

		for (User u : allUsers) {
			if (!u.isAdmin())
				for (int i : u.classes)
					if (c.getNumber() == u.classes[i])
						students.add(u);
		}
		return students;
	}

	public static String writeFrom(User u) {
		System.out.println("u.classes: " + Arrays.toString(u.classes));
		String classStr = "";

		for (int i = 0; i < u.classes.length; i++)
			classStr += "\t" + u.classes[i];

		return u.id + "\t" + u.lName + "\t" + u.fName + "\t" + u.password + "\t" + u.question + "\t" + u.answer + "\t"
				+ u.gender + "\t" + u.grade + classStr;
	}

	public int getTimesTaken(Test test) {
		if (test == null)
			return -1;
		if (hasTaken(test))
			return Integer.parseInt(Methods.getFileContents(
					new File("Results", Encoder.encode(test.getName() + "- " + id) + ".rslt")).split("\n")[1]);

		return 0;
	}

	public boolean hasPassed(Test test) {
		if (!hasTaken(test))
			return false;
		return Methods.getFileContents(new File("Results", Encoder.encode(test.getName() + "- " + id) + ".rslt"))
				.charAt(0) == 't';
	}

	public Administrator getElevatedSelf() {
		Administrator a = new Administrator();
		a.answer = answer;
		a.classes = classes;
		a.fName = fName;
		a.lName = lName;
		a.gender = gender;
		a.grade = grade;
		a.id = id;
		a.password = password;
		a.question = question;

		return a;
	}

	public List<String> getSearchableFields() {
		List<String> fields = new ArrayList<String>();
		fields.add(id);
		fields.add(lName);
		fields.add(fName);
		return fields;
	}

	public String getDisplay() {
		return lName + ", " + fName;
	}

	public String getOutputString() {
		return id;
	}

	public static void saveAll() {
		try {
			File f = new File("Users/users.dll");
			BufferedWriter out = new BufferedWriter(new FileWriter(f, false));

			for (User asdf : allUsers) {
				if (asdf.isAdmin())
					continue;
				out.write(writeFrom(asdf) + "\n");
				out.flush();
			}

			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void addUser(User u) {
		allUsers.add(u);
		saveAll();
	}

	public static User getUser(String id, String password) {
		for (User u : allUsers) {
			if (u.id.equals(id) && u.password.equals(password)) {
				return u;
			}
		}
		return null;
	}

	public static User getUser(String id) {
		for (User u : allUsers) {
			if (u.id.equals(id)) {
				return u;
			}
		}
		return null;
	}

	public String toString() {
		return lName + ", " + fName;
	}

	public boolean hasTaken(Test t) {
		if (t == null)
			return false;

		for (String file : new File("Results").list()) {
			file = file.substring(0, file.indexOf(".rslt"));
			if (Encoder.decode(file).equals(t.getName() + "- " + id))
				return true;
		}
		return false;

	}

	public static <T> boolean contains(T[] array, T value) {
		for (T t : array)
			if (t.equals(value))
				return true;
		return false;
	}
}