package framework;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import util.SearchObject;

public class Test implements SearchObject {
	public static final Test EMPTY = new Test("EMPTY");

	/*
	 * class1,class2,class3,class4,...class n, etc. 5 Question 1 6 Question 2 .
	 * . . n+4 Question n
	 */
	public static ArrayList<Test> allTests = new ArrayList<Test>();

	private String name;

	public int bands = 1;// score goes to 100 percent when this is
	// reached.
	public int available = 0;// 0 is always takable, 1 = never takable, 2 =
	// takable when admin is online, 3 = takable by
	// permission, 4 = takable at certain times, 5 = takable when gotten __ on
	// test ___ > 1000, that test, > 100 000 8192
	public int retakes = -1;// infinite retakes
	public int displayMode = 0;// -1 = do your own spacing to a page best to fit
	// dimensions,
	// -2 = all on one page with scroll bar, 1+ = that number of questions to a
	// page.
	public int time = -999; // mins, -999 equivliates to an untimed test.
	public int showAnswerMode = 0;// 0 = show correct answers; 1 = show
	// incorrect questions;
	// 2 = show score; 3 = NOTHING
	public int[] classes;

	// ************************************ NEW VARS **************************
	public boolean canGoBack = true;
	public boolean retainTestOrder = true;
	int questionsToShow = -1;

	public ArrayList<Question> questions = new ArrayList<Question>();

	static {
		refreshTests();
	}

	public Test(String n) {
		this.name = n;
	}

	public static void refreshTests() {
		try {
			File base = new File("Tests/");

			for (File f : base.listFiles()) {
				Test test = new Test(f.getName());
				BufferedReader in = new BufferedReader(new FileReader(f));

				for (int i = 0; true; i++) {
					String temp = in.readLine();

					if (temp == null)
						break;
					if (i == 0)
						test.available = Integer.parseInt(temp);
					if (i == 1)
						test.retakes = Integer.parseInt(temp);
					else if (i == 2)
						test.displayMode = Integer.parseInt(temp);
					else if (i == 3)
						test.time = Integer.parseInt(temp);
					else if (i == 4)
						test.showAnswerMode = Integer.parseInt(temp);
					else if (i == 5) {
						String[] strs = temp.split(",");
						test.classes = new int[strs.length];
						for (int v = 0; v < strs.length; v++) {
							test.classes[v] = Integer.parseInt(strs[v]);
						}
					} else if (i == 6)
						test.bands = Integer.parseInt(temp);
					else if (i >= 7) {
						Question q = Question.parseFrom(temp, test.name);
						q.text = q.text.replaceAll("�", "\n");
						q.index = i - 7;
						q.testName = test.name;
						test.questions.add(q);
					}
				}
				in.close();
				allTests.add(test);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Test fromName(String name) {
		for (Test t : allTests)
			if (t.name.equals(name))
				return t;

		return null;
	}

	public static ArrayList<String> getAvailableTestNames(User u) {
		ArrayList<String> applicableTests = new ArrayList<String>();

		if (u == null || u.classes == null || u.classes.length == 0)
			return applicableTests;

		for (Test t : allTests)
			for (int c : t.classes)
				for (int a : u.classes)
					if (a == c && !applicableTests.contains(t.name))
						applicableTests.add(t.name);

		return applicableTests;
	}

	public static ArrayList<Test> getAvailableTests(User u) {
		ArrayList<Test> applicableTests = new ArrayList<Test>();

		if (u == null || u.classes == null || u.classes.length == 0)
			return applicableTests;

		for (Test t : allTests)
			for (int c : t.classes)
				for (int a : u.classes)
					if (a == c && !applicableTests.contains(t))
						applicableTests.add(t);

		return applicableTests;
	}

	public String getDisplay() {
		return name;
	}

	public String getOutputString() {
		return name;
	}

	public String getName() {
		return name;
	}

	public void setName(String s) {
		for (Question q : questions)
			q.testName = s;

		this.name = s;
	}

	public String getFullTest() {
		StringBuilder build = new StringBuilder(name + "\n\n");

		for (Question q : questions)
			build.append(q.toString() + "\n\n");

		return build.toString();
	}

	public ArrayList<String> getSearchableFields() {
		ArrayList<String> list = new ArrayList<String>();
		list.add(name);
		for (Question q : questions) {
			list.add(q.text);
			for (String s : q.options)
				list.add(s);
		}
		return list;
	}

	public static void updateFiles() {
		try {
			for (Test asdf : allTests)
				saveTest(asdf);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void saveTest(Test t) throws IOException {
		saveTest(t, new File("Tests/" + t.name + ".tst"));
	}

	public static void saveTest(Test asdf, File f) throws IOException {
		if (!f.exists())
			f.createNewFile();

		BufferedWriter out = new BufferedWriter(new FileWriter(f, false));

		// out.write(Encoder.encode(asdf.retakes) + "\n");
		// out.write(Encoder.encode(asdf.displayMode) + "\n");
		// out.write(Encoder.encode(asdf.time) + "\n");
		// out.write(Encoder.encode(asdf.showAnswerMode) + "\n");
		//
		// for (int c = 0; c < asdf.classes.length; c++) {
		// out.write(Encoder.encode(asdf.classes[c])
		// + (c == asdf.classes.length - 1 ? "" : ","));
		// }
		//
		// out.write("\n" + Encoder.encode(asdf.passing + ""));
		//
		// for (Question q : asdf.questions) {
		// out.write("\n" + Encoder.encode(Question.createFrom(q)));
		// out.flush();
		// }

		out.write(asdf.available + "\n");
		System.out.print(asdf.available + "\n");
		out.write(asdf.retakes + "\n");
		System.out.print(asdf.retakes + "\n");
		out.write(asdf.displayMode + "\n");
		System.out.print(asdf.displayMode + "\n");
		out.write(asdf.time + "\n");
		System.out.print(asdf.time + "\n");
		out.write(asdf.showAnswerMode + "\n");
		System.out.print(asdf.showAnswerMode + "\n");

		for (int c = 0; c < asdf.classes.length; c++) {
			out.write(asdf.classes[c] + (c == asdf.classes.length - 1 ? "" : ","));
			System.out.print(asdf.classes[c] + (c == asdf.classes.length - 1 ? "" : ","));
		}

		out.write("\n" + asdf.bands);
		System.out.print("\n" + asdf.bands);

		for (Question q : asdf.questions) {
			q.text = q.text.replaceAll("\n", "�");
			out.write("\n" + Question.createFrom(q));
			System.out.print("\n" + Question.createFrom(q));
			out.flush();
		}
		out.close();
	}

	public double getAvg() {
		double total = 0;
		int count = 0;

		for (int i = 0; i < classes.length; i++) {
			Course c = Course.getByNumber(classes[i]);
			for (User u : c.getParticipants()) {
				if (u.hasTaken(this)) {
					Score s = Score.fetch(u, this);
					total += s.grade() * 100d / s.possible();
					count++;
				}
			}
		}
		return total / count;
	}
}