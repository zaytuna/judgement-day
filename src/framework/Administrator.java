package framework;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import util.Methods;

public class Administrator extends User {
	public static ArrayList<Administrator> allAdmins = new ArrayList<Administrator>();
	public static Administrator SUPER_ADMINISTRATOR = Administrator.readFrom(Methods.getFileContents(
			new java.io.File("Resources/config.dll")).split("\n")[1]);
	public static final Administrator GENERIC = new Administrator();

	static {
		try {
			File f = new File("Users/0x2E305AF3CC00.junk");

			if (!f.exists())
				f.createNewFile();

			BufferedReader in = new BufferedReader(new FileReader(f));

			while (true) {
				String temp = in.readLine();

				if (temp == null)
					break;

				Administrator a = readFrom(temp);
				a.classes = Methods.ensureContainsOnly(a.classes, Course.getCourseNumbers());
				allAdmins.add(a);
			}

			allAdmins.add(SUPER_ADMINISTRATOR);
			in.close();

			SUPER_ADMINISTRATOR.classes = Methods
					.ensureContains(SUPER_ADMINISTRATOR.classes, Course.getCourseNumbers());
			SUPER_ADMINISTRATOR.classes = Methods.ensureContainsOnly(SUPER_ADMINISTRATOR.classes, Course
					.getCourseNumbers());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void saveAll() {
		try {
			File f = new File("Users/0x2E305AF3CC00.junk");
			BufferedWriter out = new BufferedWriter(new FileWriter(f, false));

			for (Administrator asdf : allAdmins) {
				if (asdf == SUPER_ADMINISTRATOR)
					continue;
				out.write(writeFrom(asdf) + "\n");
				out.flush();
			}

			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void addAdmin(Administrator u) {
		allAdmins.add(u);
		saveAll();
	}

	public static Administrator readFrom(String line) {
		// line = Encoder.decode(line);
		String[] parts = line.split("\t");
		Administrator u = new Administrator();
		u.id = parts[0];
		u.lName = parts[1];
		u.fName = parts[2];
		u.password = parts[3];
		u.question = parts[4];
		u.answer = parts[5];
		u.gender = parts[6].charAt(0) == 't';
		u.grade = Integer.parseInt(parts[7]);
		u.classes = new int[parts.length - 8];

		for (int i = 8; i < 8 + u.classes.length; i++)
			u.classes[i - 8] = Integer.parseInt(parts[i]);

		return u;
	}

	public static Administrator fromName(String teacher) {
		for (Administrator a : allAdmins)
			if (a.lName.equals(teacher))
				return a;
		return null;
	}

	public static Administrator fromID(String teacher) {
		for (Administrator a : allAdmins)
			if (a.id.equals(teacher))
				return a;
		return null;
	}

	public static void removeAdmin(Administrator a) {
		allAdmins.remove(a);
		saveAll();
	}

}