package framework;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

import util.Encoder;
import util.Methods;

public class GradeManager {
	public static Queue<GradingTask> todo = new LinkedList<GradingTask>();
	public static HashMap<GradingTask, User> pending = new HashMap<GradingTask, User>();
	public static final int GRADES = 3;

	static {
		String[] list = Methods.getFileContents(new File("Links/Pending/queue.txt")).split("\n");
		for (String s : list)
			if (s.length() > 0)
				todo.offer(fromString(s));

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				System.out.println("System was shutdown");
				try {
					save();
				} catch (IOException e) {
				}
			}
		});

	}

	public static void save() throws IOException {
		for (GradingTask t : pending.keySet())
			todo.offer(t);

		FileWriter fw = new FileWriter(new File("Links/Pending/queue.txt"));
		while (!todo.isEmpty())
			fw.write(todo.poll().toString() + "\n");

		fw.close();
	}

	/*
	 * Hand out the task to client grader-
	 * cannot mark as finished yet- goes into pending queue
	 */
	public static GradingTask requestTask(User u) {
		GradingTask t = null;

		if(todo.size() == 0)
			return null;
		
		int counter = 0;
		do {
			if (t != null)
				todo.offer(t);

			t = todo.poll();

			if (counter > todo.size()) {
				if (t != null)
					todo.offer(t);
				return null;
			}

			counter++;

		} while (t.userID.equals(u.id)
				|| Methods.intersection(Test.fromName(t.testName).classes, u.classes).length == 0);

		pending.put(t, u);

		return t;
	}

	/*
	 * client returns completed task.
	 */
	public static void submit(GradingTask t) {
		// User user = pending.get(t);
		pending.remove(t);

		if (t.timesGraded < GRADES) {
			if (t != null)
				todo.offer(t);
		} else {
			Score.rewriteGrade(t.userID, t.testName, t.questionNumber, Methods.mean(t.grades), "link-graded");
			try {
				Methods.copy(new File("Links/Pending/"
						+ Encoder.encode(t.userID + "-" + t.testName + "-" + t.questionNumber) + ".jpg"), new File(
						"Links/Finished/" + Encoder.encode(t.userID + "-" + t.testName + "-" + t.questionNumber)
								+ ".jpg"));

				new File("Links/Pending/" + Encoder.encode(t.userID + "-" + t.testName + "-" + t.questionNumber)
						+ ".jpg").delete();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	public static void offer(String userID, String testName, int qNumber) {
		for (GradingTask t : todo)
			if (t.userID == userID && t.testName == testName && t.questionNumber == qNumber)
				return;

		for (GradingTask t : pending.keySet()) {
			if (t.userID == userID && t.testName == testName && t.questionNumber == qNumber)
				return;
		}

		GradingTask t = new GradingTask();
		t.userID = userID;
		t.testName = testName;
		t.questionNumber = qNumber;
		t.timesGraded = 0;
		t.grades = new int[GRADES];

		todo.offer(t);
	}

	public static class GradingTask {
		private static int taskcount = 0;
		public String userID;
		public String testName;
		public int questionNumber, timesGraded;
		private int taskid;
		public int[] grades;

		GradingTask() {
			taskid = taskcount++;
		}

		public String toString() {
			String s = userID + "\t" + testName + "\t" + questionNumber + "\t" + timesGraded;

			for (int i = 0; i < grades.length; i++)
				s += "\t" + grades[i];

			return s;
		}

		@Override
		public boolean equals(Object t) {
			return t instanceof GradingTask && ((GradingTask) t).taskid == taskid;
		}

		@Override
		public int hashCode() {
			return taskid;
		}

		public void applyScore(int grade) {
			User user = pending.get(this);

			if (grades == null) {
				grades = new int[GRADES];
				timesGraded = 0;
			} else if (grades.length < timesGraded) {
				int[] newG = new int[GRADES];
				for (int i = 0; i < grades.length; i++)
					newG[i] = grades[i];
				grades = newG;
			}

			grades[timesGraded++] = grade;

			if (user.isAdmin())
				while (timesGraded < GRADES)
					grades[timesGraded++] = grade;
		}
	}

	public static GradingTask fromString(String s) {
		String[] split = s.split("\t");
		if (split.length < 4)
			return null;

		GradingTask task = new GradingTask();
		task.userID = split[0];
		task.testName = split[1];
		task.questionNumber = Integer.parseInt(split[2]);
		task.timesGraded = Integer.parseInt(split[3]);

		task.grades = new int[split.length - 4];
		for (int i = 4; i < split.length; i++)
			task.grades[i - 4] = Integer.parseInt(split[i]);

		return task;
	}
}
