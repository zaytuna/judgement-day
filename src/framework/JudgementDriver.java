package framework;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.UIManager;

import login.Login;
import login.Menu;
import testing.Tester;
import util.Sound;
import admin.AdminControlPanel;
import admin.SuperAdminPanel;

/*public class JudgementDriver extends JFrame implements MenuListener {
 private static final long serialVersionUID = 552375527020389050L;
 public static final int[] DESTINATIONS = { 0, 1, 2, -1, -999 };
 public static Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

 User user;
 JComponent current;

 public JudgementDriver() {
 setLayout(null);
 setBounds(0, 0, screen.width, screen.height);
 setUndecorated(true);

 getContentPane().setBackground(Color.BLACK);

 changeComponent(new Login(this), screen.width, screen.height);
 }
					5
 public void startTest(Test t) {
 Tester tester = new Tester(this);W
 changeComponent(tester, screen.width, screen.height);
 tester.startTest(t);
 }


 public void logout() {
 changeComponent(new Login(this), screen.width, screen.height);
 }

 public void login(User u) {
 this.user = u;

 if (u == Administrator.SUPER_ADMINISTRATOR)
 changeComponent(new SuperAdminPanel(this), screen.width, screen.height);
 else if (u.isAdmin()) {
 AdminControlPanel adminPanel = new AdminControlPanel(this, null);
 changeComponent(adminPanel, 1200, 870);
 adminPanel.setAdmin((Administrator) u);
 } else {
 Menu menu = new Menu(this);
 changeComponent(menu, 1000, 750);
 menu.setUser(u);
 }
 }

 public void paint(Graphics g) {
 if (getBufferStrategy() == null)
 createBufferStrategy(2);
 g = getBufferStrategy().getDrawGraphics();

 super.paint(g);

 getBufferStrategy().show();
 }

 public static void center(JComponent c, int w, int h) {
 int x = (screen.width - w) / 2;
 int y = (screen.height - h) / 2;

 c.setBounds(x, y, w, h);
 }

 public static void main(String[] args) throws Exception {
 JudgementDriver jd = new JudgementDriver();
 jd.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 jd.setVisible(true);
 }

 public void toMenu() {
 Menu menu = new Menu(this);
 changeComponent(menu, 1000, 750);
 menu.setUser(user);
 }

 public void changeComponent(JComponent c, int w, int h) {
 if(current!=null)
 remove(current)	;
 center(c, w, h);
 add(c);

 current = c;

 invalidate();
 repaint();
 System.out.println(getContentPane().getComponents().length);
 }
 }*/

public class JudgementDriver extends JFrame implements MenuListener {
	private static final long serialVersionUID = 552375527020389050L;
	public static final int[] DESTINATIONS = { 0, 1, 2, -1, -999 };
	public static Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	public static JudgementDriver program;

	public static JDUI uioptions;

	Menu menu;
	Login login;
	Tester tester;
	AdminControlPanel adminPanel;
	SuperAdminPanel sap;

	User user = null;

	int phase = 0;

	Sound sound;

	static {
		// uioptions = JDUI.createClientUI();
		uioptions = JDUI.createGoodUI();
	}

	public JudgementDriver() {
		menu = new Menu(this);
		login = new Login(this);
		tester = new Tester(this);
		adminPanel = new AdminControlPanel(this, null);
		sap = new SuperAdminPanel(this);

		setLayout(null);
		program = this;

		setBounds(0, 0, screen.width, screen.height);
		setUndecorated(true);

		getContentPane().setBackground(uioptions.back);
		// Login login = new Login(this); // current = login;

		center(login, screen.width, screen.height);
		center(menu, 1000, 770);
		center(tester, screen.width, screen.height);
		center(adminPanel, 1200, 870);
		center(sap, 1200, 900);

		add(menu);
		add(sap);
		add(tester);
		add(adminPanel);
		add(login);

		setPhase(0);
	}

	public void startTest(Test t) {
		tester.startTest(t);
		tester.setUser(user);
		setPhase(2);
	}

	public void logout() {
		setPhase(0);
	}

	public void setPhase(int p) {
		if (sound != null)
			sound.setFade(-.2);

		if (phase == 2)
			tester.reset();
		else if (phase == 1)
			menu.reset();
		else if (phase == -1)
			adminPanel.reset();

		if (uioptions.music)
			if (p == 0) {
				sound = new Sound(new File("Music/menu.wav"));
				sound.loop(-1);
				sound.start();
			} else if (p == 2) {
				sound = new Sound(new File("Music/test.wav"));
				sound.loop(-1);
				sound.start();
			}
		// JComponent c; switch(p) { case 0: c = new Login(this); break; case 1:
		// c = new
		// Menu(this); break; case 2: c = new Tester(this); case 4: c = }
		login.setVisible(p == 0);
		menu.setVisible(p == 1);
		adminPanel.setVisible(p == -1);
		sap.setVisible(p == -999);
		tester.setVisible(p == 2);
		phase = p;
	}

	public void login(User u) {
		this.user = u;

		if (u == Administrator.SUPER_ADMINISTRATOR)
			setPhase(-999);
		else if (u.isAdmin()) {
			adminPanel.setAdmin((Administrator) u);
			setPhase(-1);
		} else {
			setPhase(1);
			menu.setUser(u);
		}
	}

	public static void center(JComponent c, int w, int h) {
		int x = (screen.width - w) / 2;
		int y = (screen.height - h) / 2;

		c.setBounds(x, y, w, h);
	}

	public static void main(String[] args) throws Exception {
		System.out.println("START");
		// UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());

		JudgementDriver jd = new JudgementDriver();
		// zzzzzzzzzoSystem.out.println(UIManager.getInstalledLookAndFeels()[0].getClassName());
		// UIManager.setLookAndFeel(UIManager.getInstalledLookAndFeels()[2].getClassName());
		jd.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jd.setVisible(true);
		System.out.println("END");

	}

	// class BackgroundPanel extends JPanel {
	// private static final long serialVersionUID = -2115018020849841795L;
	//
	// public void paint(Graphics g) {
	// super.paint(g);
	// Graphics2D g2d = (Graphics2D) g;
	//
	// BufferedImage buff = new BufferedImage(30, 30,
	// BufferedImage.TYPE_INT_RGB);
	// Graphics2D gg = buff.createGraphics();
	//
	// gg.setColor(new Color(230, 255, 230));
	// gg.fillRect(0, 0, 35, 35);
	// gg.setColor(Color.black);
	// gg.drawLine(0, 35, 35, 0);
	// gg.drawLine(0, 0, 35, 35);
	//
	// g2d.setPaint(new TexturePaint(buff, new Rectangle(30, 30)));
	// g2d.fillRect(0, 0, getWidth(), getHeight());
	// g2d.setColor(Color.black);
	// }
	// }

	@Override
	public void toMenu() {
		setPhase(1);
	}
}
