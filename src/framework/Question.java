package framework;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import testing.MatcherPanel;
import testing.MazePanel;
import testing.Tester;
import testing.Tester.Handler;
import util.Encoder;
import util.HTMLCreator;
import util.Methods;
import util.SearchObject;
import util.Timer;
import util.paint.UltraPaint;

public class Question implements SearchObject {
	public static final String[] TYPES = { "Multiple Choice Question", "True/False Question", "Short Answer Question",
			"Diagram Question", "Matching Question", "MAZE Question", "Choose Words in a Sentence Question",
			"Check all that Apply Question", "Essay Question" };
	public static final Color CORRECT_COLOR = new Color(200, 250, 200);
	public static final Color INCORRECT_COLOR = new Color(250, 200, 200);

	// QuestionComponent comp = null;

	public String testName = "UNSPECIFIED";

	public final int type; // 0 = Multiple choice, 1 = true/false, 2 = Short
	// Answer, 3
	// = Drawing, 4 = Matching, 5 = MAZE, 6 = choose words in a sentence,
	// 7 = check all that apply, 8 = Free Response

	public int index = 0;
	public int spec = 0;// 0 = nothing weird, 1 = case sensitive, 2 = override
	// to be teacher corrected, 3 = all or nothing
	public int blankPolicy = 0;// amount a blank is worth

	public String text;
	public String[] options; // used for 0, 4, 7

	public String correct;
	public int worth;
	public String image;

	// NEW

	/*
	 * type: 0 correct = "A" 1 correct = "t" 2 correct = "CORRECT_ANSWER" 3
	 * correct = null 4 correct = null 5 correct = "0" 6 correct =
	 * "he and his friends" 7 correct = "0,1,1,0,1" 8 correct = null
	 */

	public Question(String testName, int type, int worth, String text, String answer, String... options) {
		this.testName = testName;
		this.worth = worth;
		this.options = options;
		this.text = text;
		this.correct = answer;
		this.type = type;
	}

	public String toString() {
		return text;
	}

	public void setUngradable() {
		spec = 2;
	}

	public String getDisplay() {
		return "Question " + (1 + index);
	}

	public String getOutputString() {
		return text;
	}

	public String getAnswer(String str) {
		if (str.equalsIgnoreCase("NONE"))
			return "no answer";

		switch (type) {
		case 0:
			return options[Integer.parseInt(str)];
		case 1:
			return str.startsWith("t") ? "true" : "false";
		case 7:
			String s = "";
			String[] strs = str.split(",");
			for (int i = 0; i < strs.length; i++)
				s += options[Integer.parseInt(strs[i])] + ", ";

			return s;

		}
		return str;
	}

	public boolean isCorrect(String key) {
		switch (type) {

		case 4:
			String[] split1 = key.split(":");
			String[] split2 = correct.split(":");

			for (int i = 0; i < split1.length - 1; i += 2) {
				boolean found = false;
				for (int j = 0; j < split2.length - 1; j += 2) {
					if ((Integer.parseInt(split1[i]) == Integer.parseInt(split2[j]) && Integer.parseInt(split1[i + 1]) == Integer
							.parseInt(split2[j + 1]))
							|| (Integer.parseInt(split1[i]) == Integer.parseInt(split2[j + 1]) && Integer
									.parseInt(split1[i + 1]) == Integer.parseInt(split2[j]))) {
						found = true;
						break;
					}
				}
				if (!found)
					return false;
			}
			return true;
		default:
			return key.equalsIgnoreCase(correct);
		}
	}

	public ArrayList<String> getSearchableFields() {
		ArrayList<String> list = new ArrayList<String>();
		list.add(text);

		for (int i = 0; i < options.length; i++)
			list.add(options[i]);
		return list;
	}

	public boolean teacherMustGrade() {
		return (spec == 2) || (type == 3) || (type == 8);
	}

	public int getMaxMatchingLength() {
		if (type != 4)
			return -1;

		return Math.max(getLeftMatchingLength(), getRightMatchingLength());
	}

	public String[] getLeftMatchingSide() {
		if (type != 4)
			return null;

		String[] stuffs = new String[getLeftMatchingLength()];

		int counter = 0;

		for (String temp : options) {
			String[] split = temp.split(":");
			try {
				if ((split.length == 1 && temp.charAt(0) != ':') || !(split[0] == null || split[0].trim().equals("")))
					stuffs[counter++] = temp.split(":")[0].trim();
			} catch (IndexOutOfBoundsException e) {
			}
		}

		return stuffs;
	}

	public int getLeftMatchingLength() {
		if (type != 4)
			return -1;
		int val = 0;

		for (String temp : options) {
			String[] split = temp.split(":");
			try {
				if ((split.length == 1 && temp.charAt(0) != ':') || !(split[0] == null || split[0].trim().equals("")))
					val++;
			} catch (IndexOutOfBoundsException e) {
			}
		}

		return val;
	}

	public int getRightMatchingLength() {
		if (type != 4)
			return -1;
		int val = 0;

		for (String temp : options) {
			String[] split = temp.split(":");
			try {
				if ((split.length == 1 && temp.charAt(0) == ':') || !(split[1] == null || split[1].trim().equals("")))
					val++;
			} catch (IndexOutOfBoundsException e) {
			}
		}

		return val;
	}

	public String[] getRightMatchingSide() {
		if (type != 4)
			return null;

		String[] stuffs = new String[getRightMatchingLength()];

		int counter = 0;

		for (String temp : options) {
			String[] split = temp.split(":");
			try {
				if ((split.length == 1 && temp.charAt(0) == ':') || !(split[1] == null || split[1].trim().equals("")))
					stuffs[counter++] = temp.split(":")[1].trim();
			} catch (IndexOutOfBoundsException e) {
			}
		}

		return stuffs;
	}

	public static Map<String, Integer> getAllGivenAnswers(Question question, Test test) {
		HashMap<String, Integer> map = new HashMap<String, Integer>();

		for (java.io.File f : new java.io.File("Results").listFiles()) {
			if (!Encoder.decode(f.getName()).startsWith(test.getName()))
				continue;

			String temp = Methods.getFileContents(f).split("\n")[question.index + 4];
			String answer = temp.substring(temp.indexOf("\t") + 1);
			if (!map.keySet().contains(answer))
				map.put(answer, 1);
			else
				map.put(answer, map.get(answer) + 1);
		}

		return map;
	}

	public QuestionComponent createDisplay(int w, int h) {
		if (type == 0)// MCQ
			return new MCComponent(this, w, h);

		else if (type == 1)// true/false
			return new TFComponent(this, w, h);

		else if (type == 2)// Short Answer
			return new SAComponent(this, w, h);

		else if (type == 3)// Drawing
			return new DrawComponent(this, w, h);

		else if (type == 4)// Matching
			return new MatchingComponent(this, w, h);

		else if (type == 5)// Maze
			return new MAZEComponent(this, w, h);

		else if (type == 6)// Identify words in sentence
			return new ChooseWordsComponent(this, w, h);

		else if (type == 7)// Check all that apply
			return new CATAComponent(this, w, h);

		else if (type == 8) // FRQ
			return new FRComponent(this, w, h);

		return null;
	}

	public static Question parseFrom(String s, String testName) {
		String[] split = s.split("\t");

		/*
		 * Is assumed to be in format 0 type 1 spec 2 blankPolicy 3 worth 4 text
		 * 5 correct 6 optionA optonB optionC optionD optionE ... etc.
		 * 
		 * except on one line and tab delimited.
		 */

		Question q;
		if (split.length <= 7)
			q = new Question(testName, Integer.parseInt(split[0]), Integer.parseInt(split[3]), split[4], split[5]);
		else
			q = new Question(testName, Integer.parseInt(split[0]), Integer.parseInt(split[3]), split[4], split[5],
					Methods.createSubArray(split, 6, split.length - 1, new String[split.length - 7]));

		q.image = split[split.length - 1];
		q.spec = Integer.parseInt(split[1]);
		q.blankPolicy = Integer.parseInt(split[2]);

		return q;
	}

	public int getPreferredHeight() {
		int h = 60;
		switch (type) {
		case 0:
			h = 90;
			break;
		case 1:
			h = 90;
			break;
		case 2:
			h = 90;
			break;
		case 3:
			h = 400;
			break;
		case 4:
			h = options.length * 30 + 90;
			break;
		case 5:
			h = 120;
			break;
		case 6:
			h = 110;
			break;
		case 7:
			h = options.length * 30 + 40;
			break;
		case 8:
			h = 400;
			break;
		}
		h += (image.equals("NONE") ? 0 : new ImageIcon("Links/" + image).getIconHeight());
		FontMetrics fm = JudgementDriver.program.getFontMetrics(JudgementDriver.uioptions.questionFont);
		h += text.split("\n").length * fm.getHeight();

		return h;
	}

	public String getHTML() {
		return HTMLCreator.getHTML(text);
	}

	public static String createFrom(Question q) {
		String opt = "";

		for (int i = 0; i < q.options.length; i++)
			opt += "\t" + q.options[i];

		return q.type + "\t" + q.spec + "\t" + q.blankPolicy + "\t" + q.worth + "\t" + q.text + "\t" + q.correct + opt
				+ "\t" + q.image;
	}

	// *******************************************************************
	// ********************* END OF ALL METHODS **************************
	// *******************************************************************

	public abstract static class QuestionComponent extends JPanel {
		private static final long serialVersionUID = -5518301066476865956L;
		public Question question;

		QuestionComponent(Question q) {
			this.question = q;
		}

		/*
		 * @return 0 if the answer is correct, 1 if it is incorrect, 2 if blank,
		 * and -1 if it cannot be computer graded.
		 */
		abstract public int grade();

		abstract public int isCorrect();

		abstract public boolean hasStarted();

		abstract public String getAnswer();

		abstract public void addListeners(Tester.Handler h);

		abstract public void show(int mode);

		public void saveToFile(File file) {
			try {
				FileWriter fw = new FileWriter(file);
				fw.write(getAnswer());
				fw.close();
			} catch (Exception e) {
			}
		}
	}

	// ****************** START INDIVIDUAL QUESTION COMPONENTS *****************

	public class MCComponent extends QuestionComponent {
		private static final long serialVersionUID = -8262900937390095910L;
		public JRadioButton[] choices;
		JLabel inst;

		public MCComponent(Question q, int w, int h) {
			super(q);

			setLayout(null);

			choices = new JRadioButton[q.options.length];
			inst = new JLabel(q.getHTML(), JLabel.CENTER);
			inst.setFont(JudgementDriver.uioptions.questionFont);
			if (!q.image.equals("NONE"))
				inst.setIcon(new ImageIcon("Links/" + q.image));
			ButtonGroup bg = new ButtonGroup();

			for (int i = 0; i < choices.length; i++) {
				choices[i] = new JRadioButton(q.options[i]);
				choices[i].setBounds(5 + i * (w - 10) / choices.length, h / 2, (w - 10) / choices.length, h / 2);
				choices[i].setOpaque(false);
				choices[i].setFont(JudgementDriver.uioptions.questionFont);
				bg.add(choices[i]);
				add(choices[i]);
			}

			inst.setBounds(0, 0, w, h / 2);
			add(inst);
		}

		public int isCorrect() {
			if (spec == 2)
				return -1;
			if (choices[Integer.parseInt(super.question.correct)].isSelected())
				return 0;

			for (int i = 0; i < choices.length; i++)
				if (choices[i].isSelected())
					return 1;

			return 2;
		}

		@Override
		public void addListeners(Handler h) {
			for (JRadioButton r : choices)
				r.addActionListener(h);
		}

		@Override
		public boolean hasStarted() {
			for (JRadioButton r : choices)
				if (r.isSelected())
					return true;
			return false;
		}

		@Override
		public String getAnswer() {
			for (int r = 0; r < choices.length; r++)
				if (choices[r].isSelected())
					return "" + r;
			return "None";
		}

		@Override
		public void show(int mode) {
			if ((mode == 0 || mode == 1) && isCorrect() == 0)
				setBackground(CORRECT_COLOR);
			else if (mode == 1 && isCorrect() == 1) {
				choices[Integer.parseInt(super.question.correct)].setFont(new Font("SANS_SERIF", Font.ITALIC, 20));// .setBackground(CORRECT_COLOR);
				choices[Integer.parseInt(super.question.correct)].setText(choices[Integer
						.parseInt(super.question.correct)].getText().toUpperCase());
			}
			if ((mode == 1 || mode == 0) && isCorrect() == 1)
				setBackground(INCORRECT_COLOR);
			else if (isCorrect() == 2)
				setBackground(Color.LIGHT_GRAY);
		}

		@Override
		public int grade() {
			switch (isCorrect()) {
			case 0:
				return worth;
			case 2:
				return blankPolicy;
			default:
				return 0;
			}
		}
	}

	public class TFComponent extends QuestionComponent {
		private static final long serialVersionUID = -719683658250455818L;
		public JRadioButton[] choices;
		ButtonGroup g = new ButtonGroup();

		public TFComponent(Question q, int w, int h) {
			super(q);
			setLayout(null);
			choices = new JRadioButton[2];
			JLabel inst = new JLabel(q.getHTML(), JLabel.CENTER);
			inst.setFont(JudgementDriver.uioptions.questionFont);
			if (!q.image.equals("NONE"))
				inst.setIcon(new ImageIcon("Links/" + q.image));

			for (int i = 0; i < choices.length; i++) {
				choices[i] = new JRadioButton((i == 0) ? "true" : "false");
				choices[i].setBounds(w / 2 - 80 + (80 * i), h / 2, 80, h / 2);
				choices[i].setOpaque(false);
				choices[i].setFont(JudgementDriver.uioptions.questionFont);
				add(choices[i]);
				g.add(choices[i]);
			}

			inst.setBounds(0, 0, w, h / 2);
			add(inst);
		}

		public int isCorrect() {
			if (spec == 2)
				return -1;
			char c = Character.toLowerCase(super.question.correct.charAt(0));

			if ((c == 't' && choices[0].isSelected()) || (c == 'f' && choices[1].isSelected()))
				return 0;

			if (choices[0].isSelected() || choices[1].isSelected())
				return 1;

			return 2;
		}

		@Override
		public void addListeners(Handler h) {
			for (JRadioButton r : choices)
				r.addActionListener(h);
		}

		@Override
		public boolean hasStarted() {
			for (JRadioButton r : choices)
				if (r.isSelected())
					return true;
			return false;
		}

		@Override
		public String getAnswer() {
			if (choices[0].isSelected())
				return "t";
			else if (choices[1].isSelected())
				return "f";

			return "None";
		}

		@Override
		public void show(int mode) {
			if ((mode == 0 || mode == 1) && isCorrect() == 0)
				setBackground(CORRECT_COLOR);
			else if (mode == 1 && isCorrect() == 1) {
				char c = Character.toLowerCase(super.question.correct.charAt(0));
				// (c == 't' ? choices[0] :
				// choices[1]).setForeground(CORRECT_COLOR);
				(c == 't' ? choices[0] : choices[1]).setFont(new Font("SANS_SERIF", Font.ITALIC, 30));
				(c == 't' ? choices[0] : choices[1]).setText((c == 't' ? choices[0] : choices[1]).getText()
						.toUpperCase());
			}
			if ((mode == 0 || mode == 1) && isCorrect() == 1)
				setBackground(INCORRECT_COLOR);
			else if (isCorrect() == 2)
				setBackground(Color.LIGHT_GRAY);

		}

		@Override
		public int grade() {
			switch (isCorrect()) {
			case 0:
				return worth;
			case 2:
				return blankPolicy;
			default:
				return 0;
			}
		}
	}

	public class SAComponent extends QuestionComponent {
		private static final long serialVersionUID = -2542273574160128681L;
		public JTextField box = new JTextField();

		public SAComponent(Question q, int w, int h) {
			super(q);

			setLayout(null);

			JLabel inst = new JLabel(question.getHTML(), JLabel.CENTER);
			if (!q.image.equals("NONE"))
				inst.setIcon(new ImageIcon("Links/" + q.image));

			inst.setFont(JudgementDriver.uioptions.questionFont);

			inst.setBounds(0, 0, w, h - 45);
			box.setBounds(30, h - 40, w - 60, 30);

			add(inst);
			add(box);
		}

		public int isCorrect() {
			if (spec == 2)
				return -1;
			if (spec == 0) {
				if (box.getText().equalsIgnoreCase(super.question.correct))
					return 0;
				if (box.getText().equals("") || box.getText() == null)
					return 2;
				return 1;
			} else if (spec == 1) {
				if (box.getText().equals(super.question.correct))
					return 0;
				if (box.getText().equals("") || box.getText() == null)
					return 2;
				return 1;
			}
			return 2;
		}

		@Override
		public void addListeners(Handler h) {
			box.addKeyListener(h);
		}

		@Override
		public boolean hasStarted() {
			return !box.getText().equals("");
		}

		@Override
		public String getAnswer() {
			return box.getText();
		}

		@Override
		public void show(int mode) {
			if ((mode == 0 || mode == 1) && isCorrect() == 0)
				setBackground(CORRECT_COLOR);
			else if (mode == 1 && isCorrect() == 1)
				box.setText("Your answer: " + box.getText() + "     Correct: " + super.question.correct);
			if ((mode == 1 || mode == 0) && isCorrect() == 1)
				setBackground(INCORRECT_COLOR);
			else if (isCorrect() == 2)
				setBackground(Color.LIGHT_GRAY);
		}

		@Override
		public int grade() {
			switch (isCorrect()) {
			case 0:
				return worth;
			case 2:
				return blankPolicy;
			default:
				return 0;
			}
		}
	}

	public class DrawComponent extends QuestionComponent {
		private static final long serialVersionUID = 4198071503333463060L;
		JLabel inst = new JLabel();
		public UltraPaint paint = new UltraPaint();

		public DrawComponent(Question q, int w, int h) {
			super(q);
			inst.setText(q.getHTML());
			inst.setHorizontalAlignment(JLabel.CENTER);
			inst.setFont(JudgementDriver.uioptions.questionFont);
			if (!q.image.equals("NONE"))
				inst.setIcon(new ImageIcon("Links/" + q.image));
			setLayout(new BorderLayout(10, 20));
			add(inst, BorderLayout.NORTH);
			add(paint, BorderLayout.CENTER);
		}

		public int isCorrect() {
			return -1;
		}

		@Override
		public void addListeners(Handler h) {
			paint.panel.addMouseListener(h);
		}

		@Override
		public boolean hasStarted() {
			return !paint.panel.shapes.isEmpty();
		}

		@Override
		public String getAnswer() {
			return "N/A";
		}

		@Override
		public void show(int mode) {
			setBackground(Color.WHITE);
		}

		@Override
		public int grade() {
			return 0;
		}

		@Override
		public void saveToFile(File f) {
			paint.save(f);
		}
	}

	public class MatchingComponent extends QuestionComponent {
		private static final long serialVersionUID = 5499177158731717351L;

		public MatcherPanel mp = new MatcherPanel();

		public MatchingComponent(Question q, int w, int h) {
			super(q);
			setLayout(new BorderLayout());
			add(mp, BorderLayout.CENTER);
			mp.setMixAndMatchTest(q, true);
		}

		public int isCorrect() {
			return mp.isCorrect();
		}

		@Override
		public void addListeners(Handler h) {
			mp.addMouseListener(h);
		}

		@Override
		public boolean hasStarted() {
			return !mp.connections.isEmpty();
		}

		@Override
		public String getAnswer() {
			StringBuilder build = new StringBuilder();

			for (int i = 0; i < mp.connections.size(); i++)
				build.append(":" + Methods.indexOf(mp.left.get(mp.connections.get(i).a), getLeftMatchingSide()) + ":"
						+ Methods.indexOf(mp.right.get(mp.connections.get(i).b), getRightMatchingSide()));

			if (build.length() > 0)
				build.deleteCharAt(0);

			return build.toString();
		}

		@Override
		public void show(int mode) {
			if ((mode == 0 || mode == 1) && isCorrect() == 0)
				setBackground(CORRECT_COLOR);
			else if (mode == 1 && isCorrect() == 1)
				mp.show();

			if ((mode == 1 || mode == 0) && isCorrect() == 1)
				setBackground(INCORRECT_COLOR);
			else if (isCorrect() == 2)
				setBackground(Color.LIGHT_GRAY);
		}

		@Override
		public int grade() {
			if (spec == 3)
				switch (isCorrect()) {
				case 0:
					return worth;
				case 2:
					return blankPolicy;
				default:
					return 0;
				}
			return mp.grade();
		}
	}

	public class MAZEComponent extends QuestionComponent implements ActionListener {
		private static final long serialVersionUID = -4102288941832498085L;

		public MazePanel mp;
		public Timer timer = new Timer();
		JButton start = new JButton("Begin.... once started, you must work quickly.");
		JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));

		public MAZEComponent(Question q, int w, int h) {
			super(q);
			mp = new MazePanel(JudgementDriver.program.getFontMetrics(MazePanel.FONT), w, q);

			setLayout(new BorderLayout());
			panel.add(start);
			add(panel, BorderLayout.NORTH);
			add(mp, BorderLayout.CENTER);
			add(timer, BorderLayout.SOUTH);

			timer.setMax(Integer.parseInt(q.options[0]));
			setBackground(Color.BLACK);

			panel.setOpaque(false);

			timer.addActionListener(this);
			start.addActionListener(this);
			timer.setForeground(Color.white);

			mp.setVisible(false);
		}

		public int isCorrect() {
			if (spec == 2)
				return -1;
			if (!hasStarted())
				return 2;

			String[] poss = super.question.correct.split(",");
			int[] ans = new int[poss.length];

			for (int i = 0; i < ans.length; i++)
				ans[i] = Integer.parseInt(poss[i]);

			for (int i = 0; i < mp.options.size(); i++)
				if (mp.options.get(i).selected != ans[i])
					return 1;
			return 0;
		}

		@Override
		public void addListeners(Handler h) {
			mp.addMouseListener(h);
		}

		@Override
		public boolean hasStarted() {
			for (MazePanel.Option o : mp.options)
				if (o.selected != -1)
					return true;
			return false;
		}

		@Override
		public String getAnswer() {
			mp.finish();
			return mp.getAnswer();
		}

		@Override
		public void show(int mode) {
			mp.finish();
			timer.clear();
			if ((mode == 0 || mode == 1) && isCorrect() == 0)
				setBackground(CORRECT_COLOR);
			if (mode == 1)
				mp.showAnswers();
			if ((mode == 1 || mode == 0) && isCorrect() == 1)
				setBackground(INCORRECT_COLOR);
			else if (isCorrect() == 2)
				setBackground(Color.LIGHT_GRAY);
		}

		@Override
		public int grade() {
			return mp.grade();
		}

		@Override
		public void actionPerformed(ActionEvent evt) {
			if (evt.getSource() == timer) {
				mp.finish();
				timer.clear();
			} else if (evt.getSource() == start) {
				timer.start();
				panel.setVisible(false);
				mp.setVisible(true);
			}
		}
	}

	public class ChooseWordsComponent extends QuestionComponent {
		private static final long serialVersionUID = -7099154903177435996L;

		public ChooseWordsComponent(Question q, int w, int h) {
			super(q);
		}

		public int isCorrect() {
			if (spec == 2)
				return -1;
			return 0;
		}

		@Override
		public void addListeners(Handler h) {
			// TODO Auto-generated method stub

		}

		@Override
		public boolean hasStarted() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public String getAnswer() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void show(int mode) {
			// TODO Auto-generated method stub

		}

		@Override
		public int grade() {
			// TODO Auto-generated method stub
			return 0;
		}
	}

	public class FRComponent extends QuestionComponent {
		private static final long serialVersionUID = 8573663528006926002L;
		public JTextArea area;

		public FRComponent(Question q, int w, int h) {
			super(q);

			setLayout(null);

			area = new JTextArea();
			JScrollPane scroll = new JScrollPane(area);
			scroll.getVerticalScrollBar().setUnitIncrement(16);
			JLabel inst = new JLabel(q.getHTML(), JLabel.CENTER);
			inst.setFont(JudgementDriver.uioptions.questionFont);
			if (!q.image.equals("NONE"))
				inst.setIcon(new ImageIcon("Links/" + q.image));

			scroll.setBounds(10, inst.getIcon() == null ? 35 : 20 + inst.getIcon().getIconHeight() + 5, w - 20, h
					- ((inst.getIcon() == null ? 35 : 20 + inst.getIcon().getIconHeight()) + 10));
			inst.setBounds(10, 10, w - 20, inst.getIcon() == null ? 35 : 20 + inst.getIcon().getIconHeight());
			add(inst);
			add(scroll);
		}

		public int isCorrect() {
			return -1;
		}

		@Override
		public void addListeners(Handler h) {
			area.addKeyListener(h);
		}

		@Override
		public boolean hasStarted() {
			return !area.getText().equals("");
		}

		@Override
		public String getAnswer() {
			return area.getText().replaceAll("\n", "�");
		}

		@Override
		public void show(int mode) {
			setBackground(Color.WHITE);
		}

		@Override
		public int grade() {
			return 0;
		}
	}

	public class CATAComponent extends QuestionComponent {
		private static final long serialVersionUID = -5637355897042198945L;
		public JCheckBox[] choices;

		public CATAComponent(Question q, int w, int h) {
			super(q);

			setLayout(null);

			choices = new JCheckBox[options.length];
			JLabel inst = new JLabel(q.getHTML(), JLabel.CENTER);
			inst.setFont(JudgementDriver.uioptions.questionFont);

			if (!q.image.equals("NONE"))
				inst.setIcon(new ImageIcon("Links/" + q.image));

			for (int i = 0; i < choices.length; i++) {
				choices[i] = new JCheckBox(options[i]);
				choices[i].setFont(JudgementDriver.uioptions.questionFont);
				choices[i].setBounds(30, (inst.getIcon() == null ? 30 : inst.getIcon().getIconHeight()) + 30 * i, 200,
						25);
				choices[i].setOpaque(false);
				add(choices[i]);
			}

			inst.setBounds(0, 0, w, inst.getIcon() == null ? 30 : inst.getIcon().getIconHeight());
			add(inst);
		}

		public int isCorrect() {
			if (spec == 2)
				return -1;

			String[] poss = super.question.correct.split(",");
			int[] ans = new int[poss.length];

			for (int i = 0; i < ans.length; i++)
				ans[i] = Integer.parseInt(poss[i]);

			for (int i = 0; i < choices.length; i++)
				if (choices[i].isSelected() && !Methods.contains(ans, i))
					return 1;

			for (JCheckBox c : choices)
				if (c.isSelected())
					return 0;

			return 2;
		}

		@Override
		public void addListeners(Handler h) {
			for (JCheckBox c : choices)
				c.addActionListener(h);
		}

		@Override
		public boolean hasStarted() {
			for (JCheckBox r : choices)
				if (r.isSelected())
					return true;
			return false;
		}

		@Override
		public String getAnswer() {
			String ans = "";
			for (int r = 0; r < choices.length; r++)
				if (choices[r].isSelected())
					ans += "," + r;

			return ans.equals("") ? "" : ans.substring(1);
		}

		@Override
		public void show(int mode) {
			if ((mode == 0 || mode == 1) && isCorrect() == 0)
				setBackground(CORRECT_COLOR);
			else if (mode == 1 && isCorrect() == 1) {
				String[] poss = super.question.correct.split(",");

				for (int i = 0; i < poss.length; i++) {
					choices[Integer.parseInt(poss[i])].setFont(new Font("SANS_SERIF", Font.ITALIC, 20));// setBackground(CORRECT_COLOR);
					choices[Integer.parseInt(poss[i])].setText(choices[Integer.parseInt(poss[i])].getText()
							.toUpperCase());
				}
			}

			if ((mode == 0 || mode == 1) && isCorrect() == 1)
				setBackground(INCORRECT_COLOR);
			else if (isCorrect() == 2)
				setBackground(Color.LIGHT_GRAY);
		}

		@Override
		public int grade() {
			if (teacherMustGrade())
				return 0;
			if (spec == 3)
				switch (isCorrect()) {
				case 0:
					return worth;
				case 2:
					return blankPolicy;
				default:
					return 0;
				}
			double score = 0;

			String[] poss = super.question.correct.split(",");
			int[] ans = new int[poss.length];

			for (int i = 0; i < ans.length; i++)
				ans[i] = Integer.parseInt(poss[i]);

			for (int i = 0; i < choices.length; i++)
				if (choices[i].isSelected() && Methods.contains(ans, i))
					score += worth / (double) choices.length;
				else if (choices[i].isSelected())
					score -= worth / (double) choices.length;

			return Math.max(0, (int) Math.round(score * worth / ans.length));
		}
	}
}